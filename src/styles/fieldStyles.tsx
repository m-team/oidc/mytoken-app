import { StyleSheet } from 'react-native'
import { themeType } from '../types'

/*
Styling for the way all fields should look and their hitslop
 */
const createStylesField = (theme: themeType) => StyleSheet.create({
  textInput: {
    borderColor: theme.borderTextEntry,
    height: 55,
    borderWidth: 2,
    padding: 13,
    borderRadius: 6,
    fontFamily: 'Inter_400Regular',
    color: theme.mainOpposite,
    fontSize: 17,
    backgroundColor: theme.textEntryContent
  },
  textSearchDark: {
    borderColor: theme.backgroundGrey,
    height: 38,
    borderWidth: 2.4,
    borderRadius: 6,
    fontFamily: 'Inter_400Regular',
    fontSize: 17,
    backgroundColor: theme.searchBack
  },
  textSearchLight: {
    borderColor: theme.searchDarkBorder,
    height: 38,
    borderWidth: 2.4,
    borderRadius: 6,
    fontFamily: 'Inter_400Regular',
    fontSize: 17,
    backgroundColor: theme.lightback,
    color: theme.backgroundSubtle
  },
  box: {
    borderColor: theme.backgroundGrey,
    borderWidth: 2,
    padding: 13,
    borderRadius: 6,
    fontFamily: 'Inter_400Regular',
    fontSize: 17,
    backgroundColor: theme.textEntryContent
  },
  secureBox: {
    borderColor: theme.borderTextEntry,
    borderWidth: 2,
    borderRadius: 6,
    backgroundColor: theme.textEntryContent
  },
  greyButton: {
    borderColor: theme.backgroundGrey,
    borderWidth: 2,
    borderRadius: 6,
    backgroundColor: theme.neutral,
    height: 45
  },
  dateCircleButton: {
    borderRadius: 6,
    backgroundColor: theme.dateColor,
    height: 35,
    paddingHorizontal: 7,
    justifyContent: 'center'
  },
  heatMapSquare: {
    height: 23,
    width: 23,
    borderRadius: 2,
    margin: 3.5
  },
  square: {
    borderColor: theme.backgroundGrey,
    borderWidth: 2,
    height: 30,
    width: 30,
    borderRadius: 6,
    fontFamily: 'Inter_400Regular',
    fontSize: 17,
    backgroundColor: theme.neutral
  },
  squareFilters: {
    borderColor: theme.backgroundGrey,
    borderWidth: 2,
    height: 26,
    width: 26,
    borderRadius: 4,
    backgroundColor: theme.neutral
  },
  buttonMain: {
    height: 40,
    width: 120,
    borderRadius: 40,
    backgroundColor: theme.mytokenColor,
    justifyContent: 'center'
  },
  subButton: {
    height: 40,
    width: 120,
    borderWidth: 3,
    borderColor: theme.backgroundSubtle,
    borderRadius: 40,
    backgroundColor: theme.main,
    justifyContent: 'center'
  },
  buttonWeak: {
    height: 40,
    width: 120,
    borderRadius: 40,
    borderColor: theme.mytokenColor,
    borderWidth: 3,
    backgroundColor: 'white'
  },
  indicator: {
    height: 8,
    width: 8,
    borderRadius: 30,
    margin: 2.5
  },
  fixedButtonContainer: {
    alignSelf: 'center',
    position: 'absolute'
  }
})

export const HITSLOP = { top: 20, bottom: 20, left: 40, right: 40 }
export const HITSLOP_SQUARE = { top: 20, bottom: 20, left: 40, right: 40 }
export const HITSLOP_SQUARE_LEFT = { top: 20, bottom: 20, left: 40 }
export const HITSLOP_SQUARE_RIGHT = { top: 20, bottom: 20, right: 40 }
export const HITSLOP_SQUARE_TOP = { top: 20, bottom: 20, left: 10, right: 10 }

export const HITSLOP_SMALL = { top: 10, bottom: 10, left: 10, right: 10 }

export default createStylesField
