import { StyleSheet } from 'react-native'
import { themeType } from '../types'

/*
Almost exclusively styling for the way text should look
 */
// eslint-disable-next-line no-undef
const createStylesHome = (theme: themeType) => StyleSheet.create({
  header: {
    backgroundColor: theme.headerColor,
    height: 130,
    justifyContent: 'center',
    width: '100%',
    flexDirection: 'row',
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3
  },
  standardHeader: {
    backgroundColor: theme.backgroundSubtle,
    height: 130,
    justifyContent: 'center',
    width: '100%',
    flexDirection: 'row',
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3
  },
  headerText: {
    color: theme.backgroundAccentGrey,
    fontSize: 20,
    fontFamily: 'Inter_600SemiBold',
    textAlign: 'center'
  },
  heatMapText: {
    color: theme.backgroundSubtle,
    fontSize: 12,
    fontFamily: 'Inter_400Regular',
    textAlign: 'center',
    flex: 1
  },
  buttonText: {
    color: theme.buttonText,
    fontSize: 16,
    fontFamily: 'Inter_500Medium',
    textAlign: 'center'
  },
  greyButtonText: {
    color: theme.main,
    fontSize: 14,
    fontFamily: 'Inter_500Medium',
    textAlign: 'center'
  },
  subButtonText: {
    color: theme.backgroundSubtle,
    fontSize: 18,
    fontFamily: 'Inter_500Medium',
    textAlign: 'center'
  },
  loadingText: {
    color: theme.mytokenColor,
    fontSize: 10,
    fontFamily: 'Inter_600SemiBold',
    textAlign: 'center'
  },
  secureText: {
    color: theme.backgroundSubtle,
    fontSize: 18,
    fontFamily: 'Inter_600SemiBold',
    textAlign: 'center'
  },
  contentText: {
    color: theme.main,
    fontSize: 16,
    fontFamily: 'Inter_400Regular',
    textAlign: 'center'
  },
  smallText: {
    color: theme.mainOpposite,
    fontSize: 13,
    fontFamily: 'Inter_400Regular'
  },
  smallGrayText: {
    color: theme.backgroundGrey,
    fontSize: 13,
    fontFamily: 'Inter_400Regular',
    textAlign: 'center'
  },
  mediumText: {
    color: theme.backgroundSubtle,
    fontSize: 16,
    fontFamily: 'Inter_400Regular',
    textAlign: 'center'
  },
  mediumGrayText: {
    color: theme.backgroundGrey,
    fontSize: 14,
    fontFamily: 'Inter_400Regular',
    textAlign: 'center'
  },
  greyedOutText: {
    color: theme.backgroundGrey,
    fontFamily: 'Inter_500Medium',
    textAlign: 'center'
  },
  welcomeText: {
    color: theme.backgroundSubtle,
    fontSize: 26,
    fontFamily: 'Inter_400Regular',
    textAlign: 'center'
  }
})
export default createStylesHome
