import { DOMAIN_URLS, MasterToken } from '../../types'
import { getDataEncrypted, storeDataEncrypted } from './localManipulation'
import { DOMAIN_USED_KEY, identitiesKey } from '../constants/localUnsecureLocationConstants'

export const localStoreIdentitiesList = async (newList: MasterToken[], password:string) => {
  await storeDataEncrypted(newList, identitiesKey, password)
}
export const localFetchMasterTokenList = async (password:string) => {
  return await getDataEncrypted(identitiesKey, password)
}

export const localStoreDomainList = async (newList: DOMAIN_URLS[], password:string) => {
  await storeDataEncrypted(newList, DOMAIN_USED_KEY, password)
}
export const localFetchDomainList = async (password:string) => {
  return await getDataEncrypted(DOMAIN_USED_KEY, password)
}
