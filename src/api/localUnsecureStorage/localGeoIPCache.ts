import { DOMAIN_URLS, IP_LOCATION, LOCATION_INFO } from '../../types'
import { getIPLocationInfo } from '../axiosRequest'
import {
  cacheSizeLimit,
  IP_LOCATION_KEY,
  level1SizeLimit,
  REMOVE_CACHED_TIME
} from '../constants/localUnsecureLocationConstants'
import { getData, storeData } from './localManipulation'

/*
This file is used to cache GeoIP locations in unsecure storage and memory on first read
 */

let level1:IP_LOCATION[] = []

export const resetLevel1 = () => {
  level1 = []
}
export const getIPLocation = async (ip: string, masterToken: string, domainUrl: DOMAIN_URLS) => {
  // check level1Cache
  const level1Index = level1.findIndex(value => value.ip === ip)
  if (level1Index > -1) {
    return level1[level1Index].info
  }
  let level2:IP_LOCATION[] = await getData(IP_LOCATION_KEY)
  if (!level2) {
    // init long term cache
    await storeData([], IP_LOCATION_KEY)
    level2 = []
  }
  // check level2Cache
  const level2Index = level2.findIndex(value => value.ip === ip)
  if (level2Index > -1) {
    if (new Date().getTime() - new Date(level2[level2Index].used).getTime() > REMOVE_CACHED_TIME) {
      const tempData = level2.filter(x => x.ip !== level2[level2Index].ip)
      await storeData(tempData, IP_LOCATION_KEY)
    } else {
      level1.push(level2[level2Index])
      return level2[level2Index].info
    }
  }

  // get location from server
  const location:LOCATION_INFO = await getIPLocationInfo(ip, masterToken, domainUrl)
  const newLocation:IP_LOCATION = {
    info: location,
    ip,
    used: new Date()
  }
  await cacheNewIPLocation(newLocation, level2)
  if (level1.length < level1SizeLimit) {
    level1.push(newLocation)
  }
  return location
}

const cacheNewIPLocation = async (ipLocation: IP_LOCATION, level2:IP_LOCATION[]) => {
  // getStorage
  let data = level2.slice()
  if (data.length >= cacheSizeLimit) {
    data = await clearCacheElements(10, data)
  }
  data.push(ipLocation)
  await storeData(data, IP_LOCATION_KEY)
}

// remove amount elements from sorted array of elements
const clearCacheElements = async (amount: number, level2:IP_LOCATION[]) => {
  const tempData = level2.slice()
  tempData.sort((a: IP_LOCATION, b: IP_LOCATION) => {
    return new Date(a.used).getTime() - new Date(b.used).getTime()
  })
  tempData.splice(0, amount)
  return tempData
}

export const testingClearCacheElements = async (amount: any, level2:any) => {
  return await clearCacheElements(amount, level2)
}
