import { DOMAIN_URLS, MasterToken } from '../../types'
import { checkDomain, getDomainAddressesExternal } from '../axiosRequest'

/*
This file is used to manage domain addresses in context and storage
 */

export const getDomainAddresses = async (uDomainUrlUnique: string, domainList: DOMAIN_URLS[], setDomainList: any, setVisible?: boolean) => {
  let domainUrlUnique = uDomainUrlUnique
  if (uDomainUrlUnique.charAt(uDomainUrlUnique.length - 1) !== '/') {
    domainUrlUnique = uDomainUrlUnique + '/'
  }
  const level1Index = domainList.findIndex(value => value.domainUrl === domainUrlUnique)
  if (level1Index > -1) {
    if (setVisible && !domainList[level1Index].visible) {
      domainList[level1Index].visible = true
      setDomainList(domainList.slice())
    }
    return domainList[level1Index]
  }
  // get location from server
  if (!await checkDomain(domainUrlUnique)) {
    throw new Error('No domain')
  }
  const domainUrls = await getDomainAddressesExternal(domainUrlUnique)
  domainList.push(domainUrls)
  setDomainList(domainList.slice())
  return domainUrls
}
export const deleteDomainAddress = (domainUrl: DOMAIN_URLS, domainList:DOMAIN_URLS[], setDomainList:any, masterTokens:MasterToken[]) => {
  const masterDomainSet = new Set(masterTokens.map(token => token.domainUrl))
  // filter out domains not present in the masterDomainSet
  const newDomainList = domainList.filter(dom => masterDomainSet.has(dom.domainUrl))

  const indexMaster = masterTokens.findIndex(x => x.domainUrl === domainUrl.domainUrl)
  const indexDomain = newDomainList.findIndex(value => value.domainUrl === domainUrl.domainUrl)
  if (indexMaster !== -1) {
    newDomainList[indexDomain].visible = false
  }
  setDomainList(newDomainList.slice())
}

export const resetDomain = async (setDomainList:any) => {
  await setDomainList([])
}
