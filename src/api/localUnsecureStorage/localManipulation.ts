import AsyncStorage from '@react-native-async-storage/async-storage'
import CryptoES from 'crypto-es'

import { setFirstSetupLocalStorage } from './localConfigurations'
import { resetLevel1 } from './localGeoIPCache'
import { deletePassword } from '../localSecureStorage'

/*
This file is used to change, delete and manipulate local unsecure storage
 */

export const storeData = async (value:any, key: string) => {
  try {
    const jsonValue = JSON.stringify(value)
    await AsyncStorage.setItem(key, jsonValue)
  } catch (e) {
    // saving error
  }
}

export const getData = async (key:string) => {
  try {
    const value = await AsyncStorage.getItem(key)
    if (value !== null) {
      return JSON.parse(value)
    } else {
      return null
    }
  } catch (e) {
    console.log('error')
  }
}
export const removeData = async (key:string) => {
  try {
    await AsyncStorage.removeItem(key)
  } catch (e) {
    console.log('error')
  }
}

export const storeDataEncrypted = async (value:any, key: string, encryptionKey: string) => {
  try {
    const encrypted = CryptoES.AES.encrypt(JSON.stringify(value), encryptionKey)
    await AsyncStorage.setItem(key, JSON.stringify(encrypted))
  } catch (e) {
    console.log('failed to store the encrypted item', e)
  }
}

export const getDataEncrypted = async (key:string, encryptionKey: string) => {
  try {
    const value = await AsyncStorage.getItem(key)
    if (value !== null) {
      const parsed = JSON.parse(value)
      const decrypted = CryptoES.AES.decrypt(parsed, encryptionKey)
      return JSON.parse(decrypted.toString(CryptoES.enc.Utf8))
    } else {
      return null
    }
  } catch (e) {
    console.log('failed to read the encrypted item', e)
  }
}

export const resetUnsecureStorage = async () => {
  try {
    await AsyncStorage.clear()
  } catch (e) {
    console.log('error deleting async storage')
  }
  resetLevel1()
}

export const resetData = async () => {
  await deletePassword()
  await setFirstSetupLocalStorage(false)
  await resetUnsecureStorage()
}
