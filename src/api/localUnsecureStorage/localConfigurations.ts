import { setupKey, themeKey } from '../constants/localUnsecureLocationConstants'
import AsyncStorage from '@react-native-async-storage/async-storage'

const signature = '.mytokenapp.'

/*
This file is used to configure various variables stored in unsecure storage
 */

export const firstSetupCheck = async () => {
  try {
    const jsonValue = await AsyncStorage.getItem(signature + setupKey)
    return jsonValue == null
  } catch (e) {
    return false
  }
}

export const setFirstSetupLocalStorage = async (boolean: boolean) => {
  if (!boolean) {
    try {
      await AsyncStorage.removeItem(signature + setupKey)
    } catch (e) {
      console.log('deleting user login failed')
    }
  } else {
    await AsyncStorage.setItem(signature + setupKey, JSON.stringify(boolean))
  }
}
export const themeCheck = async ():Promise<'light' | 'dark'> => {
  try {
    const jsonValue = await AsyncStorage.getItem(signature + themeKey)
    if (jsonValue === 'dark') {
      return 'dark'
    } else {
      return 'light'
    }
  } catch (e) {
    return 'light'
  }
}

export const setThemeLocal = async (theme: 'dark' | 'light') => {
  try {
    await AsyncStorage.setItem(signature + themeKey, theme)
  } catch (e) {
    console.log('deleting theme failed')
  }
}
