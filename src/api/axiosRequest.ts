import {
  generateRequestBodyMasterToken,
  generateRequestBodyPolling,
  generateRequestBodySubToken,
  generateRequestBodyTokenInfo,
  generateRequestBodyTokenRevocation,
  getTokenTags
} from './generateBody'
import * as itself from './axiosRequest'
import {
  DOMAIN_URLS,
  LOCATION_INFO,
  OpenIDProvider,
  SubToken,
  TEMPLATE,
  templateType,
  TOKEN_CAPABILITIES,
  TOKEN_EVENT
} from '../types'
import axios from 'axios'
import { GEO_LOCATION_URL, MYTOKEN_SERVER_CONFIG } from './constants/domain_url'
import {
  ACTION_TAG_HISTORY,
  ACTION_TAG_INTROSPECT, ACTION_TAG_LIST,
  NAME_APPEND
} from './constants/request_body_defaults'
import { TOKEN_CAPABILITIES_TAGS, TOKEN_CAPABILTIES_FALSE } from './constants/token_capabilities_constants'
import * as Device from 'expo-device'

/*
This file is in charge of handling all axios requests to external server.
It parses the data and returns it in the type required.
Need to import the module into itself so mocking can be done for tests!
 */

export const TIMEOUT = 2000
const config = { timeout: TIMEOUT, headers: { 'User-Agent': 'mytoken-app:', 'Content-Type': 'application/json', Connection: 'keep-alive' } } // + process.env.npm_package_version } }
export const getConfirm = async (provider: OpenIDProvider, domainURL:DOMAIN_URLS) => {
  const body = generateRequestBodyMasterToken(provider, Device.deviceName || 'Unknown App')
  const r = await axios.post(domainURL.creationPath, body, config)
  return [r.data.polling_code, r.data.consent_uri, r.data.expires_in]
}

const makePollingRequest = async (body: string, domainURL:DOMAIN_URLS) => {
  try {
    const data = await axios.post(domainURL.creationPath, body, config)
    return [data.data.mytoken, data.data.mom_id]
  } catch (e) {
    return undefined
  }
}

export const polling = async (pollingCode: string, domainURL:DOMAIN_URLS) => {
  const body = generateRequestBodyPolling(pollingCode)
  return await makePollingRequest(body, domainURL)
}

export const getTransferCode = async (name: string, masterToken: string, capabilities: TOKEN_CAPABILITIES, domainURL:DOMAIN_URLS, profile: TEMPLATE, capabilitiesTemplate: TEMPLATE, restrictionTemplate: TEMPLATE, rotationTemplate: TEMPLATE, tokenExpiration?: Date) => {
  const capabilitiesArray: string[] = getTokenTags(capabilities)
  const body = generateRequestBodySubToken(name, masterToken, capabilitiesArray, profile, capabilitiesTemplate, restrictionTemplate, rotationTemplate, tokenExpiration)
  const r = await axios.post(domainURL.creationPath, body, config)
  return {
    transferCode: r.data.transfer_code,
    momID: r.data.mom_id,
    expiration: r.data.expires_in
  }
}

export const getTokenData = async (masterToken: string, momID:string, subTokenMomID: string, domainURL:DOMAIN_URLS) => {
  const data: SubToken[] = await itself.getAllTokens(masterToken, momID, domainURL)
  const index = data.findIndex(i => i.name === subTokenMomID)
  if (index === -1) {
    // handle
  }
  return data[index]
}

export const getTokenExpiration = async (masterToken: string, domainURL:DOMAIN_URLS) => {
  const data = await itself.getTokenInfoForAction(ACTION_TAG_INTROSPECT, masterToken, domainURL)
  return data.token.exp ? new Date(data.token.exp * 1000) : undefined
}

export const getAllTokens = async (masterToken: string, momID: string, domainURL:DOMAIN_URLS) => {
  const data = await itself.getTokenInfoForAction(ACTION_TAG_LIST, masterToken, domainURL)
  let arrayParsed: SubToken[] = []
  if (!Array.isArray(data.mytokens)) {
    throw new Error()
  }
  data.mytokens.forEach((tokenDataUnparsed: any) => {
    if (tokenDataUnparsed.token.mom_id === momID) {
      arrayParsed = arrayParsed.concat(parseChild(tokenDataUnparsed, masterToken))
    } else {
      arrayParsed.push({
        name: tokenDataUnparsed.token.name,
        momID: tokenDataUnparsed.token.mom_id,
        children: parseChild(tokenDataUnparsed, masterToken),
        parent: masterToken,
        creation: new Date(Number(tokenDataUnparsed.token.created) * 1000),
        expiration: tokenDataUnparsed.token.expires_at ? new Date(Number(tokenDataUnparsed.token.expires_at) * 1000) : undefined,
        external: true
      })
    }
  })
  return arrayParsed
}
const parseChild = (data: any, masterToken: string) => {
  const arrayParsed: SubToken[] = []
  if (data.children) {
    data.children.map((tokenDataUnparsed: any) => {
      let tempName = tokenDataUnparsed.token.name
      if (tempName.startsWith(NAME_APPEND)) {
        tempName = tempName.replace(NAME_APPEND, '')
      }
      return arrayParsed.push({
        momID: tokenDataUnparsed.token.mom_id,
        children: parseChild(tokenDataUnparsed, masterToken),
        parent: masterToken,
        creation: new Date(tokenDataUnparsed.token.created * 1000),
        expiration: tokenDataUnparsed.token.expires_at ? new Date(tokenDataUnparsed.token.expires_at * 1000) : undefined,
        name: tempName
      })
    }
    )
  }
  return arrayParsed
}

export const getTokenHistory = async (masterToken: string, domainURL:DOMAIN_URLS, momID?: string, recursive?: boolean) => {
  const data = await itself.getTokenInfoForAction(ACTION_TAG_HISTORY, masterToken, domainURL, momID, recursive)
  const arrayParsed: TOKEN_EVENT[] = []

  if (data.events) {
    data.events.forEach((tokenDataUnparsed: any) => {
      arrayParsed.push({
        event: tokenDataUnparsed.event,
        ip: tokenDataUnparsed.ip,
        time: new Date(tokenDataUnparsed.time * 1000)
      })
    }
    )
  }
  return arrayParsed
}

export const getTokenInfoForAction = async (action: string, masterToken: string, domainURL:DOMAIN_URLS, momID?: string, recursive?: boolean) => {
  if (action === ACTION_TAG_HISTORY) {
    const body1 = generateRequestBodyTokenInfo(action, masterToken, momID, recursive)
    const r1 = await axios.post(domainURL.tokenInfoPath, body1, config)
    return r1.data
  }
  const body = generateRequestBodyTokenInfo(action, masterToken, momID)
  const r = await axios.post(domainURL.tokenInfoPath, body, config)
  return r.data
}

export const revokeToken = async (masterToken: string, recursive: boolean, domainURL:DOMAIN_URLS, momID?: string) => {
  const body = generateRequestBodyTokenRevocation(masterToken, recursive, momID)
  const r = await axios.post(domainURL.tokenRevokePath, body, config)
  return r.data
}

export const getIPLocationInfo = async (ip: string, masterToken: string, domainUrl: DOMAIN_URLS) => {
  let locationInfo:LOCATION_INFO
  const response = await axios.post(GEO_LOCATION_URL, {
    ip,
    masterToken,
    domainUrl: domainUrl.tokenInfoPath
  }, { ...config, timeout: TIMEOUT * 2 })
  if (response.data.body) {
    locationInfo = {
      longitude: response.data.body.ll[1],
      latitude: response.data.body.ll[0],
      city: (response.data.body.city ? response.data.body.city + ', ' : '') + response.data.body.country
    }
  } else {
    locationInfo = {
      longitude: undefined,
      latitude: undefined,
      city: undefined
    }
  }
  return locationInfo
}

export const checkDomain = async (url: string) => {
  const response = await axios.get(url + MYTOKEN_SERVER_CONFIG, config)
  return response.data.issuer !== undefined
}

export const getDomainAddressesExternal = async (url: string) => {
  const r = await axios.get(url + MYTOKEN_SERVER_CONFIG, config)
  return {
    visible: true,
    domainUrl: url,
    creationPath: r.data.mytoken_endpoint,
    tokenInfoPath: r.data.tokeninfo_endpoint,
    tokenRevokePath: r.data.revocation_endpoint,
    templatesPath: r.data.profiles_endpoint
  }
}

export const getProviderList = async (domainUrl: DOMAIN_URLS) => {
  const tempList:OpenIDProvider[] = []
  const r = await axios.get(domainUrl.domainUrl + MYTOKEN_SERVER_CONFIG, config)
  r.data.providers_supported.forEach((value: any) => {
    tempList.push({
      name: value.name,
      url: value.issuer
    })
  })
  return tempList
}

const getTemplatesForGroup = async (url: string) => {
  return await axios.get(url, config)
}

export const getTemplateList = async (domainUrl: DOMAIN_URLS, templateUsed: templateType) => {
  const tempList:TEMPLATE[] = []
  const r = await axios.get(domainUrl.templatesPath, config)
  if (!r.data) {
    return []
  }
  for (const value of r.data) {
    const content = await getTemplatesForGroup(domainUrl.templatesPath + '/' + value + '/' + templateUsed)
    if (content.data) {
      content.data.forEach((template: any) => {
        tempList.push({
          name: template.name,
          templateType: templateUsed,
          capabilities: templateUsed === templateType.PROFILE ? parseCapabilities(template.payload.capabilities) : templateUsed === templateType.CAPTABILITIES ? parseCapabilities(template.payload) : undefined,
          restrictions: templateUsed === templateType.PROFILE ? parseRestrictions(template.payload.restrictions) : templateUsed === templateType.RESTRICTIONS ? parseRestrictions(template.payload) : undefined
        })
      })
    }
  }
  return tempList
}

const parseCapabilities = (payload: any) => {
  if (!payload) {
    return undefined
  }
  let tempCapabilities:TOKEN_CAPABILITIES = TOKEN_CAPABILTIES_FALSE
  if (payload.includes(TOKEN_CAPABILITIES_TAGS.obtain_tokens)) {
    tempCapabilities = { ...tempCapabilities, obtain_tokens: true }
  }
  if (payload.includes(TOKEN_CAPABILITIES_TAGS.token_info)) {
    tempCapabilities = { ...tempCapabilities, token_info: true }
  }
  if (payload.includes(TOKEN_CAPABILITIES_TAGS.create_new_token)) {
    tempCapabilities = { ...tempCapabilities, create_new_token: true }
  }
  if (payload.includes(TOKEN_CAPABILITIES_TAGS.read_write_settings)) {
    tempCapabilities = { ...tempCapabilities, read_write_settings: true }
  }
  if (payload.includes(TOKEN_CAPABILITIES_TAGS.manage_mytokens)) {
    tempCapabilities = { ...tempCapabilities, manage_mytokens: true }
  }
  return tempCapabilities
}

const parseRestrictions = (payload: any) => {
  if (!payload) {
    return undefined
  }
  const index = payload.findIndex((value:any) => value.exp !== undefined)
  if (index > -1) {
    return payload[index].exp
  }
  return 0
}

export const testParseCapabilities = (payload: any) => {
  parseCapabilities(payload)
}
