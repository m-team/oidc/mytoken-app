import { API_REQUEST_REVOCATION, OpenIDProvider, TEMPLATE, TOKEN_CAPABILITIES, TOKEN_INFO_REQUEST } from '../types'
import { TOKEN_CAPABILITIES_TAGS, TOKEN_EVENTS_TAGS } from './constants/token_capabilities_constants'
import {
  ACTION_TAG_HISTORY,
  API_REQUEST_DEFAULT_PARAMS,
  API_REQUEST_MASTER_PARAMS,
  NAME_APPEND,
  POLLING_BODY
} from './constants/request_body_defaults'

/*
This file creates request bodies for the axios requests (since most are Axios post)
 */

export const getTokenTags = function (capabilities: TOKEN_CAPABILITIES) {
  const capabilitiesArray: string[] = []
  if (capabilities.obtain_tokens) { capabilitiesArray.push(TOKEN_CAPABILITIES_TAGS.obtain_tokens) }
  if (capabilities.token_info) { capabilitiesArray.push(TOKEN_CAPABILITIES_TAGS.token_info) }
  if (capabilities.create_new_token) { capabilitiesArray.push(TOKEN_CAPABILITIES_TAGS.create_new_token) }
  if (capabilities.read_write_settings) { capabilitiesArray.push(TOKEN_CAPABILITIES_TAGS.read_write_settings) }
  if (capabilities.manage_mytokens) { capabilitiesArray.push(TOKEN_CAPABILITIES_TAGS.manage_mytokens) }
  return capabilitiesArray
}

export const generateRequestBodySubToken = function (
  name: string,
  masterToken: string,
  capabilitiesArray: string[],
  profile: TEMPLATE,
  capabilitiesTemplate: TEMPLATE,
  restrictionTemplate: TEMPLATE,
  rotationTemplate: TEMPLATE,
  tokenExpiration?: Date) {
  const requestSubToken = API_REQUEST_DEFAULT_PARAMS
  requestSubToken.name = NAME_APPEND + name
  requestSubToken.mytoken = masterToken
  if (tokenExpiration) {
    requestSubToken.restrictions = { exp: Math.floor(tokenExpiration.getTime() / 1000) }
  }
  if (profile.name !== 'DEFAULT') {
    requestSubToken.include = profile.name
  }
  if (capabilitiesTemplate.name !== 'DEFAULT') {
    requestSubToken.capabilities = '@' + capabilitiesTemplate.name
  } else {
    requestSubToken.capabilities = capabilitiesArray
  }
  if (restrictionTemplate.name !== 'DEFAULT') {
    requestSubToken.restrictions = restrictionTemplate.name
  }
  if (rotationTemplate.name !== 'DEFAULT') {
    requestSubToken.rotation = rotationTemplate.name
  }
  return JSON.stringify(requestSubToken)
}

export const generateRequestBodyMasterToken = function (provider: OpenIDProvider, name: string) {
  const requestMasterToken = API_REQUEST_MASTER_PARAMS
  requestMasterToken.name = NAME_APPEND + name
  requestMasterToken.oidc_issuer = provider.url
  return JSON.stringify(requestMasterToken)
}

export const generateRequestBodyPolling = function (pollingCode: string) {
  const requestPolling = POLLING_BODY
  requestPolling.polling_code = pollingCode
  return JSON.stringify(requestPolling)
}

export const generateRequestBodyTokenInfo = function (action: string, masterToken: string, momID?: string, recursive?: boolean) {
  let requestTokenInfo: TOKEN_INFO_REQUEST
  if (action === ACTION_TAG_HISTORY) {
    requestTokenInfo = {
      action,
      mytoken: masterToken,
      mom_ids: [momID && recursive ? `children@${momID}` : '', momID || '']
    }
  } else {
    requestTokenInfo = {
      action,
      mytoken: masterToken,
      mom_id: momID
    }
  }
  return JSON.stringify(requestTokenInfo)
}
export const generateRequestBodyTokenRevocation = function (masterToken: string, recursive: boolean, momID?: string) {
  const requestTokenInfo: API_REQUEST_REVOCATION = {
    token: masterToken,
    mom_id: momID,
    recursive: true
  }
  return JSON.stringify(requestTokenInfo)
}

export const getTagsForEvents = function (tokenHistory:boolean, tokenChildren:boolean, tokenRotation:boolean) {
  const allowEvents: string[] = []
  if (tokenHistory) {
    allowEvents.push(TOKEN_EVENTS_TAGS.token_history)
  }
  if (tokenChildren) {
    allowEvents.push(TOKEN_EVENTS_TAGS.token_children)
  }
  if (tokenRotation) {
    allowEvents.push(TOKEN_EVENTS_TAGS.token_rotated)
  }
  return allowEvents
}
// refactor once all tags are known
export const getTagsForEventsIgnore = function (tokenHistory:boolean, tokenChildren:boolean, tokenRotation:boolean) {
  const allowEvents: string[] = []
  if (!tokenHistory) {
    allowEvents.push(TOKEN_EVENTS_TAGS.token_history)
  }
  if (!tokenChildren) {
    allowEvents.push(TOKEN_EVENTS_TAGS.token_children)
  }
  if (!tokenRotation) {
    allowEvents.push(TOKEN_EVENTS_TAGS.token_rotated)
  }
  return allowEvents
}
