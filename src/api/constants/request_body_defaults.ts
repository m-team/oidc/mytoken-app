import { EGI_URL } from './oidc_issuers_constants'
import { API_REQUEST_MASTER_TOKEN, API_REQUEST_SUBTOKEN, PollingBody, TOKEN_EVENT_NAME } from '../../types'

export const NAME_APPEND = 'mytoken-app/'
export const API_REQUEST_DEFAULT_PARAMS: API_REQUEST_SUBTOKEN = {
  name: '',
  grant_type: 'mytoken',
  mytoken: '',
  capabilities: ['AT', 'tokeninfo'],
  response_type: 'transfer_code'
}

export const API_REQUEST_MASTER_PARAMS: API_REQUEST_MASTER_TOKEN = {
  name: '',
  oidc_issuer: EGI_URL.url,
  grant_type: 'oidc_flow',
  oidc_flow: 'authorization_code',
  client_type: 'native',
  restrictions: [{}],
  capabilities: ['AT', 'tokeninfo', 'manage_mytokens', 'create_mytoken', 'settings'],
  application_name: 'Mytoken app',
  response_type: 'short_token'
}

export const POLLING_BODY: PollingBody = {
  grant_type: 'polling_code',
  polling_code: ''
}

export const ACTION_TAG_INTROSPECT = 'introspect'
// export const ACTION_TAG_SUBTOKENS = 'subtokens'
export const ACTION_TAG_LIST = 'list_mytokens'
export const ACTION_TAG_HISTORY = 'event_history'

export const CREATED_EVENT = 'created'

export const EVENTS_TAGS: TOKEN_EVENT_NAME[] = [{
  TAG: 'created',
  display: 'Created'
}, {
  TAG: 'tokeninfo_introspect',
  display: 'Token Info Fetched'
},
{
  TAG: 'transfer_code_used',
  display: 'Transfer Code Used'
}]
