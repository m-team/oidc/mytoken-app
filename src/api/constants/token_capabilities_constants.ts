import { TOKEN_CAPABILITIES, TOKEN_EVENT_TAGS } from '../../types'

interface TOKEN_CAPABILITES_TAGS_TYPES {
    obtain_tokens: string,
    token_info: string,
    create_new_token: string,
    read_write_settings: string,
    manage_mytokens: string
}

export const TOKEN_CAPABILTIES_DEFAULTS: TOKEN_CAPABILITIES = {
  obtain_tokens: true,
  token_info: true,
  create_new_token: false,
  read_write_settings: false,
  manage_mytokens: false
}

export const TOKEN_CAPABILTIES_FALSE: TOKEN_CAPABILITIES = {
  obtain_tokens: false,
  token_info: false,
  create_new_token: false,
  read_write_settings: false,
  manage_mytokens: false
}

export const TOKEN_CAPABILITIES_TAGS: TOKEN_CAPABILITES_TAGS_TYPES = {
  obtain_tokens: 'AT',
  token_info: 'tokeninfo',
  create_new_token: 'create_mytoken',
  read_write_settings: 'settings',
  manage_mytokens: 'manage_mytokens'
}
export const TOKEN_EVENTS_TAGS: TOKEN_EVENT_TAGS = {
  token_history: 'tokeninfo_history',
  token_rotated: 'token_rotated',
  token_children: 'tokeninfo_subtokens'
}
