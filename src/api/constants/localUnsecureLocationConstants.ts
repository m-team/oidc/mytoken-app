export const IP_LOCATION_KEY = 'ip_location_cache'
export const cacheSizeLimit = 200

export const REMOVE_CACHED_TIME = 604800000
export const level1SizeLimit = 100
export const identitiesKey = 'identities'
export const DOMAIN_USED_KEY = 'url_domain_used'
export const setupKey = 'setupKey'

export const themeKey = 'themeKey'
