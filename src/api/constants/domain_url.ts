export const DEV_DOMAIN_URL_KIT = 'https://mytoken-dev.vm.fedcloud.eu/'
export const DOMAIN_URL_EU = 'https://mytok.eu'

// add domain
export const DOMAIN_URL_KIT = 'https://mytoken.data.kit.edu/'

export const MYTOKEN_SERVER_CONFIG = '.well-known/mytoken-configuration'
export const GEO_LOCATION_URL = 'https://metrics.data.kit.edu'
