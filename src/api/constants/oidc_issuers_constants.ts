import { OpenIDProvider } from '../../types'

export const EGI_URL: OpenIDProvider = {
  name: 'EGI',
  url: 'https://aai.egi.eu/auth/realms/egi'
}
