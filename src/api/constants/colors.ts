import { themeType } from '../../types'

export interface colorPS {
    primary: string
    secondary: string
}

export const DEFAULT_COLOR:colorPS = {
  primary: '#D07132',
  secondary: '#E9CCB5'
}

export const colorsArr = [
  DEFAULT_COLOR,
  { primary: '#EE964B', secondary: '#FFE0C4' },
  { primary: '#735751', secondary: '#9A9999' },
  { primary: '#9D5A38', secondary: '#AB9285' },
  // { primary: '#7D7D7D', secondary: '#D1DEDE' },
  // { primary: '#22333B', secondary: '#7A98A5' },
  { primary: '#33658A', secondary: '#A3BCD0' },
  { primary: '#607744', secondary: '#C4D6AE' },
  { primary: '#2E8B57', secondary: '#7BE2C0' },
  { primary: '#32DE8A', secondary: '#BFF5DB' },
  // { primary: '#6E4F78', secondary: '#D2B2DC' },
  { primary: '#735CDD', secondary: '#CEC9E7' },
  // { primary: '#F76F8E', secondary: '#FFC2D0' },
  // { primary: '#F1C40F', secondary: '#FFEDA5',
  { primary: '#FF3C38', secondary: '#FFB9B8' }]

export const lightTheme: themeType = {
  mainBackground: '#ffffff',
  searchDarkBorder: '#c4c4c4',
  borderTextEntry: '#aeaeb4',
  advancedView: '#aeaeb4',
  textEntryContent: '#F6F6F6',
  pressedOptionColor: '#D9D9D9',
  standardHeaderColor: '#aeaeb4',
  subTitleColor: '#aeaeb4',
  lockedBoxColor: '#BEBEBE',
  dividerColor: '#D9D9D9',
  headerColor: '#264653',
  smallHeaderColor: '#171717',
  mytokenColor: '#D07132',
  backgroundGrey: '#aeaeb4',
  backgroundAccentGrey: '#D9D9D9',
  backgroundSubtle: '#264653',
  heatmapColor: '#F0F0F0',
  neutral: '#F6F6F6',
  buttonText: '#ffffff',
  searchBack: '#677780',
  lightback: '#F2F2F2',
  dateColor: '#EFEFEF',
  mainOpposite: '#171717',
  main: '#ffffff',
  borderCalm: '#c4c4c4',
  accentColor: '#b3b4b5',
  accentStrong: '#989898',
  modalColor: '#fff'
}
export const darkTheme: themeType = {
  mainBackground: '#2B3E50',
  searchDarkBorder: '#677780',
  advancedView: '#D0D0D0',
  borderTextEntry: '#73838C',
  textEntryContent: '#162931',
  pressedOptionColor: '#162931',
  lockedBoxColor: '#73838C',
  heatmapColor: '#515E66',
  buttonText: '#264653',
  standardHeaderColor: '#D0D0D0',
  dividerColor: '#515C62',
  subTitleColor: '#8E8E8E',
  smallHeaderColor: '#D0D0D0',
  headerColor: '#2B3E50',
  mytokenColor: '#D07132',
  backgroundGrey: '#73838C',
  backgroundAccentGrey: '#D0D0D0',
  backgroundSubtle: '#D0D0D0',
  neutral: '#CCCCCC',
  searchBack: '#505050',
  lightback: '#4A4A4A',
  dateColor: '#808080',
  mainOpposite: '#E6E6E6',
  main: '#2B3E50',
  borderCalm: '#505050',
  accentColor: '#808080',
  accentStrong: '#FFFFFF',
  modalColor: '#162931'
}

export const mapStylingDark = [
  {
    elementType: 'geometry',
    stylers: [
      {
        color: '#242f3e'
      }
    ]
  },
  {
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#746855'
      }
    ]
  },
  {
    elementType: 'labels.text.stroke',
    stylers: [
      {
        color: '#242f3e'
      }
    ]
  },
  {
    featureType: 'administrative.locality',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#d59563'
      }
    ]
  },
  {
    featureType: 'poi',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#d59563'
      }
    ]
  },
  {
    featureType: 'poi.park',
    elementType: 'geometry',
    stylers: [
      {
        color: '#263c3f'
      }
    ]
  },
  {
    featureType: 'poi.park',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#6b9a76'
      }
    ]
  },
  {
    featureType: 'road',
    elementType: 'geometry',
    stylers: [
      {
        color: '#38414e'
      }
    ]
  },
  {
    featureType: 'road',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#212a37'
      }
    ]
  },
  {
    featureType: 'road',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#9ca5b3'
      }
    ]
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry',
    stylers: [
      {
        color: '#746855'
      }
    ]
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#1f2835'
      }
    ]
  },
  {
    featureType: 'road.highway',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#f3d19c'
      }
    ]
  },
  {
    featureType: 'transit',
    elementType: 'geometry',
    stylers: [
      {
        color: '#2f3948'
      }
    ]
  },
  {
    featureType: 'transit.station',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#d59563'
      }
    ]
  },
  {
    featureType: 'water',
    elementType: 'geometry',
    stylers: [
      {
        color: '#17263c'
      }
    ]
  },
  {
    featureType: 'water',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#515c6d'
      }
    ]
  },
  {
    featureType: 'water',
    elementType: 'labels.text.stroke',
    stylers: [
      {
        color: '#17263c'
      }
    ]
  }
]
