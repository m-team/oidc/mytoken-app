import * as SecureStore from 'expo-secure-store'
import {
  localFetchMasterTokenList,
  localStoreIdentitiesList
} from './localUnsecureStorage/localMasterSubTokenManager'
import { MasterToken } from '../types'
import { createSalt, encryptItem, getPasswordHash } from '../utilities/cryptoHashHelper'
/*
This files completely manages the secure storage. Secure storage on
IOS uses the keychain where information is stored encrypted.

A typical approach to semi-secure local data storage is creating an aes key, storing it in
secure storage, then using that key to decrypt and encrypt data in async storage.
since we don't need to store a lot of data (currently max 8 identities and max 17 sub tokens),
the even more secure variation is storing everything in secure-storage using the users password
and a fixed signature. This way no additional encryption, an additional layer for flaws, is needed.
 */

const signature = '.mytokenapp.'
const saltStore = 'salt'
const hashStore = 'mytokenpassword'

export const getSalt = async () => {
  const salt = await SecureStore.getItemAsync(signature + saltStore)
  if (!salt) {
    throw new Error()
  }
  return salt
}
export const setNewSalt = async () => {
  const salt = createSalt()
  if (!salt) {
    throw new Error()
  }
  try {
    await SecureStore.setItemAsync(signature + saltStore, salt)
  } catch (e) {
    console.log('error setting salt', e)
  }
}
export const checkPassword = async (password:string) => {
  try { // use hash
    const value = await SecureStore.getItemAsync(signature + hashStore)
    const salt = await getSalt()
    return value === encryptItem(password + salt)
  } catch (e) {
    return false
  }
}

export const storePassword = async (password:string) => {
  // creates new salt every time new password stored
  await setNewSalt()
  const hash = await getPasswordHash(password)
  if (hash) {
    await SecureStore.setItemAsync(signature + hashStore, hash)
  } else {
    throw new Error()
  }
}

export const deletePassword = async () => {
  try {
    await SecureStore.deleteItemAsync(signature + hashStore)
  } catch (e) {
    console.log('error deleting password', e)
  }
}

export const setPasswordStorage = async (password:string, oldPassword?: string) => {
  // move all data from old location
  let currentIdentities:MasterToken[] = []
  if (oldPassword) {
    currentIdentities = await localFetchMasterTokenList(oldPassword)
    await deletePassword()
  }
  await storePassword(password)
  await localStoreIdentitiesList(currentIdentities, password)
}

export const passwordIsSet = async () => {
  try {
    const jsonValue = await SecureStore.getItemAsync(signature + hashStore)
    return jsonValue !== null
  } catch (e) {
    return false
  }
}
