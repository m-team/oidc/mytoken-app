import { DayUse } from '../types'

/*
This file is used by the heatmap to generate labels and a list of data required for the render.
Only untested utility due to simplicity. The functions used are already tested in depth
 */

export const daysLabels = (days: number) => {
  const daysArray: DayUse[] = []
  // make this the real today
  for (let i = days - 1; i >= 0; i--) {
    const day = new Date()
    day.setDate(day.getDate() - i)
    daysArray.push({ date: day, frequency: 0, created: false })
  }
  return daysArray
}

export const monthsLabels = () => {
  // toLocaleString doesn't work on andorid to fetch months
  const allMonths: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
  const months: string[] = []
  for (let i = 4; i >= 0; i--) {
    const today = new Date()
    today.setMonth(today.getMonth() - i)
    months.push(
      // today.toLocaleString('default', { month: 'long' })
      allMonths[today.getMonth()]
    )
  }
  return months
}

export const DAYS = 154

export const sameDay = (first: Date, second: Date) => {
  return first.getDate() === second.getDate() &&
      first.getMonth() === second.getMonth() &&
      first.getFullYear() === second.getFullYear()
}

export const differenceDays = (first: Date, second: Date) => {
  const diff = Math.abs(first.getTime() - second.getTime())
  return Math.ceil(diff / (1000 * 3600 * 24))
}

export const getColor = (frequency: number, created: boolean, defaultColor: string) => {
  let color = defaultColor
  if (created) {
    return '#3CB371'
  }
  // refactor this lazy check (might be good for performance though)
  if (frequency < 1) {
    color = defaultColor
    return color
  }
  if (frequency < 7) {
    color = '#F6E3D6'
    return color
  }
  if (frequency < 14) {
    color = '#ECC6AD'
    return color
  }
  if (frequency <= 20) {
    color = '#E3AA84'
    return color
  }
  if (frequency > 20) {
    color = '#D07132'
  }
  return color
}
