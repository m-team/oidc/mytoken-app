import React from 'react'
import { themeType } from '../types'
import { lightTheme } from '../api/constants/colors'

/*
The context for authentication is defined here. It stores the password if it is entered correctly as
well as the login state of the app.
 */
interface contextParams {
    loggedIn: boolean,
    setLoggedIn: any,
    password: string,
    setPassword: any,
    passSet: boolean,
    setPassSet: any,
    theme: themeType,
    setTheme: any
}

export const AuthContext = React.createContext<contextParams>({
  loggedIn: false,
  setLoggedIn: () => {},
  password: '',
  setPassword: () => {},
  passSet: false,
  setPassSet: () => {},
  theme: lightTheme, // Set the default value for the theme property
  setTheme: () => {}
})
