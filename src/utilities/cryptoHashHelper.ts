import CryptoES from 'crypto-es'
import { getSalt } from '../api/localSecureStorage'

/*
Since mocking CryptoES was an issue, functions moved out so return values can be mocked.
Mocking functions that are inside the file you are mocking is not possible unless you create
cyclical imports. (Partial mocks only work if function call isn't nested)
 */

export const getPasswordHash = async (password:string) => {
  try {
    const salt = await getSalt()
    return CryptoES.SHA3(password + salt).toString(CryptoES.enc.Base64)
  } catch (e) {
    console.log('hashing password failed here', e)
  }
}

export const createSalt = () => {
  return CryptoES.lib.WordArray.random(16).toString()
}

export const encryptItem = (message: string) => {
  return CryptoES.SHA3(message).toString(CryptoES.enc.Base64)
}
