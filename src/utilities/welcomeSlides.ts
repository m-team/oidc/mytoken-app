/*
This is the data the Flatlist for the welcome slides uses in the renderItem function
 */
const mytokenIcon = require('../../assets/mytokenLogo.png')
const openIDIcon = require('../../assets/openIDLogo.png')
const customLogo = require('../../assets/customLogo.png')

export const DATA = [
  {
    image: mytokenIcon,
    text: 'A New Token Type',
    subText: 'Bearer token from any device'
  },
  {
    image: openIDIcon,
    text: 'Obtain Access Tokens',
    subText: 'Finally obtain openID connect authentication tokens for an extended period of time'

  },
  {
    image: customLogo,
    text: 'Transfer Codes',
    subText: 'Create Transfer Codes to transfer newly created tokens'
  }
]
