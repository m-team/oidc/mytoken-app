import { CONNECTION_ERROR, DOMAIN_URLS, MasterToken, OpenIDProvider, SubToken } from '../types'
import { getAllTokens, revokeToken } from '../api/axiosRequest'
import { alertTextOnly } from './alertHelper'
import { resetData } from '../api/localUnsecureStorage/localManipulation'
import { colorPS } from '../api/constants/colors'
import { DEFAULT_PROVIDER } from './main_types_defaults'

/*
This file completely manages the apps main context that stores all required information in memory.
You will find functions that alter the list of subtokens/masterTokens and other functions to manage flags
 */
export function removeMasterToken (masterToken: MasterToken, masterTokenList:MasterToken[], setIdentitiesList:any) {
  const tempArray = masterTokenList.filter(i => i.masterToken !== masterToken.masterToken)
  setIdentitiesList(tempArray)
}
export const filterProviders = (provider: OpenIDProvider, allList: SubToken[], masterTokenList: MasterToken[]) => {
  if (provider === DEFAULT_PROVIDER) return allList
  return allList.filter(r => {
    const tempToken = resolveMasterToken(r.parent, masterTokenList)
    if (!tempToken) return false
    return tempToken.provider.name === provider.name
  })
}
export function filterMasterTokensForProvider (masterTokenList: MasterToken[]): MasterToken[] {
  const providerMap = new Map<string, MasterToken>()
  masterTokenList.forEach(token => {
    const existingToken = providerMap.get(token.provider.name)
    if (!existingToken) {
      providerMap.set(token.provider.name, token)
    } else if (token.expirationDate === undefined ||
        (existingToken.expirationDate !== undefined &&
            new Date(token.expirationDate).getTime() > new Date(existingToken.expirationDate).getTime())) {
      providerMap.set(token.provider.name, token)
    }
  })
  return Array.from(providerMap.values())
}

export function reset (identitiesList:MasterToken[], setSubTokenList:any, setMasterTokenList:any, setFirstSetup:any, domainList: DOMAIN_URLS[]) {
  identitiesList.forEach((value: MasterToken) => {
    revokeToken(value.masterToken, true, resolveDomainURL(value.domainUrl, domainList)).catch(console.error)
  })
  const tempMasterToken: MasterToken[] = []
  const tempSubTokenList: SubToken[] = []
  setMasterTokenList(tempMasterToken)
  setSubTokenList(tempSubTokenList)
  resetData().catch(() => {
    alertTextOnly('Something went wrong resetting the app')
  })
  setFirstSetup(true)
}

export async function updateSubTokenList (masterTokens:MasterToken[], setSubTokenList:any, domainList:DOMAIN_URLS[], setConnectionErrors:any) {
  let tokenType: SubToken[] = []
  const errorArray: CONNECTION_ERROR[] = []
  for (const masterToken of filterMasterTokensForProvider(masterTokens)) {
    if (masterToken.expirationDate && new Date(masterToken.expirationDate) < new Date()) {
      continue
    }
    try {
      const subTokens = await getAllTokens(masterToken.masterToken, masterToken.momID, resolveDomainURL(masterToken.domainUrl, domainList))
      tokenType = tokenType.concat(subTokens)
    } catch (e) {
      errorArray.push({
        masterToken: masterToken.name,
        domainURL: masterToken.domainUrl
      })
    }
  }
  setConnectionErrors(errorArray)
  tokenType.sort((a:SubToken, b:SubToken) => a.creation.getTime() - b.creation.getTime())
  setSubTokenList(tokenType)
}

export function addMasterToken (masterToken: MasterToken, masterTokenList:MasterToken[], setIdentitiesList:any) {
  const tempArray = masterTokenList.slice()
  tempArray.push(masterToken)
  setIdentitiesList(tempArray)
}

export function resolveMasterToken (masterToken:string, masterTokenList:MasterToken[]) {
  if (masterTokenList === undefined) return
  const tempArray:MasterToken[] = masterTokenList.filter(i => i.masterToken === masterToken)
  return tempArray[0]
}
export function updateMasterTokenColor (masterToken: string, color: colorPS, masterTokenList: MasterToken[], setMasterTokenList: any) {
  const index = masterTokenList.findIndex(i => i.masterToken === masterToken)

  if (index > -1) {
    const targetProviderName = masterTokenList[index].provider.name
    // update all colors with the same provider
    const updatedList = masterTokenList.map(token => {
      if (token.provider.name === targetProviderName) {
        return { ...token, color }
      }
      return token
    })
    setMasterTokenList(updatedList)
  } else {
    throw new Error('domain resolve error')
  }
}

export const resolveDomainURL = (url:string, domainURLs:DOMAIN_URLS[]) => {
  const indexDomainURL = domainURLs.findIndex(x => x.domainUrl === url)
  if (indexDomainURL > -1) {
    return domainURLs[indexDomainURL]
  } else {
    throw new Error('domain resolve error')
  }
}
