import { Alert } from 'react-native'

/*
A simple wrapper for *some* alerts to avoid clutter in code.
 */

export const alertTextOnly = (text: string, title?:string) => {
  Alert.alert(title || 'Error', text, [
    {
      text: 'OK',
      onPress: () => {
      }
    }
  ])
}
export const alertTextOnlySuccess = (text: string) => {
  Alert.alert('Success', text, [
    {
      text: 'OK',
      onPress: () => {
      }
    }
  ])
}
