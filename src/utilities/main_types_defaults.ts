import { DOMAIN_URLS, MasterToken, OpenIDProvider, SubToken, TEMPLATE, templateType } from '../types'
import { EGI_URL } from '../api/constants/oidc_issuers_constants'
import { TOKEN_CAPABILTIES_DEFAULTS } from '../api/constants/token_capabilities_constants'
import { DEFAULT_COLOR } from '../api/constants/colors'

export const DEFAULT_SUBTOKEN: SubToken = {
  name: 'DEFAULT',
  creation: new Date(),
  parent: EGI_URL.name,
  expiration: new Date(),
  momID: 'DEFAULT'
}

export const DEFAULT_PROVIDER: OpenIDProvider = {
  name: 'DEFAULT',
  url: 'DEFAULT'
}

export const DEV_KIT_DOMAIN_URLS:DOMAIN_URLS = {
  visible: true,
  domainUrl: 'https://mytoken-dev.vm.fedcloud.eu/',
  creationPath: 'https://mytoken-dev.vm.fedcloud.eu/api/v0/token/my/',
  tokenInfoPath: 'https://mytoken-dev.vm.fedcloud.eu/api/v0/tokeninfo',
  tokenRevokePath: 'https://mytoken-dev.vm.fedcloud.eu/api/v0/token/revoke',
  templatesPath: 'https://mytoken-dev.vm.fedcloud.eu/api/v0/pt'
}

export const KIT_DOMAIN_URLS:DOMAIN_URLS = {
  visible: true,
  domainUrl: 'https://mytoken.data.kit.edu/',
  creationPath: 'https://mytoken.data.kit.edu/api/v0/token/my/',
  tokenInfoPath: 'https://mytoken.data.kit.edu/api/v0/tokeninfo',
  tokenRevokePath: 'https://mytoken.data.kit.edu/api/v0/token/revoke',
  templatesPath: 'https://mytoken.data.kit.edu/api/v0/pt'
}
export const DEFAULT_DOMAIN_URL:DOMAIN_URLS = {
  visible: false,
  domainUrl: 'DEFAULT',
  creationPath: '',
  tokenInfoPath: '',
  tokenRevokePath: '',
  templatesPath: ''
}
export const DEFAULT_MASTER_TOKEN:MasterToken = {
  name: 'DEFAULT',
  creation: new Date(),
  provider: DEFAULT_PROVIDER,
  masterToken: '',
  domainUrl: KIT_DOMAIN_URLS.domainUrl,
  expirationDate: undefined,
  momID: '',
  color: DEFAULT_COLOR
}
export const DEFAULT_TEMPLATE: TEMPLATE = {
  name: 'DEFAULT',
  capabilities: TOKEN_CAPABILTIES_DEFAULTS,
  templateType: templateType.PROFILE
}
