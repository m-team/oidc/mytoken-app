import React from 'react'
import { CONNECTION_ERROR, DOMAIN_URLS, MasterToken, SubToken, TransferCode } from '../types'

/*
The context for the app is defined here. It stores the list of subTokens and MasterTokens, callbacks to edit them
(functions provided when setting them as state in AppAuthenticated) and stores wheather the user logs in for the first
time
 */

interface contextParams {
    masterTokenList: MasterToken[];
    subTokenList: SubToken[];
    setMasterTokenList:any,
    setSubTokenList:any
    firstSetup: any;
    setFirstSetup:any;
    domainList: DOMAIN_URLS[];
    setDomainList: any;
    connectionErrors: CONNECTION_ERROR[];
    setConnectionErrors:any;
    transferCodeList: TransferCode[];
    setTransferCodeList: any;
}

export const MyContext = React.createContext<contextParams>(
  {
    masterTokenList: [],
    subTokenList: [],
    setMasterTokenList: () => {},
    setSubTokenList: () => {},
    firstSetup: null,
    setFirstSetup: () => {},
    domainList: [],
    setDomainList: () => {},
    connectionErrors: [],
    setConnectionErrors: () => {},
    transferCodeList: [],
    setTransferCodeList: () => {}
  }
)
