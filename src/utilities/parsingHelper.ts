import { SubToken, TOKEN_EVENT } from '../types'
import Moment from 'moment'
import { EVENTS_TAGS } from '../api/constants/request_body_defaults'

/*
This file is used to search given lists for a provided search term. Additionaly, as the name suggests a function
 to interpret the event tags into natural language is provided here
 */
const searchParameters = (item: string | undefined, phraseTrimmed: string) => {
  if (!item) {
    return null
  }
  return item.startsWith(phraseTrimmed.toUpperCase()) ||
      item.startsWith(phraseTrimmed.toLowerCase()) ||
      (item.includes(phraseTrimmed) && (phraseTrimmed.length >= 2)) ||
      (item.includes(phraseTrimmed.toUpperCase()) && (phraseTrimmed.length >= 2)) ||
      (item.includes(phraseTrimmed.toLowerCase()) && (phraseTrimmed.length >= 2)) ||
      (item.includes(phraseTrimmed.charAt(0).toUpperCase().concat(phraseTrimmed)) && (phraseTrimmed.length >= 2)) ||
      (item.toLowerCase().includes(phraseTrimmed.toLowerCase()) && (phraseTrimmed.length >= 3))
}

export const searchSubTokenList = (phrase: string, list: SubToken[]) => {
  const phraseTrimmed = phrase.trim()
  if (!phraseTrimmed.length) {
    return list
  }
  return list.filter(i =>
    searchParameters(i.name, phraseTrimmed) ||
        searchParameters(i.parent, phraseTrimmed)
  )
}
export const searchEventHistoryList = (phrase: string, list: TOKEN_EVENT[]) => {
  if (!phrase) {
    return list
  }
  const phraseTrimmed = phrase.trim()
  if (!phraseTrimmed.length) {
    return list
  }

  // If Moment is causing performance issues think about better solution
  return list.filter(i =>
    searchParameters(i.event, phraseTrimmed) ||
        searchParameters(Moment(i.time).format('D.MMM.YY'), phraseTrimmed) ||
      (i.info && i.info.city && searchParameters(i.info.city, phraseTrimmed)) ||
      (i.ip && searchParameters(i.ip, phraseTrimmed))
  )
}

export const parseName = (event: string) => {
  const index = EVENTS_TAGS.findIndex(i => i.TAG === event)
  if (index !== -1) {
    return EVENTS_TAGS[index].display
  }
  return event
}

export const percentBetween = (start: Date, end: Date) => {
  return Math.round(((new Date().getTime() - start.getTime()) / (end.getTime() - start.getTime())) * 100)
}

export const getText = (end: Date) => {
  const tempEnd = new Date(end)
  const tempStart = new Date()
  if (tempStart > tempEnd) {
    return ''
  }
  const days = (tempEnd.getTime() - tempStart.getTime()) / 1000 / 60 / 60 / 24
  if (Math.floor(days / 365) > 1) {
    return Math.floor(days / 365).toString() + ' years'
  }
  if (days > 2) {
    return Math.floor(days).toString() + ' days'
  }
  const hours = days * 24
  if (hours > 2) {
    return Math.floor(hours).toString() + ' hours'
  }
  const minutes = hours * 60
  if (minutes < 1) {
    return '<1 min'
  }
  if (minutes > 0) {
    return Math.floor(minutes).toString() + ' min'
  }
  return ''
}
