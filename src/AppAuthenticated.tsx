import { createStackNavigator } from '@react-navigation/stack'
import Home from './screens/Home'
import SubTokenDetails from './screens/SubTokenDetails'
// eslint-disable-next-line camelcase
import React, { useContext, useEffect, useState } from 'react'
import CreateSubToken from './screens/CreateSubToken'
import Menu from './screens/Menu'
import AddMasterToken from './screens/AddMasterToken'
import MasterTokenSettings from './screens/MasterTokenSettings'
import LoginSettings from './screens/LoginSettings'
import NotificationSettings from './screens/NotificationSettings'
import AboutSettings from './screens/AboutSettings'
import { MyContext } from './utilities/appContext'
import PollingScreen from './screens/PollingScreen'
import TransferToken from './screens/TransferToken'
import { CONNECTION_ERROR, DOMAIN_URLS, MasterToken, SubToken, TransferCode } from './types'
import { AuthContext } from './utilities/authContext'
// eslint-disable-next-line camelcase
import { Inter_400Regular, Inter_500Medium, Inter_600SemiBold, useFonts } from '@expo-google-fonts/inter'
import { ActivityIndicator } from 'react-native'
import MapsSubTokenEvents from './screens/MapsSubTokenEvents'
import { alertTextOnly } from './utilities/alertHelper'
import DomainScreen from './screens/DomainScreen'
import {
  localFetchDomainList,
  localFetchMasterTokenList, localStoreDomainList,
  localStoreIdentitiesList
} from './api/localUnsecureStorage/localMasterSubTokenManager'
import { firstSetupCheck, setThemeLocal } from './api/localUnsecureStorage/localConfigurations'
import { updateSubTokenList } from './utilities/contextUtilities'
import ResetScreen from './screens/ResetScreen'
import PrivacyPolicy from './screens/PrivacyPolicy'
import { getDomainAddresses } from './api/localUnsecureStorage/localDomainAdresses'
import { DOMAIN_URL_EU, DOMAIN_URL_KIT } from './api/constants/domain_url'

/*
This was the old app entry point. The context storing all required app information as well as the stack navigator is
initialized here. There is an activity indicator waiting for all information to be loaded from storage.
 */

const Stack = createStackNavigator()

const AppAuthenticated = () => {
  const [masterTokenList, setMasterTokenList] = useState<MasterToken[]>([])
  const [subTokenList, setSubTokenList] = useState<SubToken[]>([])
  const [transferCodeList, setTransferCodeList] = useState<TransferCode[]>([])
  const [appLoaded, setAppLoaded] = useState(false)
  const [firstSetup, setFirstSetup] = useState(true)
  const [domainList, setDomainList] = useState<DOMAIN_URLS[]>([])
  const [connectionErrors, setConnectionErrors] = useState<CONNECTION_ERROR[]>([])

  const { loggedIn, passSet, password } = useContext(AuthContext)

  useEffect(() => {
    async function prepare () {
      try {
        if (passSet && password !== '') {
          if (await firstSetupCheck()) {
            const identType:MasterToken[] = []
            const domainListType:DOMAIN_URLS[] = []
            try {
              await getDomainAddresses(DOMAIN_URL_KIT, domainListType, setDomainList, true)
              await getDomainAddresses(DOMAIN_URL_EU, domainListType, setDomainList, true)
            } catch (e) {
              console.warn(e)
            }
            await setThemeLocal('light')
            await localStoreDomainList(domainListType, password)
            // const tokenType:SubToken[] = []
            await localStoreIdentitiesList(identType, password)
            // await localStoreSubTokenList(tokenType, password)
            setFirstSetup(true)
          } else {
            const domainsPre = await localFetchDomainList(password)
            setDomainList(domainsPre)
            const masterTokenListPre:MasterToken[] = await localFetchMasterTokenList(password)
            setMasterTokenList(masterTokenListPre)
            if (masterTokenListPre.length > 0) {
              await updateSubTokenList(masterTokenListPre, setSubTokenList, domainsPre, setConnectionErrors)
            }
            setFirstSetup(false)
          }
          setAppLoaded(true)
        }
      } catch (e) {
        console.warn(e)
      }
    }
    prepare().catch(() => {
      alertTextOnly('Something went synchronising tokens')
    })
  }, [passSet, password, loggedIn])

  useEffect(() => {
    // not this many checks required but avoid override at all costs
    if (appLoaded && loggedIn && passSet && password !== '') {
      localStoreIdentitiesList(masterTokenList, password).then(() => {
      })
      updateSubTokenList(masterTokenList, setSubTokenList, domainList, setConnectionErrors).then(() => {})
    }
  }, [masterTokenList])

  useEffect(() => {
    // not this many checks required but avoid override at all costs
    if (appLoaded && loggedIn && passSet && password !== '') {
      localStoreDomainList(domainList, password).then(() => {
      })
    }
  }, [domainList])

  const [fontsLoaded] = useFonts({
    // eslint-disable-next-line camelcase
    Inter_400Regular, Inter_600SemiBold, Inter_500Medium
  })

  if (!appLoaded || !fontsLoaded) {
    return <ActivityIndicator size="small" color="#D07132" style={{ height: '100%', width: '100%' }}/>
  }

  return (
            <MyContext.Provider
                value={{
                  masterTokenList,
                  subTokenList,
                  setMasterTokenList,
                  setSubTokenList,
                  firstSetup,
                  setFirstSetup,
                  domainList,
                  setDomainList,
                  connectionErrors,
                  setConnectionErrors,
                  transferCodeList,
                  setTransferCodeList
                }}>
                    <Stack.Navigator screenOptions={{ headerShown: false }}
                                     initialRouteName={firstSetup ? 'AddMasterToken' : 'Home'}>
                        <Stack.Screen name='Home' component={Home}/>
                        <Stack.Screen name='CreateSubToken' component={CreateSubToken}/>
                        <Stack.Screen name='TransferToken' component={TransferToken}/>
                        <Stack.Screen name='SubTokenDetails' component={SubTokenDetails}/>
                        <Stack.Screen name='AddMasterToken' component={AddMasterToken}/>
                        <Stack.Screen name='Settings' component={Menu}/>
                        <Stack.Screen name='MasterTokenSettings' component={MasterTokenSettings}/>
                        <Stack.Screen name='LoginSettings' component={LoginSettings}/>
                        <Stack.Screen name='NotificationSettings' component={NotificationSettings}/>
                        <Stack.Screen name='ResetScreen' component={ResetScreen}/>
                        <Stack.Screen name='PrivacyPolicy' component={PrivacyPolicy}/>
                        <Stack.Screen name='AboutSettings' component={AboutSettings}/>
                        <Stack.Screen name='PollingScreen' component={PollingScreen}/>
                        <Stack.Screen name='MapsSubTokenEvents' component={MapsSubTokenEvents}/>
                        <Stack.Screen name='DomainScreen' component={DomainScreen}/>
                    </Stack.Navigator>
            </MyContext.Provider>
  )
}

export default AppAuthenticated
