import React, { useContext, useState } from 'react'
import { TouchableHighlight, View } from 'react-native'
import CardContent from '../content_cards/CardContent'
import { SubToken } from '../../types'
import { resolveMasterToken } from '../../utilities/contextUtilities'
import { MyContext } from '../../utilities/appContext'
import { colorPS } from '../../api/constants/colors'
import LoadingIndicator from './loading_indicator/LoadingIndicator'
import TransferProgressIndicator from './secure_text/TransferProgressIndicator'
import { AuthContext } from '../../utilities/authContext'
import Moment from 'moment'

interface Props {
    nav: any;
    value: SubToken;
    depth: number;
}
const HomeItem = ({ nav, value, depth }: Props) => {
  const { theme } = useContext(AuthContext)
  const [showItems, setShowItems] = useState<boolean>(false)
  const { masterTokenList, transferCodeList } = useContext(MyContext)
  const resolvedToken = resolveMasterToken(value.parent, masterTokenList)
  const reverseShowItems = () => {
    setShowItems(!showItems)
  }
  const loadingIndicator = (dateCreated: Date, dateExpires:Date | undefined, key:string, color: colorPS) => {
    return (
            <LoadingIndicator color={color} dateCreated={dateCreated} dateExpires={dateExpires} key={key}/>
    )
  }
  const referenceTransferCode = (subToken: SubToken) => {
    const index = transferCodeList.findIndex(i => i.momID === subToken.momID)
    return index > -1 ? transferCodeList[index] : undefined
  }
  const singleItem = resolvedToken
    ? <TouchableHighlight
            underlayColor={theme.pressedOptionColor}
            onPress={() => {
              nav.navigate('SubTokenDetails', {
                subTokenID: value.momID
              })
            }}>
            <CardContent
                paddingLeft={depth < 7 ? 15 * depth : depth * 7}
                title={value.name || 'NONAME'}
                subTitle={resolvedToken.name + ' - ' + (value.external ? 'external' : Moment(value.creation).format('D.MMM.YY'))}
                showChevron
                left={loadingIndicator(value.creation, value.expiration, value.name || 'NONAME', resolvedToken.color)}
                bottom={<TransferProgressIndicator key={value.name} transferCode={referenceTransferCode(value)} created={value.creation} expires={value.expiration}/>}
                showItems={showItems}
                reverseShowItems={reverseShowItems}
                hasChildren={value.children !== undefined && value.children.length > 0 ? true : undefined}/>
        </TouchableHighlight>
    : <View key={'error'}/>

  return (
        <View>
            {singleItem}
            { showItems && value.children?.map((item) => (
                <HomeItem nav={nav} key={item.momID} value={item} depth={depth + 1} />
            ))
            }
        </View>
  )
}

export default HomeItem
