import { ScrollView, Text, View } from 'react-native'
import React, { useContext } from 'react'
import { AntDesign } from '@expo/vector-icons'
import { AuthContext } from '../../utilities/authContext'
import createStylesHome from '../../styles/homeStyles'

/*
This component is shown when a catastrophic react renderError occurs caught by the errorhandler in
the AppAuthenticator. The error is shown to the user
 */

interface Props {
    message: string;
}

export default function ErrorScreen (props: Props) {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  return (
        <View style={{ paddingTop: 100, paddingBottom: 10, paddingHorizontal: 60 }}>
            <Text style={styles.mediumText}>
                Close and Reopen the app.
            </Text>
            <AntDesign name='warning' size={60} color='black'
                       style={{ alignSelf: 'center', paddingTop: 30, paddingBottom: 30 }}/>
            <ScrollView style={{ maxHeight: '40 %', paddingHorizontal: 40, paddingTop: 20 }}>
                <Text style={styles.smallText}>
                    {props.message}
                </Text>
            </ScrollView>
            <View style={{ paddingTop: 30 }}/>
        </View>
  )
}
