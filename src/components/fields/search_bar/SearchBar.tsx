import { Octicons } from '@expo/vector-icons'
import { Dimensions, StyleSheet, TextInput, View } from 'react-native'
import React, { useContext } from 'react'
import { AuthContext } from '../../../utilities/authContext'
import createStylesField from '../../../styles/fieldStyles'

/*
This is the search bar. It takes a callback to set the state of the search phrase in its parent
 */

interface Props {
    search: any,
    onChangeSearch: any,
    dark: boolean
    autoFocus: boolean
}
const width = Dimensions.get('window').width

export default function SearchBar (props: Props) {
  const { theme } = useContext(AuthContext)
  const stylesInput = createStylesField(theme)
  return (
        <View style={[props.dark ? stylesInput.textSearchDark : stylesInput.textSearchLight, { flexDirection: 'row', width: width * 0.75 }]}>
            <Octicons name="search" size={20} color={theme.backgroundAccentGrey} style={localStyles.iconStyle}/>
            <TextInput style={{
              flex: 7,
              fontFamily: 'Inter_400Regular',
              fontSize: 17,
              color: props.dark ? 'white' : '#878787'
            }}
                       onChangeText={text => {
                         props.onChangeSearch(text)
                       }}
                       value={props.search}
                       placeholder="search"
                       placeholderTextColor={theme.accentColor}
                       autoFocus={props.autoFocus}
            />
        </View>
  )
}

const localStyles = StyleSheet.create({
  iconStyle: {
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
    paddingLeft: 10
  }
})
