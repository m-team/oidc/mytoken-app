import { Text, View } from 'react-native'
import React, { useContext, useEffect, useState } from 'react'
import CircularProgress from 'react-native-circular-progress-indicator'
import { Octicons } from '@expo/vector-icons'
import { getText, percentBetween } from '../../../utilities/parsingHelper'
import { colorPS, DEFAULT_COLOR } from '../../../api/constants/colors'
import { AuthContext } from '../../../utilities/authContext'
import createStylesHome from '../../../styles/homeStyles'
/*
This is a loading indicator
 */

interface Props {
    color: colorPS,
    dateCreated: Date,
    dateExpires?: Date
}

export default function LoadingIndicator ({ color, dateCreated, dateExpires }: Props) {
  const [percent, setPercent] = useState<number>(0)
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  useEffect(() => {
    let interval:any
    if (dateExpires) {
      setPercent(percentBetween(dateCreated, dateExpires))
      interval = setInterval(() => {
        const calculatedPercent = percentBetween(dateCreated, dateExpires)
        if (calculatedPercent > 100) {
          clearInterval(interval)
        }
        setPercent(calculatedPercent)
      }, 3500)
    }
    if (interval) {
      return () => clearInterval(interval)
    }
  }, [])
  return (
        <View style = {{ height: 40, width: 47, alignItems: 'center', justifyContent: 'center' }}>
            { dateExpires
              ? <Text style={[styles.loadingText, { color: '#00000000' }]}> {getText(dateExpires)} </Text>
              : <View/>
            }
            <CircularProgress value={dateExpires ? dateExpires < new Date() ? 0 : percent : 100} radius={24} showProgressValue={false} activeStrokeColor={color?.primary || DEFAULT_COLOR.primary} inActiveStrokeColor={color?.secondary || DEFAULT_COLOR.secondary}/>
                  <View style={{ position: 'absolute', height: '100%', width: '100%' }}>
                    <View style={{ alignItems: 'center', justifyContent: 'center', height: '100%', width: '100%' }}>
                        {dateExpires
                          ? (dateExpires < new Date()
                              ? <Octicons name="horizontal-rule" size={17} color={color?.primary || DEFAULT_COLOR.primary}/>
                              : <View/>)
                          : <Text style={[styles.loadingText, { fontSize: 14, color: color?.primary || DEFAULT_COLOR.primary }]}>
                                ∞
                            </Text>}
                    </View>
                </View>
                { dateExpires
                  ? <Text style={[styles.loadingText, { color: color?.primary || DEFAULT_COLOR.primary }]}> {getText(dateExpires)} </Text>
                  : <View/>
                }
        </View>
  )
}
