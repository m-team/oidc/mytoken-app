import { Text, View } from 'react-native'
import Checkbox from 'expo-checkbox'
import createStylesField, { HITSLOP_SMALL } from '../../../styles/fieldStyles'
import React, { useContext } from 'react'
import { AuthContext } from '../../../utilities/authContext'

/*
This is a checkbox currently used for settings advanced settings in
the sub token creation process.
 */

interface CheckBoxComponentProps {
    text: string;
    value: boolean,
    setValue: any,
    locked?: boolean
}

export default function CheckBoxComponent (props: CheckBoxComponentProps) {
  const { theme } = useContext(AuthContext)
  const stylesInput = createStylesField(theme)
  const text = props.text
  return (
        <View style={{
          paddingTop: 13,
          paddingHorizontal: 85,
          flexDirection: 'row'
        }}>
            <Text style={{ flex: 4, fontFamily: 'Inter_400Regular', fontSize: 16, alignSelf: 'center', color: theme.smallHeaderColor }}>
                {text}
            </Text>
            <View style={{ flex: 1 }}>
                <Checkbox style={[stylesInput.square, { alignSelf: 'flex-end', borderColor: theme.borderTextEntry, backgroundColor: theme.textEntryContent }]}
                          value={props.value} onValueChange={props.setValue}
                          hitSlop={HITSLOP_SMALL}
                          color={props.value ? (props.locked ? theme.lockedBoxColor : theme.mytokenColor) : undefined}/>
            </View>
        </View>

  )
}
