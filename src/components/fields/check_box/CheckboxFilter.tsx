import { Text, View } from 'react-native'
import Checkbox from 'expo-checkbox'
import stylesInput, { HITSLOP_SMALL } from '../../../styles/fieldStyles'
import React, { useContext } from 'react'
import styles from '../../../styles/homeStyles'
import { AuthContext } from '../../../utilities/authContext'
import createStylesHome from '../../../styles/homeStyles'
import createStylesField from '../../../styles/fieldStyles'

/*
This is a checkbox currently used for filters in the sub token details menu
 */

interface Props {
    text: string;
    value: boolean,
    setValue: any,
}

export default function CheckBoxFilter (props: Props) {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const stylesInput = createStylesField(theme)
  const text = props.text
  return (
        <View style={{
          flex: 1,
          paddingTop: 5,
          flexDirection: 'column',
          justifyContent: 'center',
          alignSelf: 'center'
        }}>
            <View style={{ alignSelf: 'center' }}>
                <Checkbox style={[stylesInput.squareFilters, { alignSelf: 'flex-end' }]}
                          value={props.value} onValueChange={props.setValue}
                          hitSlop={HITSLOP_SMALL}
                          color={props.value ? theme.mytokenColor : undefined}/>
            </View>
            <Text style={[styles.greyedOutText, { fontSize: 11 }]}>
                {text}
            </Text>
        </View>

  )
}
