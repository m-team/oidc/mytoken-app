import { View } from 'react-native'
import React, { useContext } from 'react'
import SecureSquare from './SecureSquare'
import { AuthContext } from '../../../utilities/authContext'
import createStylesHome from '../../../styles/homeStyles'
import createStylesField from '../../../styles/fieldStyles'

/*
This maps multiple squares with letters inside to represent a provided phrase.
It is used to show the transfer code.
 */

interface Props {
    phrase: string
}

export default function SecureText ({ phrase }: Props) {
  return (
        <View style={[{ flexDirection: 'row' }]}>
            {
                phrase.split('').map((letter: string, index: number) => (
                    <View key={index} style={{ marginRight: 6 }}>
                        <SecureSquare letter={letter}/>
                    </View>
                ))
            }
        </View>
  )
}
