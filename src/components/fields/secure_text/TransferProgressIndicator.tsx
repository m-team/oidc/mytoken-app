import { View } from 'react-native'
import React, { useEffect, useState } from 'react'
import SecureText from './SecureText'
import { TransferCode } from '../../../types'

/*
This shows the transfer code text in the home menu if the 5 minutes haven't passed or expiration time hasn't been reached.
There is an animation that shows a green progress indication above the text
 */

interface Props {
    transferCode?: TransferCode,
    created: Date,
    expires?: Date
}

export default function TransferProgressIndicator ({ transferCode, created, expires }: Props) {
  const tempCreated:Date = new Date(created)
  const [show, setShow] = useState<boolean>(false)
  useEffect(() => {
    let interval:any
    if (transferCode && expires && // required values provided
        new Date() < expires && // token not expired
        new Date(tempCreated.getTime() + transferCode.expiration * 1000) > new Date()) { // within transfer code exp time
      setShow(true)
      interval = setInterval(() => {
        if (new Date(tempCreated.getTime() + transferCode.expiration * 1000) < new Date() || (expires && (new Date() > expires))) {
          setShow(false)
          clearInterval(interval)
        }
      }, 7500)
    }
    if (interval) {
      return () => clearInterval(interval)
    }
  }, [transferCode])
  return (
    show ? transferCode ? <View style={{ marginBottom: 15 }}><SecureText phrase={transferCode.transferCode}/></View> : <View/> : <View/>
  )
}
