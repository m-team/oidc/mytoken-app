import { Text, View } from 'react-native'
import styles from '../../../styles/homeStyles'
import React, { useContext } from 'react'
import stylesInput from '../../../styles/fieldStyles'
import { AuthContext } from '../../../utilities/authContext'
import createStylesHome from '../../../styles/homeStyles'
import createStylesField from '../../../styles/fieldStyles'

/*
This is a plain square with a letter inside
 */

interface Props {
    letter: string
}

export default function SecureSquare (props: Props) {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const stylesInput = createStylesField(theme)
  return (
        <View style={[stylesInput.secureBox, { paddingHorizontal: 8, paddingVertical: 10 }]}>
            <Text style={styles.secureText}>
                {props.letter}
            </Text>
        </View>
  )
}
