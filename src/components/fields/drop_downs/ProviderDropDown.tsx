import { StyleSheet, Text, View } from 'react-native'
import React, { useContext, useEffect, useState } from 'react'
import { DOMAIN_URLS, OpenIDProvider } from '../../../types'
import { getProviderList } from '../../../api/axiosRequest'
import DropDownMenu from './DropDownMenu'
import { alertTextOnly } from '../../../utilities/alertHelper'
import { DEFAULT_DOMAIN_URL } from '../../../utilities/main_types_defaults'
import { AuthContext } from '../../../utilities/authContext'

/*
This is the drop-down menu for provider.
 It shows all providers besides the one that is selected and uses a callback to change the selected provider
 in its parent
 */

interface Props {
    provider: any,
    setProvider: any,
    domainUrl: DOMAIN_URLS
}
export default function ProviderDropDown ({ provider, setProvider, domainUrl }: Props) {
  const { theme } = useContext(AuthContext)

  const renderProvider = (provider: OpenIDProvider) => {
    const localStyles = StyleSheet.create({
      placeholderText: {
        fontSize: 17,
        fontFamily: 'Inter_400Regular',
        color: theme.accentColor
      },
      providerText: {
        fontSize: 17,
        fontFamily: 'Inter_400Regular',
        color: theme.backgroundSubtle
      }
    })
    return provider.name === 'DEFAULT'
      ? (
        <View style={{ flex: 1, height: '100%', justifyContent: 'center' }}>
          <Text style={localStyles.placeholderText}>
            Select
          </Text>
        </View>

        )
      : (<View><Text style={localStyles.providerText}>
        {provider.name}
      </Text>
      </View>)
  }
  const [loading, setLoading] = useState<boolean>(false)
  const [providerList, setProviderList] = useState<OpenIDProvider[]>([])

  useEffect(() => {
    if (domainUrl === DEFAULT_DOMAIN_URL) {
      return
    }
    async function populating () {
      const tempList = await getProviderList(domainUrl)
      setProviderList(tempList)
      setLoading(false)
    }
    populating().catch(() => {
      alertTextOnly('Something went wrong fetching providers')
    })
  }, [domainUrl])

  if (loading) {
    return <View/>
  }

  return (
        <DropDownMenu
            chosenContent={provider}
            listContent={providerList}
            renderContent={renderProvider}
            setChosenContent={setProvider}
            removeItems/>
  )
}
