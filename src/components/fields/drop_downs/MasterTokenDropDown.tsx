import { Pressable, StyleSheet, Text, View } from 'react-native'
import React, { useContext } from 'react'
import { Entypo } from '@expo/vector-icons'
import { MyContext } from '../../../utilities/appContext'
import { MasterToken } from '../../../types'
import DropDownMenu from './DropDownMenu'
import { AuthContext } from '../../../utilities/authContext'
import { filterMasterTokensForProvider } from '../../../utilities/contextUtilities'
import { DEFAULT_MASTER_TOKEN } from '../../../utilities/main_types_defaults'

/*
This is the drop-down menu to select master tokens when creating a sub token.
It has more functionality that the provider drop down. It allows the user to add another element. It takes the user
to the addMasterToken screen if they choose to set up another master token.
 */

interface Props {
    navigation: any
    chosenProvider: any,
    setChosenProvider: any,
}

export default function MasterTokenDropDown ({ navigation, chosenProvider, setChosenProvider }: Props) {
  const { masterTokenList } = useContext(MyContext)
  const { theme } = useContext(AuthContext)
  const localStyles = StyleSheet.create({
    mediumText: {
      color: theme.mytokenColor,
      fontFamily: 'Inter_500Medium',
      textAlign: 'center',
      fontSize: 13
    },
    wideText: {
      width: '80%',
      paddingVertical: 5,
      borderBottomColor: theme.backgroundAccentGrey,
      borderBottomWidth: 1.5
    },
    lightText: {
      fontSize: 12,
      fontFamily: 'Inter_400Regular',
      color: theme.accentStrong
    },
    heavyText: {
      fontSize: 17,
      fontFamily: 'Inter_400Regular',
      color: theme.backgroundSubtle
    },
    normalText: {
      fontSize: 17,
      fontFamily: 'Inter_400Regular',
      color: theme.accentColor
    },
    coloredText: {
      fontSize: 17,
      fontFamily: 'Inter_400Regular',
      color: theme.backgroundSubtle
    },
    darkGrayText: {
      fontSize: 12,
      fontFamily: 'Inter_400Regular',
      color: theme.accentStrong
    }
  })

  const renderMasterTokenDropDownItem = (masterToken: MasterToken) => {
    return (
      masterToken.name === 'DEFAULT'
        ? (
              <View style={{ justifyContent: 'center' }}>
                  <Text style={localStyles.normalText}>
                      All providers
                  </Text>
              </View>
          )
        : <View style={{ justifyContent: 'center' }}>
                  <Text style={localStyles.coloredText}>
                          {masterToken.provider.name}
                  </Text>
        </View>)
  }

  const createNewButton = () => {
    return (
        <Pressable hitSlop={20} onPress={() => {
          navigation.navigate('AddMasterToken')
        }} style={{ marginTop: 10, flexDirection: 'row', alignSelf: 'center' }}>
            <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                <Entypo name="plus" size={18} color={theme.mytokenColor}/>
            </View>

            <Text style={localStyles.mediumText}>
                Add New Provider
            </Text>
        </Pressable>
    )
  }
  return (
        <DropDownMenu
            chosenContent={chosenProvider}
            listContent={filterMasterTokensForProvider(masterTokenList)}
            removeItems
            renderContent={renderMasterTokenDropDownItem}
            setChosenContent={setChosenProvider}
            defaultContentShow={DEFAULT_MASTER_TOKEN}>
            {createNewButton()}
        </DropDownMenu>
  )
}
