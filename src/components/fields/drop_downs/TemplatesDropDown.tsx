import { StyleSheet, Text, View } from 'react-native'
import React, { useContext, useEffect, useState } from 'react'
import { MasterToken, TEMPLATE, templateType } from '../../../types'
import DropDownMenu from './DropDownMenu'
import { getTemplateList } from '../../../api/axiosRequest'
import { DEFAULT_TEMPLATE } from '../../../utilities/main_types_defaults'
import { alertTextOnly } from '../../../utilities/alertHelper'
import { MyContext } from '../../../utilities/appContext'
import { resolveDomainURL } from '../../../utilities/contextUtilities'
import { AuthContext } from '../../../utilities/authContext'

/*
This is the drop-down menu for provider.
 It shows all providers besides the one that is selected and uses a callback to change the selected provider
 in its parent
 */

interface Props {
    template: any,
    setTemplate: any,
    chosenMasterToken: MasterToken,
    templateUsed: templateType,
    errorOne?: boolean,
    messageOne?: string,
}

export default function TemplatesDropDown ({ template, setTemplate, chosenMasterToken, templateUsed, errorOne, messageOne }: Props) {
  const [loading, setLoading] = useState<boolean>(true)
  const [templateList, setTemplateList] = useState<TEMPLATE[]>([])
  const { domainList } = useContext(MyContext)
  const { theme } = useContext(AuthContext)

  const localStyles = StyleSheet.create({
    placeholderText: {
      fontSize: 17,
      fontFamily: 'Inter_400Regular',
      color: theme.accentColor
    },
    providerText: {
      fontSize: 17,
      fontFamily: 'Inter_400Regular',
      color: theme.backgroundSubtle
    }
  })
  const renderTemplate = (template: TEMPLATE) => {
    return template.name === 'DEFAULT'
      ? (
        <View style={{ flex: 1, height: '100%', justifyContent: 'center' }}>
          <Text style={localStyles.placeholderText}>
            None
          </Text>
        </View>

        )
      : (<View>
        <Text style={localStyles.providerText}>
          {template.name}
        </Text>
      </View>)
  }
  useEffect(() => {
    setTemplateList([])
    async function populating () {
      if (chosenMasterToken.name !== 'DEFAULT') {
        const apiFetched = await getTemplateList(resolveDomainURL(chosenMasterToken.domainUrl, domainList), templateUsed)
        if (!apiFetched.length) {
          return
        }
        const indexWebDefault = apiFetched.findIndex(x => x.name === 'web-default')
        if (indexWebDefault > -1 && templateUsed === templateType.RESTRICTIONS) {
          setTemplate(apiFetched[indexWebDefault])
        }
        setTemplateList(apiFetched)
      }
    }
    populating().catch(e => {
      alertTextOnly('Something went wrong fetching ' + templateUsed + ' template')
    }).finally(() => {
      setLoading(false)
    })
  }, [chosenMasterToken])

  if (loading) {
    return <View/>
  }

  return (
        <DropDownMenu
            chosenContent={template}
            listContent={templateList}
            renderContent={renderTemplate}
            setChosenContent={setTemplate}
            removeItems
            defaultContentShow = {DEFAULT_TEMPLATE}
            errorOne= {errorOne}
            messageOne= {messageOne}
            />
  )
}
