import { Dimensions, Pressable, ScrollView, StyleSheet, Text, TouchableHighlight, View } from 'react-native'
import React, { useContext, useState } from 'react'
import { Octicons } from '@expo/vector-icons'
import { alertTextOnly } from '../../../utilities/alertHelper'
import { AuthContext } from '../../../utilities/authContext'
import createStylesHome from '../../../styles/homeStyles'
import createStylesField from '../../../styles/fieldStyles'

/*
This is the drop-down menu for provider.
 It shows all providers besides the one that is selected and uses a callback to change the selected provider
 in its parent
 */

interface Props {
    chosenContent: any,
    listContent: any,
    renderContent: any,
    setChosenContent: any,
    removeItems: boolean,
    large?: boolean,
    children?: any,
    defaultContentShow?: any,
    errorOne?: boolean,
    messageOne?: string,
}

const remove = (list: any, element: any) => {
  const tempList = list.slice()
  const index = tempList.indexOf(element)
  if (index !== -1) {
    tempList.splice(index, 1)
  }
  return tempList
}
const height = Dimensions.get('window').height

export default function DropDownMenu ({ chosenContent, listContent, renderContent, setChosenContent, removeItems, children, large, defaultContentShow, errorOne, messageOne }: Props) {
  const [visible, setVisible] = useState<boolean>(false)
  const { theme } = useContext(AuthContext)
  const stylesInput = createStylesHome(theme)
  const styles = createStylesField(theme)
  const localStyles = StyleSheet.create({
    bar: {
      width: '80%',
      paddingVertical: 5,
      borderBottomColor: theme.backgroundAccentGrey,
      borderBottomWidth: 1.5
    }
  })
  // get provider list
  const toggleDropDown = () => {
    if (errorOne !== undefined && !errorOne) {
      messageOne && alertTextOnly(messageOne)
      return
    }
    setVisible(!visible)
  }
  const adjustItems = () => {
    if (removeItems) {
      if (defaultContentShow && chosenContent.name !== 'DEFAULT') {
        return [defaultContentShow, ...remove(listContent, chosenContent)]
      } else {
        return remove(listContent, chosenContent)
      }
    } else {
      if (defaultContentShow && chosenContent.name !== 'DEFAULT') {
        return [defaultContentShow, ...listContent]
      } else {
        return listContent
      }
    }
  }

  return (
        <View>
            <Pressable onPress={() => {
              toggleDropDown()
            }}>
                <View style={[styles.textInput, { height: large ? 65 : 55 }]}>
                    <View style={{ flexDirection: 'row', height: '100%' }}>
                        <View style={{ flexDirection: 'column', flex: 3, height: '100%', justifyContent: 'center' }}>
                            {renderContent(chosenContent, chosenContent)}
                        </View>
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                                <Octicons name={visible ? 'chevron-up' : 'chevron-down'} size={22} color={theme.backgroundGrey}/>
                            </View>
                        </View>
                    </View>
                </View>
            </Pressable>
            {
                !visible
                  ? (
                        <View/>
                    )
                  : (
                        <View>
                            <Pressable onPress={() => {
                              toggleDropDown()
                            }}>
                            </Pressable>
                            <View style={[styles.box, { maxHeight: height * 0.5, zIndex: 9999 }]}>
                                <ScrollView nestedScrollEnabled={true}>
                                    {
                                        (adjustItems().length < 1)
                                          ? (
                                                <Text style={stylesInput.smallGrayText}>
                                                    No Other Ones
                                                </Text>
                                            )
                                          : adjustItems().map((values: any, index: number) => (
                                                <TouchableHighlight underlayColor={theme.pressedOptionColor} key={index} onPress={() => {
                                                  setChosenContent(values)
                                                  toggleDropDown()
                                                }} style={{ paddingVertical: 5 }}>
                                                    <View>
                                                        {renderContent(values)}
                                                        {
                                                            adjustItems().length >= 1
                                                              ? (
                                                                    <View style={[localStyles.bar, { borderBottomColor: theme.dividerColor }]}>
                                                                    </View>
                                                                )
                                                              : (<View/>)
                                                        }
                                                    </View>
                                                </TouchableHighlight>
                                          ))
                                    }
                                    {children}
                                </ScrollView>
                            </View>
                        </View>
                    )
            }
        </View>
  )
}
