import { Pressable, StyleSheet, Text, TouchableHighlight, View } from 'react-native'
import React, { useContext } from 'react'
import { Entypo, Octicons } from '@expo/vector-icons'
import { DOMAIN_URLS } from '../../../types'
import { deleteDomainAddress } from '../../../api/localUnsecureStorage/localDomainAdresses'
import DropDownMenu from './DropDownMenu'
import { MyContext } from '../../../utilities/appContext'
import { DEFAULT_DOMAIN_URL } from '../../../utilities/main_types_defaults'
import { AuthContext } from '../../../utilities/authContext'

/*
This is the drop-down menu for provider.
 It shows all providers besides the one that is selected and uses a callback to change the selected provider
 in its parent
 */

interface Props {
    domain: any,
    setDomain: any,
    navigation: any
}

export default function DomainDropDown ({ domain, setDomain, navigation }: Props) {
  const { domainList, setDomainList, masterTokenList } = useContext(MyContext)
  const { theme } = useContext(AuthContext)
  const localStyles = StyleSheet.create({
    placeholderText: {
      fontSize: 17,
      fontFamily: 'Inter_400Regular',
      color: theme.accentColor
    },
    domainText: {
      fontSize: 10.5,
      fontFamily: 'Inter_400Regular',
      color: theme.backgroundSubtle
    },
    mediumText: {
      color: theme.mytokenColor,
      fontFamily: 'Inter_500Medium',
      textAlign: 'center',
      fontSize: 13
    },
    wideText: {
      width: '80%',
      paddingVertical: 5,
      borderBottomColor: theme.backgroundAccentGrey,
      borderBottomWidth: 1.5
    }
  })
  const removeItemButton = (domainUrl: DOMAIN_URLS) => {
    if (domain === domainUrl) {
      setDomain(DEFAULT_DOMAIN_URL)
    }
    deleteDomainAddress(domainUrl, domainList, setDomainList, masterTokenList)
  }
  const renderDomain = (domainUrl: DOMAIN_URLS, selectedUrl: DOMAIN_URLS) => {
    return (
            <View style={{ flexDirection: 'row' }}>
                <Text style={[localStyles.domainText, { flex: 0.8, alignSelf: 'center' }]}>
                    {domainUrl && domainUrl !== DEFAULT_DOMAIN_URL
                      ? domainUrl.domainUrl
                      : <Text style={localStyles.placeholderText}>
                            None
                        </Text>
                    }
                </Text>
                <View style={{ flex: 0.2 }}>
                    {
                        domainUrl !== selectedUrl
                          ? <TouchableHighlight style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }} onPress={() => {
                            removeItemButton(domainUrl)
                          }}>
                            <Octicons name={'trash'} size={22} color={theme.backgroundGrey}/>
                        </TouchableHighlight>
                          : <View/>
                    }
                </View>
            </View>
    )
  }
  return (
      <DropDownMenu
          chosenContent={domain}
          listContent={(domainList && domainList.filter(x => x.visible)) || []}
          removeItems={false}
          renderContent={renderDomain}
          setChosenContent={setDomain}>
          <Pressable hitSlop={20} onPress={() => {
            navigation.navigate('DomainScreen')
          }}
                     style={{ marginTop: 10, flexDirection: 'row', alignSelf: 'center' }}>
              <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                  <Entypo name="plus" size={18} color={theme.mytokenColor}/>
              </View>

              <Text style={localStyles.mediumText}>
                  Add Domain URL
              </Text>
          </Pressable>
      </DropDownMenu>
  )
}
