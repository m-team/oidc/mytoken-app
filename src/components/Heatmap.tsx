import { ActivityIndicator, ScrollView, Text, TouchableOpacity, View } from 'react-native'
import React, { useContext, useEffect, useRef, useState } from 'react'
import {
  DAYS,
  daysLabels,
  differenceDays,
  getColor,
  monthsLabels,
  sameDay
} from '../utilities/heatmapUtilities'
import { DayUse, TOKEN_EVENT } from '../types'
import { CREATED_EVENT } from '../api/constants/request_body_defaults'
import Moment from 'moment'
import { AuthContext } from '../utilities/authContext'
import createStylesHome from '../styles/homeStyles'
import createStylesField from '../styles/fieldStyles'

/*
This heatmap uses the heatmap utility and a scrollview to generate the heatmap with frequency values
corresponding to the amount of times (per day) events the user searched for are present.
 */

interface Props {
    allEvents: TOKEN_EVENT[]
    onChangeSearch: any
}

export default function Heatmap (props: Props) {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const stylesInput = createStylesField(theme)
  const [frequencyValues, setFrequencyValues] = useState<DayUse[]>(daysLabels(DAYS))
  const [loading, setLoading] = useState<boolean>(true)
  useEffect(() => {
    setLoading(true)
    if (props.allEvents === undefined) {
      return
    }
    const today = new Date()
    // move this all events to props population like this can be done when calling useEffect
    const filteredEvents = props.allEvents.filter((obj) => {
      return differenceDays(today, obj.time) < DAYS
    })
    const copyFrequencyValue = daysLabels(DAYS)
    // very inefficient
    copyFrequencyValue.forEach((tempDay: DayUse) => (
      filteredEvents.forEach((tokenEvent: TOKEN_EVENT) => {
        if (sameDay(tempDay.date, tokenEvent.time)) {
          // make element green if event is creation
          if (tokenEvent.event === CREATED_EVENT) {
            tempDay.created = true
          } else {
            tempDay.frequency++
          }
        }
      }
      )
    ))
    setFrequencyValues(copyFrequencyValue)
    setLoading(false)
  }, [props.allEvents])

  const scrollRef = useRef<ScrollView>(null)

  if (loading) {
    return <ActivityIndicator size="small" color={theme.mytokenColor} style={{ height: '100%', width: '100%' }}/>
  }
  const renderItem = (item: DayUse[], index: number) => {
    if (index < 0 || index >= DAYS) {
      return <View key={index} style={[stylesInput.heatMapSquare, { backgroundColor: '#00000000' }]}/>
    }
    if (item[index].frequency === -1) {
      return <View key={index}/>
    }
    return (
            <TouchableOpacity onPress={() => {
              props.onChangeSearch(Moment(item[index].date).format('D.MMM.YY'))
            }} key={index}>
                <View
                    style={[stylesInput.heatMapSquare, { backgroundColor: getColor(item[index].frequency, item[index].created, theme.heatmapColor) }]}/>
            </TouchableOpacity>)
  }

  return (
        <View style={{ paddingTop: 20, paddingBottom: 25 }}>
            <ScrollView horizontal ref={scrollRef} onContentSizeChange={() => {
              scrollRef.current?.scrollToEnd({ animated: false })
            }}>
                <View style={{ flexDirection: 'column', paddingBottom: 10 }}>
                    <View style={{ flexDirection: 'row' }}>
                        {
                            // previous implementation with Flatlist but not possible to customize enough
                            [...Array(Math.ceil((DAYS / 7) + ((frequencyValues[0].date.getDay() === 0) ? 0 : 1)))].map((value: any, outer: number) => (
                                <View style={{ flexDirection: 'column' }} key={outer}>
                                    {
                                        [...Array(7)].map((value: any, inner: number) => (
                                          renderItem(frequencyValues, outer * 7 + inner - (frequencyValues[0].date.getDay())
                                          )))
                                    }
                                </View>
                            ))
                        }
                        <View style={{ flexDirection: 'column', height: '100%', margin: 5, alignSelf: 'center' }}>
                            <View style={{ paddingTop: 37 }}/>
                            <Text style={styles.heatMapText}>
                                Monday
                            </Text>
                            <View style={{ paddingBottom: 5 }}/>
                            <Text style={styles.heatMapText}>
                                Wednesday
                            </Text>
                            <Text style={styles.heatMapText}>
                                Friday
                            </Text>
                        </View>
                    </View>
                    <View style={{ height: 20, width: 605, alignSelf: 'flex-end' }}>
                        <View style={{
                          flexDirection: 'row',
                          paddingRight: new Date().getDate() * 2,
                          alignSelf: 'flex-end'
                        }}>
                            {
                                monthsLabels().map((values: string, index: number) => (
                                    <View style={{ width: 120, height: 20 }} key={index}>
                                        <Text style={[styles.heatMapText, { textAlign: 'left' }]}>
                                            {values}
                                        </Text>
                                    </View>
                                ))
                            }
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
  )
}
