import { Text, TouchableOpacity, View } from 'react-native'
import { Octicons } from '@expo/vector-icons'
import React, { useContext } from 'react'
import { AuthContext } from '../../utilities/authContext'
import { HITSLOP_SQUARE } from '../../styles/fieldStyles'

/*
A card that splices together content with an optional chevron
 */

interface CardContentProps {
    title: string;
    subTitle: string;
    showItems?: boolean;
    reverseShowItems?: any;
    left?: any;
    showChevron?: boolean;
    compact?: boolean;
    bottom?:any;
    hasChildren?: boolean;
    paddingLeft?: number;
}

export default function CardContent ({ title, subTitle, showItems, reverseShowItems, showChevron, compact, left, bottom, hasChildren, paddingLeft }: CardContentProps) {
  const { theme } = useContext(AuthContext)
  return (
        <View
            style={{ borderBottomColor: theme.dividerColor, borderBottomWidth: 1.5, marginHorizontal: 23 }}>
            <View style={{ flexDirection: 'row' }}>
            <View style={{ paddingLeft, marginLeft: 20, alignSelf: 'center' }}>
                {left}
            </View>
            <View style={{ paddingLeft: paddingLeft ? paddingLeft / 4 : undefined, flexDirection: 'column', flex: 3 }}>
                <Text style={{
                  fontFamily: compact ? 'Inter_400Regular' : 'Inter_500Medium',
                  fontSize: compact ? 17 : 20,
                  color: theme.backgroundSubtle,
                  marginLeft: 20,
                  marginTop: compact ? 15 : 25
                }}>
                    {title}
                </Text>
                <Text style={{
                  fontFamily: 'Inter_400Regular',
                  fontSize: 14,
                  color: theme.subTitleColor,
                  marginLeft: 20,
                  marginBottom: compact ? 15 : 25
                }}>
                    {subTitle}
                </Text>
            </View>
            {
                showChevron
                  ? (
                    <View style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'flex-start',
                      flexDirection: 'row'
                    }}>
                        {
                            hasChildren
                              ? (
                                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                      <TouchableOpacity hitSlop={HITSLOP_SQUARE} onPress={reverseShowItems}>
                                          <Octicons name={showItems ? 'chevron-up' : 'chevron-down'} size={28} color={theme.backgroundGrey}/>
                                      </TouchableOpacity>
                                  </View>
                                )
                              : <Octicons name="chevron-right" size={28} color={theme.backgroundGrey}/>
                        }
                    </View>)
                  : <View/>
            }
        </View>
            <View style={{ alignSelf: 'center' }}>
                {bottom}
            </View>
        </View>
  )
}
