import { Text, View } from 'react-native'
import { Octicons } from '@expo/vector-icons'
import React, { useContext } from 'react'
import SubDivider from '../spacing_and_headers/SubDivider'
import { AuthContext } from '../../utilities/authContext'

/*
A card that splices together content with a logo on the left currently used in the settings menu.
 */

interface CardSettingsProps {
    text: string,
    icon: any,
    hideChevron?: boolean
}

export default function CardSettings ({ text, icon, hideChevron }: CardSettingsProps) {
  const { theme } = useContext(AuthContext)
  return (
        <View>
            <View style={{ height: 80, flexDirection: 'row' }}>
                <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'flex-end' }}>
                    <Octicons name={icon} size={20} color={theme.backgroundSubtle}/>
                </View>
              <View style={{ flex: 0.1 }}/>
                <View style={{ flex: 0.6, justifyContent: 'center', alignContent: 'center' }}>
                    <Text style={{
                      fontFamily: 'Inter_400Regular',
                      fontSize: 17,
                      color: theme.backgroundSubtle
                    }}>
                        {text}
                    </Text>
                </View>
                <View style={{ flex: 0.3, justifyContent: 'center', alignContent: 'flex-start' }}>
                    {
                        hideChevron ? <View/> : <Octicons name="chevron-right" size={28} color={theme.backgroundGrey}/>
                    }
                </View>
            </View>
            <SubDivider/>
        </View>
  )
}
