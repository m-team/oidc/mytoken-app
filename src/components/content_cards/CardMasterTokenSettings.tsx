import { Alert, Linking, Modal, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import { Octicons, MaterialIcons } from '@expo/vector-icons'
import React, { useCallback, useContext, useState } from 'react'
import { HITSLOP_SQUARE_LEFT, HITSLOP_SQUARE_RIGHT, HITSLOP_SQUARE_TOP } from '../../styles/fieldStyles'
import { alertTextOnly } from '../../utilities/alertHelper'
import { getText } from '../../utilities/parsingHelper'
import ColorPicker, { Swatches } from 'reanimated-color-picker'
import { resolveDomainURL, resolveMasterToken, updateMasterTokenColor } from '../../utilities/contextUtilities'
import { MyContext } from '../../utilities/appContext'
import { colorsArr, DEFAULT_COLOR } from '../../api/constants/colors'
import { AuthContext } from '../../utilities/authContext'
import { getConfirm } from '../../api/axiosRequest'
import debounce from 'lodash.debounce'

/*
A card that splices together content with a logo on the right and custom styling currently used in the master
 token management menu.
 */

interface Props {
    title: string;
    subTitle: string;
    onPress: any;
    subTokenCount: number;
    expirationDate: Date | undefined,
    masterToken: string,
    nav: any}

export default function CardMasterTokenSettings (props: Props) {
  const [showModal, setShowModal] = useState(false)
  const { masterTokenList, setMasterTokenList, domainList } = useContext(MyContext)
  const { theme } = useContext(AuthContext)

  const localStyles = StyleSheet.create({
    bottomBorder: {
      flexDirection: 'row',
      borderBottomColor: theme.dividerColor,
      borderBottomWidth: 1.5,
      marginHorizontal: 25
    },
    titleText: {
      fontFamily: 'Inter_500Medium',
      fontSize: 20,
      color: theme.backgroundSubtle,
      marginTop: 15
    },
    subTitleText: {
      fontFamily: 'Inter_400Regular',
      fontSize: 14,
      color: theme.backgroundGrey,
      marginBottom: 15
    },
    trashStyling: {
      justifyContent: 'center',
      alignSelf: 'center'
    },
    circleButtonNumber: {
      fontFamily: 'Inter_500Medium',
      fontSize: 24,
      color: theme.backgroundSubtle
    },
    circleButton: {
      width: 38,
      height: 38,
      borderRadius: 30,
      justifyContent: 'center',
      alignItems: 'center',
      margin: 10
    },
    pillSelection: {
      width: 70,
      height: 38,
      borderRadius: 30,
      justifyContent: 'center',
      alignItems: 'center',
      color: theme.backgroundAccentGrey
    }
  })
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignContent: 'center',
      backgroundColor: theme.mainBackground
    },
    pickerContainer: {
      alignSelf: 'center',
      width: 300,
      backgroundColor: theme.modalColor,
      padding: 20,
      borderRadius: 20,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 5
      },
      shadowOpacity: 0.34,
      shadowRadius: 6.27,

      elevation: 10
    },
    swatchesContainer: {
      alignItems: 'center',
      flexWrap: 'wrap',
      gap: 15
    },
    swatchStyle: {
      borderRadius: 20,
      height: 40,
      width: 40,
      margin: 0,
      marginBottom: 4,
      marginHorizontal: 4,
      marginVertical: 4
    },
    openButton: {
      width: '100%',
      borderRadius: 20,
      paddingHorizontal: 40,
      paddingVertical: 10,
      backgroundColor: '#fff',

      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,

      elevation: 5
    }
  })
  const onSelectColor = ({ hex }:{hex: string}) => {
    const findIndex = colorsArr.findIndex((val) => val.primary.toUpperCase() === hex.toUpperCase())
    if (findIndex > -1) {
      updateMasterTokenColor(props.masterToken, colorsArr[findIndex], masterTokenList, setMasterTokenList)
    }
    setShowModal(false)
  }

  const mainButtonAction = () => {
    const token = resolveMasterToken(props.masterToken, masterTokenList)
    if (!token) return
    getConfirm(token.provider, resolveDomainURL(token.domainUrl, domainList)).then(r => {
      props.nav.navigate('PollingScreen', {
        pollingCode: r[0],
        pollingLink: r[1],
        providerName: token.provider.name,
        providerUrl: token.provider.url,
        name: token.provider.name,
        domainUrl: token.domainUrl,
        expiresIn: r[2],
        color: token.color
      })
      Linking.openURL(r[1]).then(() => {})
    }).catch(() => {
      alertTextOnly('Something went wrong adding or updating your provider')
    })
  }
  const debouncedMainButtonHandler = useCallback(debounce(mainButtonAction, 2000, {
    leading: true,
    trailing: false
  }), [])
  const refreshButtonAction = () => {
    let text
    if (props.expirationDate && new Date(props.expirationDate) < new Date()) {
      text = 'It is Expired'
    } else {
      text = props.expirationDate ? 'It expires in ' + getText(props.expirationDate) : 'It does not expire'
    }
    Alert.alert('Warning', `This provider has ${props.subTokenCount} in app Tokens \n` + text + '. Would you like to refresh it?', [
      {
        text: 'Cancel',
        onPress: () => {},
        style: 'cancel'
      },
      {
        text: 'Refresh',
        onPress: () => { debouncedMainButtonHandler() }
      }])
  }
  return (<View style={localStyles.bottomBorder}>
      <Modal
          onRequestClose={() => setShowModal(false)}
          visible={showModal}
          animationType="slide">
        <TouchableWithoutFeedback onPress={() => setShowModal(false)}>
        <View style={styles.container}>
          <TouchableWithoutFeedback onPress={() => {}}>
          <View style={styles.pickerContainer}>
              <ColorPicker
                  value={DEFAULT_COLOR.primary}
                  sliderThickness={25}
                  thumbSize={24}
                  thumbShape="circle"
                  onChange={onSelectColor}
                  boundedThumb>
                  <Swatches
                      style={styles.swatchesContainer}
                      swatchStyle={styles.swatchStyle}
                      colors={colorsArr.map(val => val.primary)}
                  />
              </ColorPicker>
          </View>
          </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
              <View style={{ flexDirection: 'column', flex: 0.6, paddingLeft: 35 }}>
                  <Text style={localStyles.titleText}>
                      {props.title}
                  </Text>
                  <Text style={localStyles.subTitleText}>
                      {props.subTitle}
                  </Text>
              </View>
              {
                  <View style={[localStyles.pillSelection, localStyles.trashStyling, { flexDirection: 'row', flex: 0.4 }]}>
                          <TouchableOpacity hitSlop={HITSLOP_SQUARE_LEFT}
                                            onPress={() => { setShowModal(true) }}>
                              <View style={{}}>
                                  <MaterialIcons name="colorize" size={28} color={resolveMasterToken(props.masterToken, masterTokenList)?.color.primary || DEFAULT_COLOR.primary} />
                              </View>
                          </TouchableOpacity>
                          <View style={{ width: 15 }}/>
                          <TouchableOpacity hitSlop={HITSLOP_SQUARE_RIGHT}
                                            onPress={refreshButtonAction}>
                            <View style={{}}>
                              <MaterialIcons name="refresh" size={33} color={props.expirationDate ? '#de7a10' : '#AEAEB4'} />
                            </View>
                          </TouchableOpacity>
                        <View style={{ width: 18 }}/>
                        <TouchableOpacity hitSlop={HITSLOP_SQUARE_TOP}
                                            onPress={props.onPress}>
                              <View style={{}}>
                                  <Octicons name="trash" size={28} color="#AEAEB4"/>
                              </View>
                          </TouchableOpacity>
                  </View>
              }
          </View>
  )
}
