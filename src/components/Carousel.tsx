import { Dimensions, FlatList, Image, StyleSheet, Text, View, ViewToken } from 'react-native'
import React, { useContext, useRef, useState } from 'react'
import { AuthContext } from '../utilities/authContext'
import createStylesHome from '../styles/homeStyles'
import createStylesField from '../styles/fieldStyles'

/*
A carousel for the onboarding screen.
 */

const windowWidth = Dimensions.get('window').width

const viewabilityConfig = {
  viewAreaCoveragePercentThreshold: 65,
  waitForInteraction: true
}

export default function Carousel ({ DATA }:{DATA:any}) {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const stylesInput = createStylesField(theme)
  const renderItem = ({ item }:{item:any}) => {
    return (
      <View style={{ width: windowWidth, justifyContent: 'center', height: '100%' }} >
        <View style={{ paddingHorizontal: 80, height: '60%', width: '100%', paddingTop: 25 }}>
          <Image source={item.image} style = {localStyles.imageStyling}/>
        </View>
        <View style={{ paddingTop: 7 }}>
          <Text style={styles.welcomeText}>
            {item.text}
          </Text>
        </View>
        <View style={{ paddingHorizontal: 70, marginBottom: 30 }}>
          <Text style={styles.mediumText}>
            {item.subText}
          </Text>
        </View>
      </View>
    )
  }
  const [index, setIndex] = useState<number | null>(0)
  const onViewableItemsChanged = ({
    viewableItems
  }:{viewableItems:ViewToken[]}) => {
    if (viewableItems.length) {
      setIndex(viewableItems[0].index)
    }
  }
  const viewabilityConfigCallbackPairs = useRef([
    { viewabilityConfig, onViewableItemsChanged }
  ])

  return (
        <View style = {{ height: 280, alignSelf: 'center', width: '100%', marginBottom: 40 }}>
            <FlatList data={DATA}
                      renderItem={renderItem}
                      pagingEnabled={true}
                      horizontal
                      showsHorizontalScrollIndicator={false}
                      viewabilityConfigCallbackPairs={viewabilityConfigCallbackPairs.current}
                      bounces={false}
                        viewabilityConfig={viewabilityConfig}
            scrollEventThrottle={25}/>
            <View style = {{ alignSelf: 'center', flexDirection: 'row', paddingTop: 10 }}>
                {
                    DATA.map((_:any, indexMap: number) => (
                        <View key={indexMap} style={[stylesInput.indicator, { backgroundColor: index === indexMap ? '#aeaeb4' : '#D9D9D9' }]}>
                        </View>
                    ))
                }
            </View>
        </View>
  )
}
const localStyles = StyleSheet.create({
  imageStyling: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain'
  }
})
