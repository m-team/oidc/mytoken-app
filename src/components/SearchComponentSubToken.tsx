import React, { useContext, useEffect, useState } from 'react'
import { FlatList, Pressable, RefreshControl, Text, View } from 'react-native'
import SearchBar from './fields/search_bar/SearchBar'
import { Entypo } from '@expo/vector-icons'
import DividerWithText from './spacing_and_headers/DividerWithText'
import { TOKEN_EVENT } from '../types'
import CardContent from './content_cards/CardContent'
import Moment from 'moment'
import { parseName, searchEventHistoryList } from '../utilities/parsingHelper'
import { alertTextOnly } from '../utilities/alertHelper'
import { AuthContext } from '../utilities/authContext'
import createStylesHome from '../styles/homeStyles'

/*
This component is the upper part of the subtoken details. The component was created
to drastically reduce the amount of renders of the heatmap and improve code quality.
 */

interface Props {
    children:any
    filterList: TOKEN_EVENT[],
    transferSearch: any,
    nav: any,
  reloadCounter: number,
  setReloadCounter: any
}

const renderItem = ({ item }:{item:TOKEN_EVENT}) => {
  return (
  // using Moment here is causing performance issues of flat-list (only warning expo),
  // evaluate further and move Moment to where data is prepared
        <CardContent title={parseName(item.event)}
                 subTitle={((!item.info || !item.info.city) ? item.ip : item.info.city + ' - ' + item.ip) + ' - ' +
                     Moment(item.time).format('D.MMM.YY HH:mm')} compact/>
  )
}
export default function SearchComponentSubToken ({ children, filterList, transferSearch, nav, reloadCounter, setReloadCounter }: Props) {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const [searchList, setSearchList] = useState<TOKEN_EVENT[]>([])
  const [search, onChangeSearch] = useState('')
  const [visible, setVisible] = useState<boolean>(false)
  const [refreshing, setRefreshing] = useState<boolean>(false)

  const onRefresh = React.useCallback(() => {
    setRefreshing(true)
    setReloadCounter(reloadCounter + 1)
    setTimeout(() => {
      setRefreshing(false)
    }, 1000)
  }, [reloadCounter])
  const invertVisible = () => {
    setVisible(!visible)
  }
  // handle search
  useEffect(() => {
    // remove element not defined
    setSearchList(searchEventHistoryList(search, filterList.slice().reverse()))
  }, [search, filterList])

  useEffect(() => {
    onChangeSearch(transferSearch)
  }, [transferSearch])

  return (
      <View style={{ height: '100%' }}>
    <View style={{ paddingTop: 10 }}/>
    <View style={{ alignSelf: 'center' }}>
        <View style={{ alignItems: 'center', flexDirection: 'row', width: '100%', paddingTop: 4.5 }}>
            <View style={{ flex: 0.80, alignSelf: 'center', alignItems: 'flex-end' }}>
                <SearchBar onChangeSearch={onChangeSearch} search={search} dark={false}
                           autoFocus={false}/>
            </View>
            <View style={{ flex: 0.2, alignSelf: 'center', justifyContent: 'center' }}>
                <Pressable hitSlop={20} onPress={() => {
                  const filteredList = searchList.filter((value) => value.info?.city !== undefined)
                  if (!searchList.length) {
                    alertTextOnly('There are no search results to display on a map!')
                  } else if (!filteredList.length) {
                    alertTextOnly('No locations were found for these IPs!')
                  } else {
                    nav.navigate('MapsSubTokenEvents', { searchList })
                  }
                }}>
                    <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                        <Entypo name="map" size={24} color={theme.mytokenColor}/>
                    </View>
                </Pressable>
            </View>
        </View>
        <Pressable hitSlop={{ top: 5, bottom: 20, left: 20, right: 20 }} onPress={() => {
          invertVisible()
        }} style={{ marginTop: 10 }}>
            <Text style={[styles.greyedOutText, { color: theme.advancedView }]}>
                {!visible ? 'show filters' : 'hide filters'}
            </Text>
            <View style={{ alignSelf: 'center' }}>
                <Entypo name={!visible ? 'chevron-down' : 'chevron-up'} size={20} color={theme.backgroundGrey}/>
            </View>
        </Pressable>
        {visible
          ? (<View style={{ height: !visible ? 30 : 45, justifyContent: 'center', width: '100%' }}>
                <View style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 10, paddingBottom: 5 }}>
                    {children}
                </View>
            </View>)
          : <View/>
        }
    </View>
        <DividerWithText text="Token Usage Details"/>
            {
                !searchList.length
                  ? (
                    <View style={{ flex: 1, alignSelf: 'center', paddingTop: 10 }}>
                        <Entypo name="dots-three-horizontal" size={27} color={theme.backgroundAccentGrey}/>
                    </View>
                    )
                  : (
                    <FlatList
                     data={searchList}
                     renderItem={renderItem}
                     refreshControl={
                       <RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>
                    }/>
                    )
            }
    </View>)
}
