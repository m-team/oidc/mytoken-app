import { Text, TouchableOpacity, View } from 'react-native'
import styles from '../../styles/homeStyles'
import React, { useContext } from 'react'
import stylesInput, { HITSLOP } from '../../styles/fieldStyles'
import { AuthContext } from '../../utilities/authContext'
import createStylesHome from '../../styles/homeStyles'
import createStylesField from '../../styles/fieldStyles'

/*
The apps main button style for buttons that are sure to call to action
 */

interface mainButtonProps {
    onPress: any;
    text: string
}

export default function MainButton (props: mainButtonProps) {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const stylesInput = createStylesField(theme)

  return (
        <View style={stylesInput.buttonMain}>
            <TouchableOpacity hitSlop={HITSLOP} onPress={props.onPress}>
                <Text style={styles.buttonText}>
                    {props.text}
                </Text>
            </TouchableOpacity>
        </View>
  )
}
