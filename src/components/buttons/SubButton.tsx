import { Text, TouchableOpacity, View } from 'react-native'
import styles from '../../styles/homeStyles'
import React, { useContext } from 'react'
import stylesInput, { HITSLOP } from '../../styles/fieldStyles'
import { AuthContext } from '../../utilities/authContext'
import createStylesHome from '../../styles/homeStyles'
import createStylesField from '../../styles/fieldStyles'

/*
The apps sub button style for buttons that are not as important (call to action)
 */

interface Props {
    onPress: any;
    text: string
}

export default function SubButton (props: Props) {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const stylesInput = createStylesField(theme)

  return (
        <View style={stylesInput.subButton}>
            <TouchableOpacity hitSlop={HITSLOP} onPress={props.onPress}>
                <Text style={styles.subButtonText}>
                    {props.text}
                </Text>
            </TouchableOpacity>
        </View>
  )
}
