import { Pressable, Text, View } from 'react-native'
import React, { useContext } from 'react'
import { Entypo } from '@expo/vector-icons'
import { AuthContext } from '../../utilities/authContext'

/*
The apps button that splices together text and a plus logo
 */

interface Props {
    onPress: any;
    text: string;
    icon:any;
}

export default function CreateNewButton (props: Props) {
  const { theme } = useContext(AuthContext)
  return (
        <Pressable hitSlop={20} onPress={props.onPress}>
            <Text style={{ color: theme.mytokenColor, fontFamily: 'Inter_500Medium', textAlign: 'center' }}>
                {props.text}
            </Text>
            <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                <Entypo name={props.icon} size={18} color={theme.mytokenColor}/>
            </View>
        </Pressable>
  )
}
