import { View } from 'react-native'
import React, { useContext } from 'react'
import { AuthContext } from '../../utilities/authContext'

/*
This is a plain divider that's wider than the subdivider
 */

export default function Divider () {
  const { theme } = useContext(AuthContext)

  return (
        <View style={{ borderBottomColor: theme.dividerColor, borderBottomWidth: 1.5, marginHorizontal: 23 }}>
        </View>
  )
}
