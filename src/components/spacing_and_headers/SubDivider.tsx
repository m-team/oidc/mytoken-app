import { View } from 'react-native'
import React, { useContext } from 'react'
import { AuthContext } from '../../utilities/authContext'

/*
A plain divider
 */

export default function SubDivider () {
  const { theme } = useContext(AuthContext)

  return (
        <View style={{ borderBottomColor: theme.dividerColor, borderBottomWidth: 1.5, marginHorizontal: 40 }}>
        </View>
  )
}
