import { StyleSheet, TextInput, View } from 'react-native'
import EntrySubTitle from './EntrySubTitle'
import React, { useContext } from 'react'
import { AuthContext } from '../../utilities/authContext'
import createStylesField from '../../styles/fieldStyles'

/*
This is a text field with a header to explain its purpose
 */

interface Props {
    onChangeText: any
    value: any
    placeholder: string
    placeholderTextColor: string
    keyboardType?: any
    secureTextEntry?: boolean
    autoCapitalizeOff?: boolean
    text: string
    reference?: any
    nextRef?:any
    onSubmitEditing?:any
}

export default function TextFieldWithHeader (props: Props) {
  const { theme } = useContext(AuthContext)
  const stylesInput = createStylesField(theme)
  const localStyles = StyleSheet.create({
    textEntrySubSection: {
      paddingTop: 12,
      paddingHorizontal: 70,
      paddingBottom: 12
    }
  })
  return (
        <View style={localStyles.textEntrySubSection}>
            <EntrySubTitle text={props.text}/>
            <TextInput style={stylesInput.textInput}
                       onChangeText={props.onChangeText}
                       value={props.value}
                       placeholder={props.placeholder}
                       placeholderTextColor={props.placeholderTextColor}
                       keyboardType={props.keyboardType}
                       secureTextEntry={props.secureTextEntry}
                       autoCapitalize = {props.autoCapitalizeOff ? 'none' : 'sentences'}
                       onSubmitEditing={props.onSubmitEditing ? props.onSubmitEditing : props.nextRef ? () => { props.nextRef.current.focus() } : undefined}
                       ref={props.reference}/>
        </View>
  )
}
