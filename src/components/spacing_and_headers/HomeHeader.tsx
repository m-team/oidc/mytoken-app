import { Dimensions, View } from 'react-native'
import React, { useContext } from 'react'
import { AuthContext } from '../../utilities/authContext'
import createStylesHome from '../../styles/homeStyles'
import { darkTheme } from '../../api/constants/colors'

/*
This is the home header which splices together components provided as props (composition)
to create the home screen header.
 */

interface EntrySubTitleProps {
    iconLeft?: any;
    iconRight?: any;
    contentCenter?: any;
}
const height = Dimensions.get('window').height

export default function HomeHeader ({ iconLeft, iconRight, contentCenter }: EntrySubTitleProps) {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  return (
      <View>
          <View style={[styles.header, {
            paddingTop: 20,
            height: height / 7,
            shadowColor: theme === darkTheme ? undefined : theme.mainOpposite,
            shadowOffset: theme === darkTheme ? undefined : { width: 0, height: 3 },
            shadowOpacity: theme === darkTheme ? undefined : 0.15
          }]}>
              <View style={{ flex: 1 }}>
                  <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                      { iconLeft }
                  </View>
              </View>
              <View style={{ justifyContent: 'center', flex: 3 }}>
                  { contentCenter }
              </View>
              <View style={{ flex: 1 }}>
                  <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                      { iconRight }
                  </View>
              </View>
          </View>
      </View>
  )
}
