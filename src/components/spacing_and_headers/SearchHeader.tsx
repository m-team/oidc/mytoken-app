import { Dimensions, View } from 'react-native'
import React, { useContext } from 'react'
import { AuthContext } from '../../utilities/authContext'
import createStylesHome from '../../styles/homeStyles'

/*
This is the header shown when the search button is clicked on the home screen.
It uses composition to splice together the provided props.
 */

interface Props {
    iconLeft: any;
    SearchBar: any;
}
const height = Dimensions.get('window').height

export default function SearchHeader ({ iconLeft, SearchBar }: Props) {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  return (
        <View>
            <View style={[styles.header, {
              paddingTop: 20,
              height: height / 8
            }]}>
                <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                    <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                        { iconLeft }
                    </View>
                </View>
                <View style={{ justifyContent: 'center', flex: 3, paddingRight: 30 }}>
                    { SearchBar }
                </View>
                <View style={{ flex: 1 }}/>
            </View>
        </View>
  )
}
