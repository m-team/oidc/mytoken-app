import { Dimensions, Pressable, StyleSheet, Text, View } from 'react-native'
import { Octicons } from '@expo/vector-icons'
import React, { useContext } from 'react'
import { AuthContext } from '../../utilities/authContext'
import createStylesHome from '../../styles/homeStyles'

/*
This is the navigation header. It uses composition to splice together provided props provided into
 a header.
 */

interface Props {
    nav?: any
    text: string
    children?: any,
    chevronColor?: string,
    backAction?: any
}

const height = Dimensions.get('window').height

export default function StandardHeader (props: Props) {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const localStyles = StyleSheet.create({
    headerAdjustments: {
      paddingTop: 30,
      backgroundColor: '#00000000',
      height: height / 7
    }
  })
  return (
        <View style={{ flexDirection: 'column' }}>
            <View style={[styles.standardHeader, localStyles.headerAdjustments]}>
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                        { props.nav
                          ? <Pressable hitSlop={20} onPress={() => {
                            props.backAction && props.backAction()
                            props.nav.goBack()
                          }}>
                                <Octicons name="chevron-left" size={37} color= {props.chevronColor ? props.chevronColor : theme.standardHeaderColor}/>
                            </Pressable>
                          : <View/>
                        }
                    </View>
                </View>
                <View style={{ flexDirection: 'column', justifyContent: 'center', flex: 3 }}>
                    <Text style={[styles.headerText, { color: theme.backgroundSubtle }]}>
                        {props.text}
                    </Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                    {props.children}
                </View>
            </View>
        </View>
  )
}
