import { Text } from 'react-native'
import React, { useContext } from 'react'
import { AuthContext } from '../../utilities/authContext'

/*
A formatted subtitle for text entry fields
 */

interface EntrySubTitleProps {
    text: string;
}

export default function EntrySubTitle (props: EntrySubTitleProps) {
  const { theme } = useContext(AuthContext)

  const text = props.text
  return (
        <Text style={{ fontFamily: 'Inter_400Regular', fontSize: 14, color: theme.mainOpposite, paddingBottom: 4, paddingLeft: 3 }}>
            {text}
        </Text>
  )
}
