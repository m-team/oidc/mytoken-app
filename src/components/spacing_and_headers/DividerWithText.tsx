import { StyleSheet, Text, View } from 'react-native'
import React, { useContext } from 'react'
import { AuthContext } from '../../utilities/authContext'

/*
This is a text divider with a text description
 */

interface textDividerProps {
    text: string;
}

export default function DividerWithText (props: textDividerProps) {
  const { theme } = useContext(AuthContext)
  const localStyles = StyleSheet.create({
    textStyle: {
      fontFamily: 'Inter_400Regular',
      fontSize: 12,
      color: theme.smallHeaderColor,
      paddingBottom: 6,
      paddingLeft: 10
    }
  })
  const text = props.text
  return (
        <View style={{ borderBottomColor: theme.dividerColor, borderBottomWidth: 1.5, marginHorizontal: 35 }}>
            <Text style={localStyles.textStyle}>
                {text}
            </Text>
        </View>

  )
}
