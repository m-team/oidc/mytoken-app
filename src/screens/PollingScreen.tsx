import React, { useContext, useEffect, useRef, useState } from 'react'
import StandardHeader from '../components/spacing_and_headers/StandardHeader'
import { Linking, StyleSheet, Text, View } from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons'
import { getTokenExpiration, polling } from '../api/axiosRequest'
import { MyContext } from '../utilities/appContext'
import { alertTextOnly, alertTextOnlySuccess } from '../utilities/alertHelper'
import { addMasterToken } from '../utilities/contextUtilities'
import { AuthContext } from '../utilities/authContext'
import { StatusBar } from 'expo-status-bar'
import { setFirstSetupLocalStorage } from '../api/localUnsecureStorage/localConfigurations'
import { getDomainAddresses } from '../api/localUnsecureStorage/localDomainAdresses'
import { DEFAULT_COLOR, lightTheme } from '../api/constants/colors'
import createStylesHome from '../styles/homeStyles'

/*
This screen is shown to the user once they try to setup a master token is configured and they are prompted to confirm their
 identity in the browser. If they return to the app before doing so, they have the option to visit the
 provided url on another device.
 */

interface Props {
    navigation: any;
    route: any;
}

const PollingScreen = (props: Props) => {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const pollingLink = props.route.params.pollingLink
  const pollingCode = props.route.params.pollingCode
  const providerName = props.route.params.providerName
  const providerUrl = props.route.params.providerUrl
  const name = props.route.params.name
  const domainUrl = props.route.params.domainUrl
  const expiresIn = props.route.params.expiresIn
  const color = props.route.params.color
  const { setMasterTokenList, masterTokenList, domainList, setDomainList, setFirstSetup } = useContext(MyContext)
  const { loggedIn } = useContext(AuthContext)
  const [abort, setAbort] = useState<boolean>(false)
  const abortRef = useRef(abort)
  function timeout (ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms))
  }
  const pollingAwait = async () => {
    const resolvedURL = await getDomainAddresses(domainUrl, domainList, setDomainList)
    const startTime = new Date().getTime()
    while (!abortRef.current) {
      const currentTime = new Date().getTime()
      const elapsedTime = (currentTime - startTime) / 1000
      if (elapsedTime > expiresIn) {
        alertTextOnly('Authentication expired')
        if (props.navigation) {
          props.navigation.goBack()
        }
        break
      }
      await timeout(5000)
      const data = await polling(pollingCode, resolvedURL)
      if (data) {
        return data
      }
    }
  }
  useEffect(() => {
    abortRef.current = abort
  }, [abort])

  const backArrowAction = () => {
    alertTextOnly('Aborted online confirmation process', 'Information')
    setAbort(true)
  }
  useEffect(() => {
    async function prepare () {
      const resolvedURL = await getDomainAddresses(domainUrl, domainList, setDomainList)
      const r = await pollingAwait()
      if (r) {
        const expiration = await getTokenExpiration(r[0], resolvedURL)
        addMasterToken({
          name,
          provider: { url: providerUrl, name: providerName },
          masterToken: r[0],
          momID: r[1],
          creation: new Date(),
          domainUrl: resolvedURL.domainUrl,
          expirationDate: expiration,
          color: color || DEFAULT_COLOR
        }, masterTokenList, setMasterTokenList)
        // props.navigation.dispatch(StackActions.pop(2)); better but cant be done because of welcome screen (or else infinite loop)
        await setFirstSetupLocalStorage(true)
        setFirstSetup(false)
        if (loggedIn) {
          props.navigation.reset({
            index: 0,
            routes: [{ name: 'Home' }]
          })
        }
        alertTextOnlySuccess('Your Master Token ' + name + ' Was created!')
      }
    }
    prepare().catch(() => {
      alertTextOnly('Something went wrong adding your Master Token')
      if (props.navigation) {
        props.navigation.goBack()
      }
    })
  }, [pollingCode])

  return (
        <View>
            <StandardHeader nav={props.navigation} text="Waiting For Verification..." backAction={backArrowAction}/>
            <View style={localStyles.contentContainer}>
                <Text style={styles.mediumText}>
                    Confirm your Token in the browser that opened! Or go to this link:
                    <Text style={{ color: 'blue' }}
                         onPress={() => {
                           Linking.openURL(pollingLink).then(() => {})
                         }}>
                        {pollingLink}
                    </Text>
                </Text>
                <View style={localStyles.truckContainer}>
                    <FontAwesome5 name="truck-loading" size={100} color={theme.backgroundAccentGrey}/>
                </View>
            </View>
            <StatusBar style={theme === lightTheme ? 'dark' : 'light'} backgroundColor={theme.main} />
        </View>
  )
}
const localStyles = StyleSheet.create({
  contentContainer: {
    paddingTop: 60,
    paddingBottom: 10,
    paddingHorizontal: 60
  },
  truckContainer: {
    alignSelf: 'center',
    paddingTop: 150,
    paddingBottom: 50
  }
})

export default PollingScreen
