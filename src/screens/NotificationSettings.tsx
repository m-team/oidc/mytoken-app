import React, { useContext } from 'react'
import StandardHeader from '../components/spacing_and_headers/StandardHeader'
import { Text, View } from 'react-native'
import { StatusBar } from 'expo-status-bar'
import { AuthContext } from '../utilities/authContext'
import createStylesHome from '../styles/homeStyles'
import { lightTheme } from '../api/constants/colors'

/*
This screen can at some point be used to manage notifications globally overwriting
the values set for each sub token.
 */

interface Props {
    navigation: any;
}

const NotificationSettings = (props: Props) => {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  return (
        <View>
            <StandardHeader nav={props.navigation} text="Notifications"/>
            <View style={{ alignSelf: 'center' }}>
                <Text style={styles.headerText}>
                    Coming Soon!!
                </Text>
            </View>
            <StatusBar style={theme === lightTheme ? 'dark' : 'light'} backgroundColor={theme.main} />
        </View>
  )
}

export default NotificationSettings
