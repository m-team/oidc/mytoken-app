import React, { useContext, useState } from 'react'
import { Alert, Dimensions, Pressable, View } from 'react-native'
import MainButton from '../components/buttons/MainButton'
import TextFieldWithHeader from '../components/spacing_and_headers/TextFieldWithHeader'
import StandardHeader from '../components/spacing_and_headers/StandardHeader'
import { AuthContext } from '../utilities/authContext'
import { checkPassword } from '../api/localSecureStorage'
import { alertTextOnly } from '../utilities/alertHelper'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { StatusBar } from 'expo-status-bar'
import { resetData } from '../api/localUnsecureStorage/localManipulation'
import * as Updates from 'expo-updates'
import createStylesField from '../styles/fieldStyles'
import { lightTheme } from '../api/constants/colors'

/*
This screen prompts the user to login if they have the flag set in local storage from
the settings menu. If so it set the Authcontext provided by AppEntry (AppAuthenticator)
to the password and the login flag to true
 */

interface Props {
  navigation: any;
  route: any;
}
const height = Dimensions.get('window').height

const Login = (props: Props) => {
  const { theme } = useContext(AuthContext)
  const inputStyles = createStylesField(theme)
  const { setLoggedIn, setPassword, passSet } = useContext(AuthContext)
  const [entryPassword, onChangeEntryPassword] = useState('')
  const correctPassAction = async () => {
    setPassword(entryPassword)
    setLoggedIn(true)
  }
  const mainButtonAction = async () => {
    checkPassword(entryPassword).then(r => {
      if (r) {
        correctPassAction()
      } else (alertTextOnly('Incorrect password'))
    }).catch(() => {
      alertTextOnly('Something went wrong checking your password')
    })
    onChangeEntryPassword('')
  }
  const resetButtonAction = () => {
    Alert.alert('Warning', 'Do you want to reset your password? All data will be lost', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel'
      },
      {
        text: 'Yes',
        onPress: () => resetData().then(() => {
          Updates.reloadAsync().then(() => {})
        }).catch(() => {
          alertTextOnly('Something went wrong resetting your password')
        })
      }
    ])
  }
  return (
      <View style={{ height: '100%', width: '100%' }}>
        <StandardHeader nav={passSet ? null : props.navigation} text="Login">
            <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                <Pressable hitSlop={20} onPress={resetButtonAction}>
                    <MaterialCommunityIcons name="lock-reset" size={30} color={theme.backgroundGrey} />
                </Pressable>
            </View>
        </StandardHeader>
        <View style={{ paddingTop: 100 }}>
        <TextFieldWithHeader onChangeText={onChangeEntryPassword}
                             value={entryPassword}
                             placeholder="password"
                             placeholderTextColor='#b3b4b5'
                             text="Enter Your Password"
                             secureTextEntry={true}
                             onSubmitEditing={mainButtonAction}/>
        </View>
          <View style={[inputStyles.fixedButtonContainer, { bottom: height / 6 }]}>
          <MainButton onPress={mainButtonAction} text="Unlock"/>
        </View>
          <StatusBar style={theme === lightTheme ? 'dark' : 'light'} backgroundColor={theme.main} />
      </View>
  )
}

export default Login
