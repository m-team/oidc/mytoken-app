import React, { useContext } from 'react'
import { Pressable, ScrollView, TouchableHighlight, View } from 'react-native'
import CardSettings from '../components/content_cards/CardSettings'
import StandardHeader from '../components/spacing_and_headers/StandardHeader'
import { AuthContext } from '../utilities/authContext'
import * as Updates from 'expo-updates'
import { StatusBar } from 'expo-status-bar'
import { Entypo, Ionicons } from '@expo/vector-icons'
import { setThemeLocal } from '../api/localUnsecureStorage/localConfigurations'
import { darkTheme, lightTheme } from '../api/constants/colors'

interface Props {
    navigation: any;
}

/*
This screen presents the settings to the user.
 */

const Menu = (props: Props) => {
  const { theme, setLoggedIn, setTheme } = useContext(AuthContext)
  const nav = props.navigation
  return (
        <View style={{ height: '100%', width: '100%' }}>
            <StandardHeader nav={nav} text="Menu">
              <Pressable hitSlop={20} onPress={() => {
                setThemeLocal(theme === lightTheme ? 'dark' : 'light').then(() => {})
                setTheme(theme === lightTheme ? darkTheme : lightTheme)
              }}>
                  <View style ={{ paddingLeft: 2 }}>
                {
                  theme === lightTheme
                    ? <Ionicons name="ios-moon" size={28} color={theme.standardHeaderColor} />
                    : <Entypo name="light-down" size={35} color={theme.standardHeaderColor} />
                }
                  </View>
              </Pressable>
            </StandardHeader>
            <View style={{ height: '85%', width: '100%' }}>
                <ScrollView style={{ height: '80%', width: '100%' }}>
                    <View style={{ flexDirection: 'column' }}>
                        <TouchableHighlight underlayColor={theme.pressedOptionColor} onPress={() => {
                          nav.navigate('MasterTokenSettings')
                        }}>
                            <CardSettings text="My Providers" icon="person"/>
                        </TouchableHighlight>

                        <TouchableHighlight underlayColor={theme.pressedOptionColor} onPress={() => {
                          nav.navigate('LoginSettings')
                        }}>
                            <CardSettings text="Security" icon="lock"/>
                        </TouchableHighlight>
                        <TouchableHighlight underlayColor={theme.pressedOptionColor} onPress={() => {
                          nav.navigate('AboutSettings')
                        }}>
                            <CardSettings text="About" icon="info"/>
                        </TouchableHighlight>
                        <TouchableHighlight underlayColor={theme.pressedOptionColor} onPress={() => {
                          nav.navigate('PrivacyPolicy')
                        }}>
                            <CardSettings text="Privacy Policy" icon="note"/>
                        </TouchableHighlight>
                        <TouchableHighlight underlayColor={theme.pressedOptionColor} onPress={() => {
                          nav.navigate('ResetScreen')
                        }}>
                            <CardSettings text="Reset" icon="x-circle"/>
                        </TouchableHighlight>
                        <TouchableHighlight underlayColor={theme.pressedOptionColor} onPress={() => {
                          // reloads the entire app
                          Updates.reloadAsync().then(() => {})
                          setLoggedIn(false)
                        }}>
                            <CardSettings text="Lock" icon="sign-out" hideChevron={true}/>
                        </TouchableHighlight>
                    </View>
                </ScrollView>
            <StatusBar style={theme === lightTheme ? 'dark' : 'light'} backgroundColor={theme.main} />
            </View>
        </View>

  )
}

export default Menu
