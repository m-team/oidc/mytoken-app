import React, { useContext } from 'react'
import { Dimensions, Pressable, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import createStylesField, { HITSLOP } from '../styles/fieldStyles'
import { DATA } from '../utilities/welcomeSlides'
import Carousel from '../components/Carousel'
import { AuthContext } from '../utilities/authContext'
import { Octicons } from '@expo/vector-icons'
import { StatusBar } from 'expo-status-bar'
import createStylesHome from '../styles/homeStyles'
import { lightTheme } from '../api/constants/colors'

/*
This is the welcome screen that shows on first launch and if a user has login set.
 */

interface Props {
    navigation: any;
}
const windowHeight = Dimensions.get('window').height
const windowWidth = Dimensions.get('window').width

const Welcome = (props: Props) => {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const stylesInput = createStylesField(theme)

  const localStyles = StyleSheet.create({
    topTextWithPadding: {
      alignSelf: 'center',
      position: 'absolute',
      top: 80,
      color: theme.backgroundSubtle
    },
    centerContentFull: {
      alignSelf: 'center',
      justifyContent: 'center',
      flex: 1,
      height: '100%',
      width: '100%'
    }
  })
  const nav = props.navigation
  const { passSet, loggedIn } = useContext(AuthContext)
  return (
        <View style={{ height: windowHeight, width: windowWidth, flex: 1 }}>
            <View style={ localStyles.centerContentFull }>
                <Text style={[styles.headerText, localStyles.topTextWithPadding]}>
                    mytoken
                </Text>

                <Carousel DATA = {DATA}/>

            </View>
            <View style={[stylesInput.buttonMain, {
              alignSelf: 'center',
              position: 'absolute',
              bottom: windowHeight / 6
            }]}>
                <TouchableOpacity hitSlop={HITSLOP} onPress={() => {
                  nav.navigate(!passSet ? 'LoginSettings' : 'Login')
                }}>
                    <Text style={styles.buttonText}>
                        {!passSet ? 'Next!' : 'Unlock!'}
                    </Text>
                </TouchableOpacity>
            </View>
            <View style={[{
              alignSelf: 'center',
              position: 'absolute',
              bottom: 75
            }]}>
                {
                    loggedIn
                      ? (
                            <Pressable hitSlop={20} onPress={() => {
                              nav.navigate('DomainScreen')
                            }} style={{ marginTop: 12 }}>
                                <Text style={styles.greyedOutText}>
                                    Change Domain
                                </Text>
                                <View style={{ justifyContent: 'center', alignSelf: 'center', paddingTop: 4 }}>
                                    <Octicons name= {'database'} size={20} color={theme.backgroundGrey}/>
                                </View>
                            </Pressable>
                        )
                      : (<View/>)
                }

            </View>
            <StatusBar style={theme === lightTheme ? 'dark' : 'light'} backgroundColor={theme.main} />
        </View>
  )
}

export default Welcome
