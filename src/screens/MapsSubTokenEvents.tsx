import React, { useContext, useEffect, useRef, useState } from 'react'
import { ActivityIndicator, View } from 'react-native'
import StandardHeader from '../components/spacing_and_headers/StandardHeader'
import { TOKEN_EVENT } from '../types'
import MapView, { Marker } from 'react-native-maps'
import Moment from 'moment'
import { parseName } from '../utilities/parsingHelper'
import { StatusBar } from 'expo-status-bar'
import { darkTheme, lightTheme, mapStylingDark } from '../api/constants/colors'
import { AuthContext } from '../utilities/authContext'

/*
This screen presents the user with a google map and markers to the locations that are provided in the params.
technically not good too pass props through navigator (I avoided this EVERYWHERE else), but this is the only option
I saw viable here, using context is a huge workaround for little benefit
 */

interface Props {
    navigation: any;
    route:any;
}

const MapsSubTokenEvents = (props: Props) => {
  const map = useRef<MapView>(null)
  const searchList: TOKEN_EVENT[] = props.route.params.searchList
  const { theme } = useContext(AuthContext)
  const [loading, setLoading] = useState(true)
  const [markerInfoList, setMarkerInfoList] = useState<{
      coordinate: any,
      title: string,
      description: string
  }[]>(
    [])

  useEffect(() => {
    async function populating () {
      const tempMarker:any = markerInfoList
      for (const value of searchList) {
        const locationInfo = value.info
        if (!locationInfo || !locationInfo.latitude || !locationInfo.longitude || !locationInfo.city) {
          continue
        }
        tempMarker.push({
          coordinate: { latitude: locationInfo.latitude, longitude: locationInfo.longitude },
          title: parseName(value.event) + ' - ' + locationInfo.city,
          description: Moment(value.time).format('D.MMM.YY') + ' - ' + value.ip
        })
      }
      setMarkerInfoList(tempMarker)
      setLoading(false)
    }
    populating().catch(console.error)
  }, [])

  if (loading) {
    return (<ActivityIndicator size="small" color="#D07132" style={{ height: '100%', width: '100%' }}/>)
  }
  const isDarkTheme = theme === darkTheme
  return (
        <View style={{ height: '100%', width: '100%' }}>
            <MapView customMapStyle={isDarkTheme ? mapStylingDark : undefined} ref={map} style={{ height: '100%', width: '100%' }} provider={'google'} maxZoomLevel={14} onMapReady={() => {
              if (map.current !== null) {
                map.current.fitToCoordinates(markerInfoList.map((value:any) => { return value.coordinate }), {
                  edgePadding: {
                    top: 100,
                    right: 100,
                    bottom: 130,
                    left: 100
                  },
                  animated: false
                })
              }
            }} >
                {markerInfoList.map((value:any, index: number) => (
                    <Marker
                        key={index}
                        coordinate={value.coordinate}
                        description={value.description}
                        title={value.title}
                    />
                ))}
            </MapView>
            <View style={{ position: 'absolute', top: 0, width: '100%' }}>
                <StandardHeader nav={props.navigation} text="Map for Search-results" chevronColor={isDarkTheme ? undefined : '#264653'}/>
            </View>
            <StatusBar style={theme === lightTheme ? 'dark' : 'light'} translucent={true} />
        </View>
  )
}

export default MapsSubTokenEvents
