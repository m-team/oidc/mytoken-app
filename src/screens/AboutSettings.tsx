import React, { useContext } from 'react'
import { ScrollView, Text, View } from 'react-native'
import DividerWithText from '../components/spacing_and_headers/DividerWithText'
import StandardHeader from '../components/spacing_and_headers/StandardHeader'
import { StatusBar } from 'expo-status-bar'
import { AuthContext } from '../utilities/authContext'
import createStylesHome from '../styles/homeStyles'
import { lightTheme } from '../api/constants/colors'

/*
This screen shows important license and privacy information about the app.
 */

interface AboutSettingsProps {
    navigation: any;
}

const AboutSettings = (props: AboutSettingsProps) => {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  return (
        <View style={{ height: '100%', width: '100%' }}>
            <StandardHeader nav={props.navigation} text="About"/>
            <DividerWithText text="Mytoken About"/>
            <View style={{ height: '75%' }}>
                <ScrollView style={{ marginHorizontal: 45, marginTop: 20 }}>
                    <View style = {{ paddingHorizontal: 10 }}>
                    <Text style={styles.smallText}>
                        Mytoken is a web service to obtain OpenID Connect Access Tokens
                        in an easy but secure way for extended periods of time and across multiple devices.
                        {'\n\n'}
                        This app was created to manage-, create-, delete- and view details of- mytokens. The app,
                        through a simple, minimalistic design, guides the user through the process of creating
                        and transferring their mytokens.
                        {'\n\n'}
                        Within the app, users must authenticate providers. These are confirmed externally through the online OpenID provider. To do this, the app automatically redirects
                        the user to their smartphones browser when authentication is required. When created, the authentication token is encrypted
                        and stored in the persistent storage of the device.
                        {'\n\n'}
                        All tokens created with authenticated providers are shown within the app. Also,
                        using an authenticated provider, users can create new tokens. Creating new tokens does not require an external authentication step.
                        After creation, the user is presented a transfer code that is valid for 5 minutes. Only the authentication for the provider requires storing a token on the device.
                        The powerful provider token is used to fetch details about a tokens usage. These details are visualized using text, charts and maps.
                        {'\n\n'}
                        The mytoken service is developed by Gabriel Zachmann while this mobile app is developed by Morris Baumgarten-Egemole
                    </Text>
                    </View>
                </ScrollView>
            </View>
            <StatusBar style={theme === lightTheme ? 'dark' : 'light'} backgroundColor={theme.main} />
        </View>
  )
}

export default AboutSettings
