import React, { useCallback, useContext, useState } from 'react'
import { Dimensions, Text, View } from 'react-native'
import MainButton from '../components/buttons/MainButton'
import TextFieldWithHeader from '../components/spacing_and_headers/TextFieldWithHeader'
import StandardHeader from '../components/spacing_and_headers/StandardHeader'
import { alertTextOnly, alertTextOnlySuccess } from '../utilities/alertHelper'
import { StatusBar } from 'expo-status-bar'
import { getDomainAddresses } from '../api/localUnsecureStorage/localDomainAdresses'
import { MyContext } from '../utilities/appContext'
import debounce from 'lodash.debounce'
import { AuthContext } from '../utilities/authContext'
import createStylesHome from '../styles/homeStyles'
import createStylesField from '../styles/fieldStyles'
import { lightTheme } from '../api/constants/colors'

interface Props {
    navigation: any;
    route: any;
}
const height = Dimensions.get('window').height

const DomainScreen = (props: Props) => {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const stylesInput = createStylesField(theme)
  const [domainURL, setDomainURL] = useState('https://')
  const { domainList, setDomainList } = useContext(MyContext)

  const saveButtonAction = () => {
    async function buttonActionAsync () {
      await getDomainAddresses(domainURL, domainList, setDomainList, true)
      alertTextOnlySuccess('Domain added: ' + domainURL)
      props.navigation.goBack()
    }
    buttonActionAsync().catch(() => {
      alertTextOnly('Something went wrong adding this domain URL')
    })
  }
  const debouncedMainButtonHandler = useCallback(debounce(saveButtonAction, 2000, {
    leading: true,
    trailing: false
  }), [domainURL])
  return (
        <View style={{ height: '100%', width: '100%' }}>
            <StandardHeader nav={props.navigation} text="Add Domain"/>
            <View style={{ paddingTop: 100 }}>
                <TextFieldWithHeader onChangeText={setDomainURL}
                                     value={domainURL}
                                     placeholder= 'https://'
                                     placeholderTextColor={theme.accentColor}
                                     text="Enter Domain URL"
                                     autoCapitalizeOff={true}
                                     onSubmitEditing={debouncedMainButtonHandler}/>
            </View>
            <View style = {{ paddingHorizontal: 70, paddingTop: 50 }}>
                <Text style={styles.mediumGrayText}>
                    Domain Should be in the following format:{'\n'} https://mytoken.data.kit.edu/ {'\n\n'}
                </Text>
            </View>
            <View style={[stylesInput.buttonMain, stylesInput.fixedButtonContainer, { bottom: height / 6 }]}>
                <MainButton onPress={debouncedMainButtonHandler} text="Save"/>
            </View>
            <StatusBar style={theme === lightTheme ? 'dark' : 'light'} backgroundColor={theme.main} />
        </View>
  )
}

export default DomainScreen
