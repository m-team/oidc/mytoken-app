import React, { useCallback, useContext, useEffect, useState } from 'react'
import { Pressable, ScrollView, StyleSheet, Text, View, Platform, TouchableHighlight, Dimensions } from 'react-native'
import { Entypo } from '@expo/vector-icons'
import EntrySubTitle from '../components/spacing_and_headers/EntrySubTitle'
import DividerWithText from '../components/spacing_and_headers/DividerWithText'
import CheckBoxComponent from '../components/fields/check_box/CheckBoxComponent'
import { MyContext } from '../utilities/appContext'
import MainButton from '../components/buttons/MainButton'
import TextFieldWithHeader from '../components/spacing_and_headers/TextFieldWithHeader'
import StandardHeader from '../components/spacing_and_headers/StandardHeader'
import { TEMPLATE, templateType, TOKEN_CAPABILITIES } from '../types'
import { TOKEN_CAPABILTIES_DEFAULTS, TOKEN_CAPABILTIES_FALSE } from '../api/constants/token_capabilities_constants'
import { getTransferCode } from '../api/axiosRequest'
import { DEFAULT_MASTER_TOKEN, DEFAULT_TEMPLATE } from '../utilities/main_types_defaults'
import { alertTextOnly } from '../utilities/alertHelper'
import { StatusBar } from 'expo-status-bar'
import RNDateTimePicker, { DateTimePickerEvent } from '@react-native-community/datetimepicker'
import TemplatesDropDown from '../components/fields/drop_downs/TemplatesDropDown'
import createStylesField, { HITSLOP_SMALL } from '../styles/fieldStyles'
import Moment from 'moment'
import { resolveDomainURL, resolveMasterToken, updateSubTokenList } from '../utilities/contextUtilities'
import debounce from 'lodash.debounce'
import { AuthContext } from '../utilities/authContext'
import createStylesHome from '../styles/homeStyles'
import { lightTheme } from '../api/constants/colors'

/*
This screen allows the user to configure a new sub token. They can choose various options which only
show if they choose the advanced menu.
 */

interface Props {
    navigation: any;
    route: any;
}
const height = Dimensions.get('window').height

const CreateSubToken = ({ navigation, route }: Props) => {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const fieldStyles = createStylesField(theme)
  const [name, onChangeName] = useState('')
  const [lifeTime, onChangeLifeTime] = useState<boolean>(true)
  const [expiration, setExpiration] = useState<Date>(new Date())
  const [showMode, setShowMode] = useState<'date' | 'time' | undefined>('date')
  const [show, setShow] = useState<boolean>(false)
  const [capabilities, setCapabilities] = useState<TOKEN_CAPABILITIES>(TOKEN_CAPABILTIES_DEFAULTS)
  const { setSubTokenList, masterTokenList, domainList, setConnectionErrors, transferCodeList, setTransferCodeList } = useContext(MyContext)
  const [profile, setProfile] = useState<TEMPLATE>(DEFAULT_TEMPLATE)
  const [capabilitiesTemplate, setCapabilitiesTemplate] = useState<TEMPLATE>(DEFAULT_TEMPLATE)
  const [rotationTemplate, setRotationTemplate] = useState<TEMPLATE>(DEFAULT_TEMPLATE)
  const [restrictionTemplate, setRestrictionTemplate] = useState<TEMPLATE>(DEFAULT_TEMPLATE)
  const chosenMasterToken = resolveMasterToken(route?.params?.chosenMasterToken, masterTokenList) || DEFAULT_MASTER_TOKEN

  // use effect needed for profile and template since selecting these must affect SHOWN settings
  useEffect(() => {
    if (capabilitiesTemplate.capabilities) {
      setCapabilities(profile.capabilities ? profile.capabilities : TOKEN_CAPABILTIES_FALSE)
      if (profile.name !== 'DEFAULT' && profile.restrictions) {
        setExpiration(new Date(profile.restrictions * 1000))
        onChangeLifeTime(!!profile.restrictions)
      } else {
        onChangeLifeTime(true)
      }
      setRestrictionTemplate(DEFAULT_TEMPLATE)
      setRotationTemplate(DEFAULT_TEMPLATE)
      setCapabilitiesTemplate(DEFAULT_TEMPLATE)
    }
  }, [profile])
  useEffect(() => {
    if (profile.name !== 'DEFAULT') {
      setCapabilitiesTemplate(DEFAULT_TEMPLATE)
      return
    }
    if (capabilitiesTemplate.name !== 'DEFAULT' && capabilitiesTemplate.capabilities) {
      setCapabilities(capabilitiesTemplate.capabilities ? capabilitiesTemplate.capabilities : TOKEN_CAPABILTIES_FALSE)
    }
  }, [capabilitiesTemplate])
  useEffect(() => {
    if (profile.name !== 'DEFAULT') {
      setRestrictionTemplate(DEFAULT_TEMPLATE)
      return
    }
    if (restrictionTemplate.name !== 'DEFAULT' && restrictionTemplate.restrictions) {
      setExpiration(new Date(restrictionTemplate.restrictions * 1000))
      onChangeLifeTime(!!restrictionTemplate.restrictions)
    } else {
      onChangeLifeTime(true)
    }
  }, [restrictionTemplate])
  useEffect(() => {
    if (profile.name !== 'DEFAULT') {
      setRotationTemplate(DEFAULT_TEMPLATE)
    }
  }, [rotationTemplate])
  const setMode = (showMode: 'date' | 'time' | undefined) => {
    setShow(true)
    setShowMode(showMode)
  }
  const checkProfileAndCapabilities = () => {
    if (profile.name !== 'DEFAULT' || capabilitiesTemplate.name !== 'DEFAULT') {
      alertTextOnly('You can not edit capabilities when a profile or capabilities template is selected')
      return true
    }
    return false
  }
  const setObtainTokens = () => {
    if (checkProfileAndCapabilities()) { return }
    setCapabilities({ ...capabilities, obtain_tokens: !capabilities.obtain_tokens })
  }
  const setTokenInfo = () => {
    if (checkProfileAndCapabilities()) { return }
    setCapabilities({ ...capabilities, token_info: !capabilities.token_info })
  }
  const setCreateNewToken = () => {
    if (checkProfileAndCapabilities()) { return }
    setCapabilities({ ...capabilities, create_new_token: !capabilities.create_new_token })
  }
  const setReadWriteSettings = () => {
    if (checkProfileAndCapabilities()) { return }
    setCapabilities({ ...capabilities, read_write_settings: !capabilities.read_write_settings })
  }
  const setManageMytokens = () => {
    if (checkProfileAndCapabilities()) { return }
    setCapabilities({ ...capabilities, manage_mytokens: !capabilities.manage_mytokens })
  }
  const [advancedVisible, setAdvancedVisible] = useState(false)
  const toggleAdvanced = () => {
    setAdvancedVisible(!advancedVisible)
  }
  const changeLifeTime = (value: boolean) => {
    if (profile.name !== 'DEFAULT') {
      alertTextOnly('You can not change expiration date preferances when a profile is selected')
      return
    }
    if (restrictionTemplate.name === 'DEFAULT') {
      onChangeLifeTime(value)
      return
    }
    alertTextOnly('You can not change expiration date preferances when a restriction template is selected')
  }
  const setDate = (event: DateTimePickerEvent, date?: Date) => {
    if (restrictionTemplate.name !== 'DEFAULT') return
    setShow(Platform.OS === 'ios')
    const {
      nativeEvent: { timestamp }
    } = event
    if (timestamp) {
      setExpiration(new Date(timestamp))
    }
  }
  const showSelector = () => {
    // differentiate android ios
    if (Platform.OS === 'ios') {
      return <RNDateTimePicker mode={'datetime'} onChange={(event) => {
        setDate(event)
        if (restrictionTemplate.name !== 'DEFAULT') {
          alertTextOnly('You can not change the expiration date when a restriction template is selected')
        }
      }} value={expiration} minimumDate={new Date()}/>
    }
    return (
    <View style = {{ flexDirection: 'row' }}>
        <TouchableHighlight onPress={() => {
          if (restrictionTemplate.name === 'DEFAULT') {
            setMode('date')
          } else {
            alertTextOnly('You can not change the expiration date when a restriction template is selected')
            setShow(false)
          }
        }} hitSlop={HITSLOP_SMALL}>
          <View style={fieldStyles.dateCircleButton}>
            <Text style={{ fontFamily: 'Inter_400Regular', fontSize: 16, alignSelf: 'center' }}>
              {Moment(expiration).format('D.MMM.YY')}
            </Text>
          </View>
        </TouchableHighlight>
      <View style={{ width: 13 }}/>
        <TouchableHighlight onPress={() => {
          if (restrictionTemplate.name === 'DEFAULT') {
            setMode('time')
          } else {
            alertTextOnly('You can not change the expiration date when a restriction template is selected')
          }
        }} hitSlop={HITSLOP_SMALL}>
          <View style={fieldStyles.dateCircleButton}>
            <Text style={{ fontFamily: 'Inter_400Regular', fontSize: 16, alignSelf: 'center' }}>
              {Moment(expiration).format('HH:mm')}
            </Text>
          </View>
        </TouchableHighlight>
      { show && restrictionTemplate.name === 'DEFAULT' && <RNDateTimePicker mode={showMode} onChange={setDate} value={expiration} minimumDate={new Date()}/>}
    </View>)
  }
  const mainButtonAction = async () => {
    try {
      if (name === '') {
        alertTextOnly('You need to enter a name')
        return
      }
      if (chosenMasterToken.name === 'DEFAULT') {
        alertTextOnly('You need to choose a Master Token')
        return
      }
      if (name.length > 20) {
        alertTextOnly('The name is too long')
        return
      }
      const fetchedCode = await getTransferCode(
        name,
        chosenMasterToken.masterToken,
        capabilities,
        resolveDomainURL(chosenMasterToken.domainUrl, domainList),
        profile,
        capabilitiesTemplate,
        restrictionTemplate,
        rotationTemplate,
        // divide by 1000 because unix time in seconds
        lifeTime ? expiration : undefined
      )
      await updateSubTokenList(masterTokenList, setSubTokenList, domainList, setConnectionErrors)
      setTransferCodeList(transferCodeList.concat([fetchedCode]))
      navigation.navigate('TransferToken', {
        transferCode: fetchedCode.transferCode,
        subTokenID: name
      })
    } catch (e) {
      alertTextOnly('Something went wrong creating your new Token')
      navigation.goBack()
    }
  }
  const debouncedMainButtonHandler = useCallback(debounce(mainButtonAction, 3000, {
    leading: true,
    trailing: false
  }), [name, rotationTemplate, restrictionTemplate, lifeTime, expiration, showMode, show, chosenMasterToken, capabilities, profile, capabilitiesTemplate])

  return (
        <View style={{ height: '100%', width: '100%' }}>
            <View style={{ flexDirection: 'column' }}>
                <StandardHeader nav={navigation} text="Create Your New Token"/>
                <ScrollView style={{ height: '100%' }}>
                  <View style={{ paddingTop: 30 }}>
                        <TextFieldWithHeader onChangeText={onChangeName}
                                             value={name}
                                             placeholder="name"
                                             placeholderTextColor={theme.accentColor} text="Name"/>
                    </View>
                    <Pressable hitSlop={20} onPress={() => {
                      toggleAdvanced()
                    }} style={{ marginTop: 12 }}>
                                <Text style={styles.greyedOutText}>
                                    Advanced
                                </Text>
                                <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                                    <Entypo name= {advancedVisible ? 'chevron-up' : 'chevron-down'} size={20} color="#AEAEB4"/>
                                </View>
                            </Pressable>

                        <View style={[{ height: '100%', width: '100%' }, !advancedVisible && { display: 'none' }]}>
                          <View style={{ flexDirection: 'column', paddingTop: 5 }}>
                            <View style={{ paddingHorizontal: 70, paddingBottom: 20 }}>
                              <EntrySubTitle text="Profiles"/>
                              <TemplatesDropDown template={profile} setTemplate={setProfile} chosenMasterToken={chosenMasterToken} templateUsed={templateType.PROFILE}
                                                 />
                            </View>
                            <DividerWithText text="Capabilities"/>
                            <View style={{ paddingTop: 15, paddingHorizontal: 70, paddingBottom: 5 }}>
                              <EntrySubTitle text="Capabilities Templates"/>
                              <TemplatesDropDown template={capabilitiesTemplate} setTemplate={setCapabilitiesTemplate} chosenMasterToken={chosenMasterToken} templateUsed={templateType.CAPTABILITIES}
                                                 errorOne={profile.name === 'DEFAULT'} messageOne={'You can not edit capabilities when a profile is selected'}
                                                 />

                            </View>
                            <CheckBoxComponent text="Obtain OpenID Tokens" value={capabilities.obtain_tokens}
                                               setValue={setObtainTokens} locked={profile.name !== 'DEFAULT' || capabilitiesTemplate.name !== 'DEFAULT'}/>
                            <CheckBoxComponent text="TokenInfo" value={capabilities.token_info}
                                               setValue={setTokenInfo} locked={profile.name !== 'DEFAULT' || capabilitiesTemplate.name !== 'DEFAULT'}/>
                            <CheckBoxComponent text="Manage Tokens" value={capabilities.manage_mytokens}
                                               setValue={setManageMytokens} locked={profile.name !== 'DEFAULT' || capabilitiesTemplate.name !== 'DEFAULT'}/>
                            <CheckBoxComponent text="Create new Token" value={capabilities.create_new_token}
                                               setValue={setCreateNewToken} locked={profile.name !== 'DEFAULT' || capabilitiesTemplate.name !== 'DEFAULT'}/>
                            <CheckBoxComponent text="Menu" value={capabilities.read_write_settings}
                                               setValue={setReadWriteSettings} locked={profile.name !== 'DEFAULT' || capabilitiesTemplate.name !== 'DEFAULT'}/>
                              <View style={{ paddingBottom: 35 }}/>
                              <DividerWithText text="Token expiration"/>
                            <View style={{ paddingTop: 15, paddingHorizontal: 70, paddingBottom: 5 }}>
                              <EntrySubTitle text="Restrictions Templates"/>
                              <TemplatesDropDown template={restrictionTemplate} setTemplate={setRestrictionTemplate} chosenMasterToken={chosenMasterToken} templateUsed={templateType.RESTRICTIONS}
                                                 errorOne={profile.name === 'DEFAULT' } messageOne={'You can not edit restrictions when a profile is selected'}
                                                 />

                            </View>
                              <CheckBoxComponent text="Set Expiration Date" value={lifeTime}
                                                 setValue={changeLifeTime} locked={profile.name !== 'DEFAULT' || restrictionTemplate.name !== 'DEFAULT'}/>
                                <View style={{ alignSelf: 'center', alignItems: 'center', paddingTop: 15 }}>
                                      {advancedVisible && lifeTime && profile.name === 'DEFAULT' && restrictionTemplate.name === 'DEFAULT' && showSelector()}
                                </View>
                              <View style={{ height: 35 }}/>
                              <DividerWithText text="Rotation"/>
                              <View style={{ paddingHorizontal: 70, paddingTop: 10 }}>
                                  <EntrySubTitle text="Rotation Templates"/>
                                  <TemplatesDropDown template={rotationTemplate} setTemplate={setRotationTemplate} chosenMasterToken={chosenMasterToken} templateUsed={templateType.ROTATION}
                                                     errorOne={profile.name === 'DEFAULT'} messageOne={'You can not edit rotation when a profile is selected'}
                                                     />
                              </View>
                              </View>
                        </View>
                  <View style={{ paddingBottom: height / 2 }}/>
                  <StatusBar style={theme === lightTheme ? 'dark' : 'light'} backgroundColor="white" />
                  {
                      advancedVisible && <View style={[localStyles.mainButtonFixed, { bottom: height / 2.5 }]}>
                        <MainButton onPress={debouncedMainButtonHandler} text="Create"/>
                      </View>
                  }
                </ScrollView>
              {
                !advancedVisible && <View style={[localStyles.mainButtonFixed, { bottom: height / 3.5 }]}>
                  <MainButton onPress={debouncedMainButtonHandler} text="Create"/>
                </View>
              }
            </View>
        </View>

  )
}
const localStyles = StyleSheet.create({
  mainButtonFixed: {
    justifyContent: 'center',
    alignSelf: 'center',
    position: 'absolute'
  },
  centerContent: {
    justifyContent: 'center',
    alignSelf: 'center'
  }
})

export default CreateSubToken
