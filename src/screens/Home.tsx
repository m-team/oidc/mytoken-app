import React, { useContext, useEffect, useState } from 'react'
import {
  Alert,
  Pressable,
  RefreshControl,
  ScrollView,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import { Entypo, Octicons } from '@expo/vector-icons'
import DividerWithText from '../components/spacing_and_headers/DividerWithText'
import { MyContext } from '../utilities/appContext'
import { searchSubTokenList } from '../utilities/parsingHelper'
import CreateNewButton from '../components/buttons/CreateNewButton'
import { MasterToken, SubToken } from '../types'
import SearchBar from '../components/fields/search_bar/SearchBar'
import { HITSLOP_SQUARE } from '../styles/fieldStyles'
import HomeHeader from '../components/spacing_and_headers/HomeHeader'
import SearchHeader from '../components/spacing_and_headers/SearchHeader'
import { StatusBar } from 'expo-status-bar'
import { filterProviders, updateSubTokenList } from '../utilities/contextUtilities'
import { AuthContext } from '../utilities/authContext'
import createStylesHome from '../styles/homeStyles'
import HomeItem from '../components/fields/HomeItem'
import MasterTokenDropDown from '../components/fields/drop_downs/MasterTokenDropDown'
import { DEFAULT_MASTER_TOKEN, DEFAULT_PROVIDER } from '../utilities/main_types_defaults'
import { alertTextOnly } from '../utilities/alertHelper'

/*
This screen is the apps homes screen. It presents the sub tokens to the user and allows them to navigate to
fetch new sub tokens. If no sub tokens are present it shows some informational text.
 */

interface Props {
    navigation: any;
}

const Home = (props: Props) => {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const nav = props.navigation
  const { subTokenList, masterTokenList, connectionErrors, setSubTokenList, domainList, setConnectionErrors } = useContext(MyContext)
  const [search, onChangeSearch] = useState('')
  const [searchVisible, setSearchVisible] = useState(false)
  const [searchList, setSearchList] = useState<SubToken[]>([])
  const [refreshing, setRefreshing] = useState<boolean>(false)
  const [chosenProvider, setChosenProvider] = useState<MasterToken>(DEFAULT_MASTER_TOKEN)

  const onRefresh = React.useCallback(() => {
    setRefreshing(true)
    updateSubTokenList(masterTokenList, setSubTokenList, domainList, setConnectionErrors).then(() => {
      setRefreshing(false)
    })
  }, [])
  const toggleSearch = () => {
    setSearchVisible(!searchVisible)
    setSearchList(subTokenList)
  }
  useEffect(() => {
    setSearchList(searchSubTokenList(search, subTokenList))
  }, [search])

  useEffect(() => {
    setSearchList(subTokenList)
  }, [subTokenList])
  const renderErrors = () => {
    return (
        <CreateNewButton onPress={renderErrorsAction} text="Connection Error" icon="cycle"/>
    )
  }
  const renderErrorsAction = () => {
    Alert.alert('Reload?', 'Connection error due to: \n' + [...new Set(connectionErrors)].map(x => x.masterToken + ': ' + x.domainURL).join('\n'), [
      {
        text: 'Cancel',
        onPress: () => {},
        style: 'cancel'
      },
      {
        text: 'Yes',
        onPress: () => updateSubTokenList(masterTokenList, setSubTokenList, domainList, setConnectionErrors)
      }])
  }

  return (
        <View style={{ height: '100%', width: '100%' }}>
            {
                searchVisible
                  ? (
                    <SearchHeader
                        SearchBar={
                            <SearchBar onChangeSearch={onChangeSearch} search={search} dark={true} autoFocus/>
                        }
                        iconLeft={
                                <Pressable hitSlop={20} onPress={() => {
                                  toggleSearch()
                                }}>
                                    <Octicons name="x" size={32} color={theme.backgroundAccentGrey}/>
                                </Pressable>
                        }
                        />
                    )
                  : (
                    <HomeHeader
                        contentCenter={
                            <Text style={styles.headerText}>
                                <Text style={{ color: theme.mytokenColor }}>my</Text>token
                            </Text>
                        }
                        iconLeft={
                            <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }} onPress={() => {
                              nav.navigate('Settings')
                            }}>
                                <Octicons name="three-bars" size={29} color={theme.backgroundAccentGrey}/>
                            </TouchableOpacity>
                        }
                        iconRight={
                            <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                                <Pressable hitSlop={HITSLOP_SQUARE} onPress={() => {
                                  toggleSearch()
                                }}>
                                    <Octicons name="search" size={28} color={theme.backgroundAccentGrey}/>
                                </Pressable>
                            </View>
                        }/>
                    )
            }
            <View style={{ paddingTop: 20, maxHeight: '82%' }}>
              <View style={{ paddingHorizontal: 40, paddingBottom: 10 }}>
                <MasterTokenDropDown navigation={nav} chosenProvider={chosenProvider} setChosenProvider={setChosenProvider}/>
              </View>
                {connectionErrors && connectionErrors.length ? renderErrors() : <View/>}
                {subTokenList.length ? <DividerWithText text="Tokens"/> : <View/>}
                <ScrollView refreshControl={
                  <RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>
                }>
                    {
                        !searchList.length
                          ? !searchVisible
                              ? (
                            <View style={{ paddingTop: 30, paddingBottom: 30, paddingHorizontal: 60 }}>
                                <Text style={styles.mediumGrayText}>
                                    Create new tokens and share them to other devices! Other tokens you have created using the provider you authenticated are shown here too.
                                </Text>
                            </View>)
                              : (<View style={{ height: 30, width: 50, alignSelf: 'center' }}>
                            <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center', paddingTop: 10 }}>
                                <Entypo name="dots-three-horizontal" size={27} color={theme.backgroundAccentGrey}/>
                            </View>
                        </View>)
                          : (
                              filterProviders(chosenProvider.provider, searchList.slice().reverse(), masterTokenList).map((values: SubToken) => {
                                return <HomeItem key={values.momID || values.name} nav={nav} depth={0} value={values}/>
                              }))
                    }
                </ScrollView>
                <View style={{ marginTop: 25 }}/>
                <CreateNewButton onPress={() => {
                  if (chosenProvider.provider === DEFAULT_PROVIDER) {
                    alertTextOnly('Choose a provider you want to create your new token with', 'Hold on')
                  } else {
                    nav.navigate('CreateSubToken', {
                      chosenMasterToken: chosenProvider.masterToken
                    })
                  }
                }} text="Create New Token" icon="plus"/>
            </View>
            <StatusBar style="light" backgroundColor={theme.headerColor} />
        </View>
  )
}

export default Home
