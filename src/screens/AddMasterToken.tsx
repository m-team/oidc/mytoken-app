import React, { useCallback, useContext, useEffect, useState } from 'react'
import { Dimensions, Linking, Pressable, ScrollView, StyleSheet, Text, View } from 'react-native'
import MainButton from '../components/buttons/MainButton'
import StandardHeader from '../components/spacing_and_headers/StandardHeader'
import ProviderDropDown from '../components/fields/drop_downs/ProviderDropDown'
import { DOMAIN_URLS, OpenIDProvider } from '../types'
import EntrySubTitle from '../components/spacing_and_headers/EntrySubTitle'
import { getConfirm } from '../api/axiosRequest'
import { DEFAULT_DOMAIN_URL, DEFAULT_PROVIDER } from '../utilities/main_types_defaults'
import { alertTextOnly } from '../utilities/alertHelper'
import { MyContext } from '../utilities/appContext'
import { StatusBar } from 'expo-status-bar'
import DomainDropDown from '../components/fields/drop_downs/DomainDropDown'
import { Entypo } from '@expo/vector-icons'
import debounce from 'lodash.debounce'
import { AuthContext } from '../utilities/authContext'
import createStylesHome from '../styles/homeStyles'
import createStylesField from '../styles/fieldStyles'
import { lightTheme } from '../api/constants/colors'

/*
This screen allows the user to add a master token. It is not configurable apart from the required functions.
It automatically opens link in accords with the OIDC flow and moves the user to the polling screen at the same time.
 */

interface Props {
    navigation: any;
    route: any;
}
const height = Dimensions.get('window').height

const AddMasterToken = (props: Props) => {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const stylesInput = createStylesField(theme)
  const nav = props.navigation
  const { firstSetup, domainList } = useContext(MyContext)
  const [provider, setProvider] = useState<OpenIDProvider>(DEFAULT_PROVIDER)
  const [domainUrl, setDomain] = useState<DOMAIN_URLS>((domainList && domainList.filter(x => x.visible).length && domainList.filter(x => x.visible)[0]) || DEFAULT_DOMAIN_URL)
  const [advancedVisible, setAdvancedVisible] = useState<boolean>(false)

  const toggleAdvanced = () => {
    setAdvancedVisible(!advancedVisible)
  }
  const mainButtonAction = () => {
    if (provider.name === 'DEFAULT') {
      alertTextOnly('You need to choose a provider')
      return
    }
    if (domainUrl === undefined) {
      alertTextOnly('You must select a Domain')
      return
    }
    getConfirm(provider, domainUrl).then(r => {
      nav.navigate('PollingScreen', {
        pollingCode: r[0],
        pollingLink: r[1],
        providerName: provider.name,
        providerUrl: provider.url,
        name: provider.name,
        domainUrl: domainUrl.domainUrl,
        expiresIn: r[2]
      })
      Linking.openURL(r[1]).then(() => {})
    }).catch(() => {
      alertTextOnly('Something went wrong authenticating your provider')
    })
  }
  const debouncedMainButtonHandler = useCallback(debounce(mainButtonAction, 2000, {
    leading: true,
    trailing: false
  }), [provider, domainUrl])
  useEffect(() => {
    setProvider(DEFAULT_PROVIDER)
  }, [domainUrl])

  return (
        <View style={{ height: '100%', width: '100%' }}>
            <StandardHeader nav={firstSetup ? undefined : props.navigation} text="Add Provider"/>
            <View style={{ paddingBottom: height / 9 }}>
            <ScrollView style={{ height: '82%' }}>
                {firstSetup
                  ? (<View style={localStyles.firstSetupPadding}>
                    <Text style={styles.mediumGrayText}>
                        Providers are authenticated on your device. Use them to create transferable tokens on the home screen or show other tokens you have created previously.
                    </Text>
                </View>)
                  : (<View/>)
                }
                <View style={{ paddingHorizontal: 70 }}>
                    <EntrySubTitle text="OpenID Provider"/>
                    <ProviderDropDown provider={provider} setProvider={setProvider} domainUrl={domainUrl}/>
                </View>
                <Pressable hitSlop={20} onPress={() => {
                  toggleAdvanced()
                }} style={{ marginTop: 12 }}>
                    <Text style={styles.greyedOutText}>
                        Advanced
                    </Text>
                    <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                        <Entypo name= {advancedVisible ? 'chevron-up' : 'chevron-down'} size={20} color="#AEAEB4"/>
                    </View>
                </Pressable>
                {
                    advancedVisible && <View>
                        <View style={{ paddingHorizontal: 70, paddingBottom: 20 }}>
                            <EntrySubTitle text="Domain URL"/>
                            <DomainDropDown domain={domainUrl} setDomain={setDomain} navigation={nav}/>
                        </View></View>
                }
                <StatusBar style={theme === lightTheme ? 'dark' : 'light'} backgroundColor={theme.main} />
                <View style={{ paddingBottom: height / 2.5 }}/>
            </ScrollView>
            <View style={[stylesInput.buttonMain, stylesInput.fixedButtonContainer, { bottom: height / 8 }]}>
                    <MainButton onPress={ debouncedMainButtonHandler } text="Add"/>
            </View>
            </View>
        </View>
  )
}
const localStyles = StyleSheet.create({
  firstSetupPadding: {
    paddingTop: 15,
    paddingBottom: 30,
    paddingHorizontal: 60
  }
})

export default AddMasterToken
