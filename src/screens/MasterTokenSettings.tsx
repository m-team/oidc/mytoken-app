import React, { useContext, useEffect, useState } from 'react'
import { Alert, ScrollView, Text, View } from 'react-native'
import { MyContext } from '../utilities/appContext'
import StandardHeader from '../components/spacing_and_headers/StandardHeader'
import CardMasterTokenSettings from '../components/content_cards/CardMasterTokenSettings'
import CreateNewButton from '../components/buttons/CreateNewButton'
import { MasterToken, SubToken } from '../types'
import { revokeToken } from '../api/axiosRequest'
import { filterMasterTokensForProvider, removeMasterToken, resolveDomainURL } from '../utilities/contextUtilities'
import { alertTextOnly } from '../utilities/alertHelper'
import { StatusBar } from 'expo-status-bar'
import { AuthContext } from '../utilities/authContext'
import createStylesHome from '../styles/homeStyles'
import Moment from 'moment/moment'
import { lightTheme } from '../api/constants/colors'

/*
This screen allows the user to manage their mater tokens. They can remove tokens and proceed to another screen
from here to add other ones.
 */

interface Props {
    navigation: any;
}

const MasterTokenSettings = (props: Props) => {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const { masterTokenList, setMasterTokenList, domainList, subTokenList } = useContext(MyContext)

  const [masterTokenCounts, setMasterTokenCounts] = useState<Map<string, number>>(
    new Map()
  )
  useEffect(() => {
    const countSubTokens = (subTokens: SubToken[]) => {
      let count = 0
      const queue: SubToken[] = [...subTokens]
      while (queue.length > 0) {
        const subToken = queue.shift()
        if (subToken) {
          count++
          if (subToken.children) {
            queue.push(...subToken.children)
          }
        }
      }
      return count
    }

    setMasterTokenCounts(() => {
      const tempProviderSubTokenCounts = new Map()
      masterTokenList.forEach((masterToken) => {
        const matchingSubTokens = subTokenList.filter(
          (subToken) => subToken.parent === masterToken.masterToken
        )
        const count = countSubTokens(matchingSubTokens)
        const currentCount = tempProviderSubTokenCounts.get(masterToken.provider.name) || 0
        tempProviderSubTokenCounts.set(masterToken.provider.name, currentCount + count)
      })
      return tempProviderSubTokenCounts
    })
  }, [masterTokenList, subTokenList])
  const cardPressedAction = (values: MasterToken) => {
    const completeActionRevoke = async (recursive: boolean) => {
      try {
        const revokeThese = masterTokenList.filter(val => val.provider.name === values.provider.name)
        await Promise.all(revokeThese.map(async tempToken => {
          await revokeToken(tempToken.masterToken, recursive, resolveDomainURL(tempToken.domainUrl, domainList))
        }))
        revokeThese.forEach(tempVal => {
          removeMasterToken(tempVal, masterTokenList, setMasterTokenList)
        })
      } catch (e) {
        alertTextOnly('Something went wrong revoking your tokens')
      }
    }
    Alert.alert('Warning', 'Do you want to revoke all your ' + values.provider.name + ' provider app tokens?', [
      {
        text: 'Cancel',
        onPress: () => {},
        style: 'cancel'
      },
      {
        text: 'Yes',
        onPress: () => {
          // delete from context
          Alert.alert('Notice', 'Do you want to revoke all tokens created with this provider? They are not visible from within the app beyond this point.', [
            {
              text: 'Cancel',
              onPress: () => {},
              style: 'cancel'
            },
            {
              text: 'No',
              onPress: () => completeActionRevoke(false),
              style: 'default'
            },
            {
              text: 'Yes',
              onPress: () => completeActionRevoke(true),
              style: 'default'
            }])
        }
      }])
  }

  return (
        <View style={{ height: '100%', width: '100%' }}>
            <View style={{ flexDirection: 'column' }}>
                <StandardHeader nav={props.navigation} text="My Providers"/>
                <View style={{ maxHeight: '75%' }}>
                  <ScrollView>
                        {
                            !masterTokenList.length
                              ? <View style={{ paddingTop: 30, paddingBottom: 30, paddingHorizontal: 60 }}>
                                    <Text style={styles.mediumGrayText}>
                                        Providers are authenticated on your device. Use them to create transferable tokens or have all your tokens shown you have created with this provider
                                    </Text>
                                </View>
                              : filterMasterTokensForProvider(masterTokenList).map((values: MasterToken, index: number) => (
                                    <View key={index}>
                                        <CardMasterTokenSettings title={values.provider.name} subTitle={values.expirationDate ? 'Expires - ' + Moment(values.expirationDate).format('D.MMM.YY HH:mm') : 'Does not Expire'}
                                                                 subTokenCount={masterTokenCounts.get(values.provider.name) || 0}
                                                                 onPress={() => {
                                                                   cardPressedAction(values)
                                                                 }} expirationDate={values.expirationDate}
                                        masterToken={values.masterToken}
                                        nav={props.navigation}/>
                                    </View>
                              ))
                        }
                    </ScrollView>
                </View>
                <View style={{ marginTop: 25 }}/>
                <CreateNewButton onPress={() => {
                  props.navigation.navigate('AddMasterToken')
                }} text="Add New Provider" icon="plus"/>
            </View>
          <StatusBar style={theme === lightTheme ? 'dark' : 'light'} backgroundColor={theme.main} />
        </View>
  )
}

export default MasterTokenSettings
