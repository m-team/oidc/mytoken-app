import React, { useCallback, useContext, useEffect, useState } from 'react'
import { ActivityIndicator, Alert, Dimensions, Pressable, Text, View } from 'react-native'
import { Octicons } from '@expo/vector-icons'
import { MyContext } from '../utilities/appContext'
import Moment from 'moment'
import DividerWithText from '../components/spacing_and_headers/DividerWithText'
import Heatmap from '../components/Heatmap'
import { MasterToken, SubToken, TOKEN_EVENT } from '../types'
import { getTokenHistory, revokeToken } from '../api/axiosRequest'
import { resolveDomainURL, resolveMasterToken, updateSubTokenList } from '../utilities/contextUtilities'
import CheckBoxFilter from '../components/fields/check_box/CheckboxFilter'
import { getTagsForEvents, getTagsForEventsIgnore } from '../api/generateBody'
import SearchComponentSubToken from '../components/SearchComponentSubToken'
import { HITSLOP_SQUARE } from '../styles/fieldStyles'
import HomeHeader from '../components/spacing_and_headers/HomeHeader'
import { alertTextOnly } from '../utilities/alertHelper'
import { StatusBar } from 'expo-status-bar'
import { getIPLocation } from '../api/localUnsecureStorage/localGeoIPCache'
import debounce from 'lodash.debounce'
import { AuthContext } from '../utilities/authContext'
import createStylesHome from '../styles/homeStyles'
import { DEFAULT_SUBTOKEN } from '../utilities/main_types_defaults'
import { lightTheme } from '../api/constants/colors'

/*
This screen shows when a sub token is selected from the home screen. In essence, it is split into two
 additional components, SearchComponentSubToken and Heatmap. The former is in charge of the search bar
 and the content that is searcher for and the latter for the heatmap shown on the bottom. This screen
 can also take one to the map
 */

interface Props {
    navigation: any;
    route: any;
}
const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const SubTokenDetails = (props: Props) => {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const nav = props.navigation
  const { subTokenList, masterTokenList, domainList, setSubTokenList, setConnectionErrors } = useContext(MyContext)

  // how to get this from a parent? Use context to store them globally or just fetch it ourselves every time from here?
  const [allEvents, setAllEvents] = useState<TOKEN_EVENT[]>([])
  const [filterList, setFilterList] = useState<TOKEN_EVENT[]>([])

  const [tokenHistory, setTokenHistory] = useState<boolean>(true)
  const [tokenChildren, setTokenChildren] = useState<boolean>(true)
  const [tokenRotation, setTokenRotation] = useState<boolean>(true)
  const [other, setOther] = useState<boolean>(true)
  const [recursive, setRecursive] = useState<boolean>(true)

  const [transferSearch, setTransferSearch] = useState<string>('')
  const [detailsLoaded, setDetailsLoaded] = useState<boolean>(false)
  const [reloadCounter, setReloadCounter] = useState<number>(0)

  const [parentToken, setParentToken] = useState<MasterToken>()
  const [hide, setHide] = useState<boolean>(false)
  const getSubToken = (momID: string): SubToken => {
    const queue: SubToken[] = [...subTokenList]
    while (queue.length > 0) {
      const subToken = queue.shift()
      if (subToken?.momID === momID) {
        return subToken
      }
      if (subToken?.children) {
        queue.push(...subToken.children)
      }
    }
    return DEFAULT_SUBTOKEN
  }
  const subToken = getSubToken(props.route.params.subTokenID)
  const toggleHide = () => {
    setHide(!hide)
  }

  const debouncedInvertTokenHistory = useCallback(debounce((bool) => setTokenHistory(bool), 1000, {
    leading: true,
    trailing: false
  }), [])

  const debouncedInvertTokenChildren = useCallback(debounce((bool) => setTokenChildren(bool), 1000, {
    leading: true,
    trailing: false
  }), [])

  const debouncedInvertTokenRotation = useCallback(debounce((bool) => setTokenRotation(bool), 1000, {
    leading: true,
    trailing: false
  }), [])

  const debouncedInvertOther = useCallback(debounce((bool) => setOther(bool), 1000, {
    leading: true,
    trailing: false
  }), [])
  const debouncedRecursive = useCallback(debounce((bool) => setRecursive(bool), 1000, {
    leading: true,
    trailing: false
  }), [])

  useEffect(() => {
    const allowEvents = getTagsForEvents(tokenHistory, tokenChildren, tokenRotation)
    const ignoreEvents = getTagsForEventsIgnore(tokenHistory, tokenChildren, tokenRotation)
    setFilterList(allEvents.filter(event => (allowEvents.includes(event.event)) || (!ignoreEvents.includes(event.event) && other)))
  }, [tokenHistory, tokenChildren, tokenRotation, other])

  useEffect(() => {
    async function populating () {
      const tempAllEvents: TOKEN_EVENT[] = []
      // get all IP information and add it to event
      const masterToken = resolveMasterToken(subToken.parent, masterTokenList)
      if (masterToken === undefined) return
      for (const event of await getTokenHistory(
        masterToken.masterToken,
        resolveDomainURL(masterToken.domainUrl, domainList),
        subToken.momID,
        recursive
      )) {
        if (event.ip) {
          const tempLocation = await getIPLocation(event.ip, masterToken.masterToken, resolveDomainURL(masterToken.domainUrl, domainList))
          tempAllEvents.push({ ...event, info: tempLocation })
          continue
        }
        tempAllEvents.push(event)
      }
      setAllEvents(tempAllEvents)
      const allowEvents = getTagsForEvents(tokenHistory, tokenChildren, tokenRotation)
      const ignoreEvents = getTagsForEventsIgnore(tokenHistory, tokenChildren, tokenRotation)
      setFilterList(tempAllEvents.filter(event => (allowEvents.includes(event.event)) || (!ignoreEvents.includes(event.event) && other)))
      setParentToken(masterToken)
      setDetailsLoaded(true)
    }
    populating().catch(e => {
      alertTextOnly('Something went wrong fetching token details')
      nav.navigate('Home')
    })
  }, [reloadCounter, recursive])

  const getRevocationIDAndRevoke = async (subToken: SubToken, recursive: boolean, identitiesList: MasterToken[]) => {
    const masterTokenInQuestion = resolveMasterToken(subToken.parent, identitiesList)
    if (masterTokenInQuestion === undefined) return
    await revokeToken(masterTokenInQuestion.masterToken, recursive, resolveDomainURL(masterTokenInQuestion.domainUrl, domainList), subToken.momID)
    await updateSubTokenList(masterTokenList, setSubTokenList, domainList, setConnectionErrors)
  }

  const deleteButton = () => {
    Alert.alert('Warning', 'Do you want to delete your ' + subToken.name + ' Subtoken?', [
      {
        text: 'Cancel',
        onPress: () => {
        },
        style: 'cancel'
      },
      {
        text: 'Yes',
        onPress: () => {
          Alert.alert('Notice', 'Do you want to revoke and delete all children of this token', [
            {
              text: 'Cancel',
              onPress: () => {
              },
              style: 'cancel'
            },
            {
              text: 'Yes',
              onPress: () => {
                setDetailsLoaded(false)
                getRevocationIDAndRevoke(subToken, true, masterTokenList).then(() => {
                  nav.goBack()
                })
              }
            },
            {
              text: 'No',
              onPress: () => {
                setDetailsLoaded(false)
                getRevocationIDAndRevoke(subToken, false, masterTokenList).then(() => {
                  nav.goBack()
                })
              }
            }])
        }
      }])
  }
  if (!detailsLoaded) {
    return <View style={{ height: '100%', width: '100%' }}>
            <HomeHeader/>
            <ActivityIndicator size="small" color={theme.mytokenColor} style={{ alignSelf: 'center', justifyContent: 'center', flex: 0.85 }} />
        </View>
  }

  return (
        <View style={{ height: '100%', width: '100%' }}>
                <HomeHeader
                    iconLeft={
                        <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                            <Pressable hitSlop={20} onPress={() => {
                              nav.goBack()
                            }}>
                                <Octicons name="chevron-left" size={37} color={theme.backgroundAccentGrey}/>
                            </Pressable>
                        </View>
                    }
                    contentCenter={
                        <View style={{ flexDirection: 'column', justifyContent: 'center', flex: 3 }}>
                            <Text style={styles.headerText}>
                                {subToken.name}
                            </Text>
                            <Text
                                style={[styles.headerText, { fontSize: 14, paddingTop: height < 1500 ? 0 : 5, fontFamily: 'Inter_400Regular' }]}>
                                { (height < 1500
                                  ? ''
                                  : 'Parent: ' + (parentToken && (parentToken.name || '')) + '\n') +
                                  (subToken.expiration
                                    ? 'Expires: ' + Moment(subToken.expiration).format('D.MMM.YY HH:mm')
                                    : '\n Created: ' + Moment(subToken.creation).format('D.MMM.YY HH:mm'))}
                            </Text>
                        </View>
                    }
                    iconRight={
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                                <Pressable hitSlop={HITSLOP_SQUARE} onPress={deleteButton}>
                                    <Octicons name="trash" size={28} color={theme.backgroundAccentGrey}/>
                                </Pressable>
                            </View>
                        </View>
                    }/>
                <View style={{ height: '100%', flex: 1 }}>
                    <View style={{ height: hide ? '100%' : (((1 - width / height) / 0.90) * 100).toString() + '%' } }>
                        <SearchComponentSubToken filterList={filterList} transferSearch={transferSearch} nav={nav} reloadCounter={reloadCounter} setReloadCounter={setReloadCounter}>
                            <CheckBoxFilter text="Token History" value={tokenHistory} setValue={() => debouncedInvertTokenHistory(!tokenHistory)}/>
                            <CheckBoxFilter text="Token Children" value={tokenChildren} setValue={() => debouncedInvertTokenChildren(!tokenChildren)}/>
                            {
                                // <CheckBoxFilter text="Token Rotation" value={tokenRotation} setValue={() => debouncedInvertTokenRotation(!tokenRotation)}/>
                            }
                            <CheckBoxFilter text="Other" value={other} setValue={() => debouncedInvertOther(!other)}/>
                            <CheckBoxFilter text="Recursive" value={recursive} setValue={() => debouncedRecursive(!recursive)}/>
                        </SearchComponentSubToken>
                    </View>
                        {!hide && <View>
                            <DividerWithText text="Daily Token Usage"/>
                            <Heatmap allEvents={filterList} onChangeSearch={setTransferSearch}/>
                        </View>
                        }
                        <View style={{ position: 'absolute', right: 27, bottom: 26, flexDirection: 'row' }}>
                            <Pressable hitSlop={HITSLOP_SQUARE} onPress={toggleHide}>
                                <Octicons name={hide ? 'eye-closed' : 'eye'} size={24} color={theme.backgroundGrey}/>
                            </Pressable>
                        </View>
                </View>
                <StatusBar style={theme === lightTheme ? 'dark' : 'light'} backgroundColor={theme.backgroundSubtle} />
        </View>
  )
}

export default SubTokenDetails
