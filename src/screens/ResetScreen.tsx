import React, { useContext, useState } from 'react'
import { Alert, Dimensions, View } from 'react-native'
import TextFieldWithHeader from '../components/spacing_and_headers/TextFieldWithHeader'
import StandardHeader from '../components/spacing_and_headers/StandardHeader'
import { alertTextOnly } from '../utilities/alertHelper'
import MainButton from '../components/buttons/MainButton'
import { checkPassword } from '../api/localSecureStorage'
import { StatusBar } from 'expo-status-bar'
import { reset } from '../utilities/contextUtilities'
import { resetUnsecureStorage } from '../api/localUnsecureStorage/localManipulation'
import * as Updates from 'expo-updates'
import { MyContext } from '../utilities/appContext'
import { AuthContext } from '../utilities/authContext'
import createStylesField from '../styles/fieldStyles'
import { lightTheme } from '../api/constants/colors'

/*
This screen allows the user to manage their login settings, if a user doesn't enter a
password they can remove the one they set previously
 */

interface Props {
    navigation: any;
}
const height = Dimensions.get('window').height
const ResetScreen = (props: Props) => {
  const { theme } = useContext(AuthContext)
  const inputStyles = createStylesField(theme)
  const [oldPassword, onChangeOldPassword] = useState('')
  const { setMasterTokenList, setSubTokenList, masterTokenList, setFirstSetup, domainList } = useContext(MyContext)

  const resetButtonAction = async () => {
    if (!oldPassword.length) {
      alertTextOnly('Enter the current password to reset the app!')
      return
    }
    if (!await checkPassword(oldPassword)) {
      alertTextOnly('Your current password is incorrect')
      return
    }
    Alert.alert('Warning', 'Do you want to reset the app, and revoke all tokens', [
      {
        text: 'Cancel',
        onPress: () => {
          // nothing
        },
        style: 'cancel'
      },
      {
        text: 'Yes',
        onPress: () => {
          reset(masterTokenList, setSubTokenList, setMasterTokenList, setFirstSetup, domainList)
          resetUnsecureStorage().then(() => {
            Updates.reloadAsync().then(() => {})
          })
        }
      }])
  }
  return (
        <View style={{ height: '100%', width: '100%' }}>
            <StandardHeader nav={props.navigation} text="Security">
                <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}/>
            </StandardHeader>
            <TextFieldWithHeader onChangeText={onChangeOldPassword}
                                        value={oldPassword}
                                        placeholder="password"
                                        placeholderTextColor={theme.accentColor}
                                        secureTextEntry={true}
                                        text="Current Password"/>
            <View style={[inputStyles.fixedButtonContainer, { bottom: height / 6 }]}>
                <MainButton text="Reset" onPress={resetButtonAction}/>
            </View>
            <StatusBar style={theme === lightTheme ? 'dark' : 'light'} backgroundColor={theme.main} />
        </View>
  )
}

export default ResetScreen
