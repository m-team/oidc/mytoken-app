import React, { useContext } from 'react'
import StandardHeader from '../components/spacing_and_headers/StandardHeader'
import { StyleSheet, Text, View } from 'react-native'

import { StackActions } from '@react-navigation/native'
import { Octicons } from '@expo/vector-icons'
import SubButton from '../components/buttons/SubButton'
import SecureText from '../components/fields/secure_text/SecureText'
import { StatusBar } from 'expo-status-bar'
import { AuthContext } from '../utilities/authContext'
import createStylesHome from '../styles/homeStyles'

interface TransferTokenProps {
    navigation: any;
    route: any;
}

/*
This is the screen that shows when a transfer token was returned upon creation of a new subtoken
 */

const TransferToken = (props: TransferTokenProps) => {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  const nav = props.navigation
  const params = props.route.params
  return (
        <View style={{ height: '100%', width: '100%' }}>
            <StandardHeader text="Your Transfer Code..."/>
            <View style={localStyles.contentContainer}>
                <Text style={[styles.mediumText, { paddingHorizontal: 60, flex: 0.3 }]}>
                    Transfer <Text>{params.subTokenID}</Text> to another device. Valid for 5 minutes. You
                    can’t generate another one!
                </Text>
                <View style={localStyles.iconContainer}>
                    <Octicons name="key" size={100} color={theme.backgroundSubtle}/>
                </View>
                <View style={localStyles.secureTextContainer}>
                    <SecureText phrase={params.transferCode}/>
                </View>
                <View style={{ alignSelf: 'center', flex: 0.1 }}>
                    <SubButton text="Continue..." onPress={() => {
                      nav.dispatch(StackActions.pop(2))
                    }}/>
                </View>
                <View style={{ flex: 0.2 }}/>
            </View>
            <StatusBar style="dark" backgroundColor={theme.main} />
        </View>
  )
}
const localStyles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    width: '100%'
  },
  iconContainer: {
    alignSelf: 'center',
    flex: 0.3
  },
  secureTextContainer: {
    flex: 0.2,
    alignSelf: 'center'
  }
})

export default TransferToken
