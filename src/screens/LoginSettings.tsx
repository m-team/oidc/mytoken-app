import React, { useContext, useRef, useState } from 'react'
import { Alert, Dimensions, View } from 'react-native'
import TextFieldWithHeader from '../components/spacing_and_headers/TextFieldWithHeader'
import StandardHeader from '../components/spacing_and_headers/StandardHeader'
import { alertTextOnly } from '../utilities/alertHelper'
import MainButton from '../components/buttons/MainButton'
import { checkPassword, setPasswordStorage } from '../api/localSecureStorage'
import { AuthContext } from '../utilities/authContext'
import { StatusBar } from 'expo-status-bar'
import { MyContext } from '../utilities/appContext'
import createStylesField from '../styles/fieldStyles'
import { lightTheme } from '../api/constants/colors'

/*
This screen allows the user to manage their login settings, if a user doesn't enter a
password they can remove the one they set previously
 */

interface Props {
    navigation: any;
}
const height = Dimensions.get('window').height
const LoginSettings = (props: Props) => {
  const { theme } = useContext(AuthContext)
  const inputStyles = createStylesField(theme)
  const nav = props.navigation
  const [newPassword, onChangeNewPassword] = useState('')
  const [oldPassword, onChangeOldPassword] = useState('')
  const [repeatPassword, onChangeRepeatPassword] = useState('')
  const { setLoggedIn, passSet, setPassword } = useContext(AuthContext)
  const { firstSetup } = useContext(MyContext)
  const newPassFirst = useRef()
  const newPassSecond = useRef()

  const mainButtonAsyncAction = async () => {
    setPassword(newPassword)
    setLoggedIn(true)
    if (passSet) {
      nav.goBack()
    }
    // alertTextOnlySuccess('Password set')
  }
  const mainButtonAction = async () => {
    // write all data to new location to avoid "reset"
    if (repeatPassword !== newPassword) {
      alertTextOnly('Your passwords dont match!')
      return
    }
    if (passSet && !await checkPassword(oldPassword)) {
      alertTextOnly('Your old password is incorrect')
      return
    }
    if (!newPassword.length) {
      alertTextOnly('Your must enter a new password for encryption purposes!')
      return
    }
    if (firstSetup) {
      Alert.alert('Warning', 'Do you want to set a new password', [
        {
          text: 'Cancel',
          onPress: () => {},
          style: 'cancel'
        },
        {
          text: 'Yes',
          onPress: changePasswordAction
        }])
    } else {
      changePasswordAction()
    }
  }

  const changePasswordAction = () => {
    setPasswordStorage(newPassword, passSet ? oldPassword : undefined).then(() => {
      mainButtonAsyncAction().then(() => {})
    }).catch(() => {
      alertTextOnly('Something went wrong setting the password')
    })
  }
  return (
        <View style={{ height: '100%', width: '100%' }}>
            <StandardHeader nav={props.navigation} text="Security">
              <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}/>
            </StandardHeader>
            {passSet
              ? (<TextFieldWithHeader
                     onChangeText={onChangeOldPassword}
                     value={oldPassword}
                     placeholder="password"
                     placeholderTextColor={theme.accentColor}
                     secureTextEntry={true}
                     text="Current Password"
                     nextRef={newPassFirst}/>)
              : <View/>}
                <View style={{ paddingTop: 30 }}/>
                <TextFieldWithHeader onChangeText={onChangeNewPassword}
                                     value={newPassword}
                                     placeholder="password"
                                     placeholderTextColor={theme.accentColor}
                                     secureTextEntry={true}
                                     text="New Password"
                                     reference={newPassFirst}
                                     nextRef={newPassSecond}/>
                <TextFieldWithHeader onChangeText={onChangeRepeatPassword}
                                     value={repeatPassword}
                                     placeholder="password"
                                     placeholderTextColor={theme.accentColor}
                                     secureTextEntry={true}
                                     text="Repeat New Password"
                                     reference={newPassSecond}
                                     onSubmitEditing={mainButtonAction}/>
            <View style={[inputStyles.fixedButtonContainer, { bottom: height / 6 }]}>
                <MainButton text="Set-Pass" onPress={mainButtonAction}/>
            </View>

          <StatusBar style={theme === lightTheme ? 'dark' : 'light'} backgroundColor={theme.main} />

        </View>
  )
}

export default LoginSettings
