import React, { useContext } from 'react'
import { ScrollView, Text, View } from 'react-native'
import DividerWithText from '../components/spacing_and_headers/DividerWithText'
import StandardHeader from '../components/spacing_and_headers/StandardHeader'
import { StatusBar } from 'expo-status-bar'
import { AuthContext } from '../utilities/authContext'
import createStylesHome from '../styles/homeStyles'
import { lightTheme } from '../api/constants/colors'

/*
This screen shows important license and privacy information about the app.
 */

interface PrivacyPolicyProps {
    navigation: any;
}

const PrivacyPolicy = (props: PrivacyPolicyProps) => {
  const { theme } = useContext(AuthContext)
  const styles = createStylesHome(theme)
  return (
        <View style={{ height: '100%', width: '100%' }}>
            <StandardHeader nav={props.navigation} text="About"/>
            <DividerWithText text="Mytoken Privacy Policy"/>
            <View style={{ height: '75%' }}>
                <ScrollView style={{ marginHorizontal: 45, marginTop: 20 }}>
                    <View style = {{ paddingHorizontal: 10 }}>
                        <Text style={styles.smallText}>
                            Users of the mytoken service use it to manage and obtain OpenID Connect tokens. Therefore, mytoken
                            receives these tokens and stores them. All tokens are only stored encrypted. Personal information
                            (e.g. emails, names) mytoken receives from the OpenID provider are discarded, and not further
                            processed.
                            {'\n\n'}
                            The data processed (OpenID Connect tokens) is necessary in order for the user to obtain access
                            tokens.
                            {'\n\n'}
                            Usage of the mytoken service generates logs, which are retained. These records contain:
                            The network (IP) address from which you access mytoken
                            The user agent used to connect to the mytoken service
                            Time and date of access
                            Details of actions you perform
                            {'\n\n'}
                            This data is necessary to ensure that the mytoken service is reliable and secure, and are used for
                            assisting in the analysis of reported problems and responding to security incidents. Part of this
                            data is also used, so users can check how their mytoken tokens were used.
                            The legal basis for processing the personal data is legitimate interest, Article 6.1(f), GDPR.
                            {'\n\n'}
                            Disclosure of personal data:
                            The collected personal data is only accessible to the authorised personnel of Karlsruhe Institute of
                            Technology, and then only for reasons outlined above. The processed OpenID Connect tokens are
                            secured in a way that they are not accessible by the personnel. Personal data is not regularly
                            disclosed to third parties.
                            {'\n\n'}
                            How to access, rectify, and delete personal data:
                            For the data retained and processed by mytoken, you may use service manager contacts (provided in
                            help section) to access or rectify information. To rectify the data released by an OpenID provider,
                            contact the providers&apos; operators.
                            {'\n\n'}
                            Data protection code of conduct:
                            Personal data will be handled according to the Code of Conduct for Service Providers, a common
                            standard for the research and higher education sector to protect the users&apos; privacy.
                            How long your personal data will be retained
                            Tokens and data related to them are stored until the tokens are revoked or you delete your account.
                            Network logs are deleted, at the latest, 12 months after the users&apos; last use of the service.
                        </Text>
                    </View>
                </ScrollView>
            </View>
            <StatusBar style={theme === lightTheme ? 'dark' : 'light'} backgroundColor={theme.main} />
        </View>
  )
}

export default PrivacyPolicy
