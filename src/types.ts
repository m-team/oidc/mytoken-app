import { colorPS } from './api/constants/colors'

export enum templateType {
    PROFILE = 'profiles',
    RESTRICTIONS = 'restrictions',
    ROTATION = 'rotation',
    CAPTABILITIES = 'capabilities'
}
export interface OpenIDProvider {
    name: string,
    url: string
}
export interface DOMAIN_URLS {
    visible: boolean,
    domainUrl: string,
    creationPath: string,
    tokenInfoPath: string,
    tokenRevokePath: string,
    templatesPath: string
}
export interface MasterToken {
    name: string,
    creation: Date,
    provider: OpenIDProvider,
    momID: string,
    masterToken: string,
    domainUrl: string,
    expirationDate: Date | undefined,
    color: colorPS
}

export interface TransferCode {
    transferCode: string,
    momID: string,
    expiration: number
}

export interface SubToken {
    momID: string,
    creation: Date,
    parent: string, // masterToken token not string name
    children?: SubToken[],
    name?: string,
    expiration?: Date,
    external?: boolean
}
export interface TOKEN_CAPABILITIES {
    obtain_tokens: boolean,
    token_info: boolean,
    create_new_token: boolean,
    read_write_settings: boolean,
    manage_mytokens: boolean
}
export interface TEMPLATE {
    name: string,
    templateType: templateType,
    capabilities?: TOKEN_CAPABILITIES,
    restrictions?: number // could add type later if more can be adjusted from app
    rotation?: any // not used currently, template is just provided
}

export interface API_REQUEST_SUBTOKEN {
    name: string,
    grant_type: string,
    mytoken: string,
    capabilities: string[] | string,
    response_type?: string,
    rotation?: any,
    restrictions?:any,
    include?: string,
}

export interface API_REQUEST_REVOCATION {
    token: string,
    mom_id?: string,
    recursive: boolean,
}

export interface API_REQUEST_MASTER_TOKEN {
    name: string,
    oidc_issuer: string,
    grant_type: string,
    oidc_flow: string,
    client_type: string,
    restrictions: any,
    capabilities: string[] | string,
    application_name: string,
    response_type: string
}

export interface PollingBody {
    'grant_type': string,
    'polling_code': string
}

export interface DayUse {
    date: Date,
    frequency: number,
    created: boolean
}

export interface TOKEN_INFO_REQUEST {
    action: string,
    mytoken: string
    mom_ids?: string[],
    mom_id?: string
}

export interface TOKEN_EVENT_NAME {
    TAG: string,
    display: string
}
export interface TOKEN_EVENT_TAGS {
    token_history: string,
    token_rotated: string,
    token_children: string
}
export interface LOCATION_INFO {
    longitude: number | undefined,
    latitude: number | undefined,
    city: string | undefined
}

export interface IP_LOCATION {
    info: LOCATION_INFO,
    ip: string,
    used: Date
}

export interface TOKEN_EVENT {
    event: string,
    time: Date,
    ip?: string,
    info?: LOCATION_INFO
}
export interface CONNECTION_ERROR {
    masterToken: string,
    domainURL: string
}
export interface themeType {
    smallHeaderColor: string,
    modalColor: string,
    standardHeaderColor: string,
    heatmapColor: string,
    lockedBoxColor: string,
    borderTextEntry: string,
    buttonText: string,
    textEntryContent: string,
    advancedView: string,
    pressedOptionColor: string,
    dividerColor: string,
    searchDarkBorder: string,
    subTitleColor: string,
    mainBackground: string,
    headerColor: string,
    mytokenColor: string,
    backgroundGrey: string,
    backgroundAccentGrey: string,
    backgroundSubtle: string,
    neutral: string,
    searchBack: string,
    lightback: string,
    dateColor: string,
    mainOpposite: string,
    main: string,
    borderCalm: string,
    accentColor: string,
    accentStrong: string
}
