import { createStackNavigator } from '@react-navigation/stack'
import { DarkTheme, DefaultTheme, NavigationContainer } from '@react-navigation/native'
// eslint-disable-next-line camelcase
import { Inter_400Regular, Inter_500Medium, Inter_600SemiBold, useFonts } from '@expo-google-fonts/inter'
import React, { useCallback, useEffect, useState } from 'react'
import { ErrorBoundary } from 'react-error-boundary'
import ErrorScreen from './components/fields/ErrorScreen'
import Login from './screens/Login'
import { AuthContext } from './utilities/authContext'
import AppAuthenticated from './AppAuthenticated'
import Welcome from './screens/Welcome'
import * as SplashScreen from 'expo-splash-screen'
import LoginSettings from './screens/LoginSettings'
import { passwordIsSet } from './api/localSecureStorage'
import { themeType } from './types'
import { darkTheme, lightTheme } from './api/constants/colors'
import { themeCheck } from './api/localUnsecureStorage/localConfigurations'
/*
This is the appEntry point defined in the expo appEntry.js file. It checks if the user has the login flag set, and if
so it asks for the password. If the password is correct, the navigator show the AppAuthenticated stack. Otherwise
the Stack from this file is shown. A context let's AppAuthenticator know when the use got the correct password.
 */
const Stack = createStackNavigator()
SplashScreen.preventAutoHideAsync().catch(console.error)

const AppAuthenticator = () => {
  const [loggedIn, setLoggedIn] = useState<boolean>(false)
  const [passSet, setPassSet] = useState<boolean>(true)
  const [loading, setLoading] = useState<boolean>(false)
  const [password, setPass] = useState<string>('')
  const [theme, setTheme] = useState<themeType>(lightTheme)
  const setPasswordAndSet = async (password: string) => {
    setPass(password)
    setPassSet(true)
  }
  function ErrorFallback ({ error, resetErrorBoundary }: { error: any, resetErrorBoundary: any }) {
    return (
            <ErrorScreen message={error.message}/>
    )
  }
  const [fontsLoaded] = useFonts({
    // eslint-disable-next-line camelcase
    Inter_400Regular, Inter_600SemiBold, Inter_500Medium
  })
  const lightThemeBg = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      background: lightTheme.mainBackground
    }
  }

  const darkThemeBg = {
    ...DarkTheme,
    colors: {
      ...DarkTheme.colors,
      background: darkTheme.mainBackground
    }
  }

  useEffect(() => {
    async function prepare () {
      try {
        await setPassSet(await passwordIsSet())
        await setTheme((await themeCheck()) === 'dark' ? darkTheme : lightTheme)
      } catch (e) {
        console.warn(e)
      } finally {
        setLoading(false)
      }
    }
    prepare().then(() => {
    })
  }, [])

  const onReadyRootNav = useCallback(async () => {
    if (!loading) {
      await SplashScreen.hideAsync()
    }
  }, [loading])

  if (loading || !fontsLoaded) {
    return null
  }
  const isDarkTheme = theme === darkTheme
  const currentTheme = isDarkTheme ? darkThemeBg : lightThemeBg

  return (
        <ErrorBoundary
            FallbackComponent={ErrorFallback}
            onReset={() => {
            }}
        >
            <AuthContext.Provider
                value={{
                  loggedIn,
                  setLoggedIn,
                  password,
                  setPassword: setPasswordAndSet,
                  passSet,
                  setPassSet,
                  theme,
                  setTheme
                }}>
                <NavigationContainer theme={currentTheme} onReady={onReadyRootNav}>
                    {!loggedIn
                      ? (<Stack.Navigator screenOptions={{ headerShown: false }}
                                       initialRouteName={passSet ? 'Login' : 'Welcome'}>
                              <Stack.Screen name='Login' component={Login}/>
                              <Stack.Screen name='Welcome' component={Welcome}/>
                              <Stack.Screen name='LoginSettings' component={LoginSettings}/>
                          </Stack.Navigator>)

                      : (
                              <AppAuthenticated/>
                        )}
                </NavigationContainer>
            </AuthContext.Provider>
        </ErrorBoundary>
  )
}

export default AppAuthenticator
