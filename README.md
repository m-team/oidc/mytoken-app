# Table of Contents

1. [MyToken App](#mytoken app)
2. [Features](#features)
3. [Screenshots](#screenshots)
4. [Project Setup-Guide](#project-setup-guide)
5. [Usage as developer](#usage-as-developer)
6. [Distribute](#distribute)
7. [Running Tests](#running-tests)
8. [Environment Variables](#environment-variables)
9. [Authors](#authors)

# Mytoken App

Mytoken is a web service to obtain OpenID Connect Access Tokens
in an easy but secure way for extended periods of time and across multiple devices.
[Read more](https://mytoken-docs.data.kit.edu)

This app was created to manage-, create-, delete- and view details of- mytokens. The app,
through a simple, minimalistic design, guides the user through the process of creating
and transferring their mytokens.

Within the app, users must authenticate providers. These are confirmed externally through the online OpenID provider. To do this, the app automatically redirects 
the user to their smartphones' browser when authentication is required. When created, the authentication token is encrypted
and stored in the persistent storage of the device.

All tokens created with authenticated providers are shown within the app. Also, 
using an authenticated provider, users can create new tokens. Creating new tokens does not require an external authentication step. 
After creation, the user is presented a transfer code that's valid for 5 minutes. Only the authentication for the provider requires storing a token on the device.
The powerful provider token is used to fetch details about a tokens usage. These details are visualized using text, charts and maps.
## Features

- Works with Android and IOS
- Authenticate Providers
- View all tokens made for a provider
- Create customized new tokens
- Loading indicator for token expiration date
- Customize colors for each provider
- View token usage history
- App secured using AES encryption, SHA3 hashing and a user-set password
- filter token history with toggles
- search token and token usage history
- View token usage (ip) on a map (GeoIP Maxmind Server)
- Use predefined templates to configure tokens during creation
- Choose any mytoken domain

## Screenshots
<img src="./assets/screenshots/IMG_3015.PNG" width="200" />
<img src="./assets/screenshots/IMG_3017.PNG" width="200" />
<img src="./assets/screenshots/IMG_3007.PNG" width="200" />
<img src="./assets/screenshots/IMG_1226.PNG" width="200" />
<img src="./assets/screenshots/IMG_1228.PNG" width="200" />
<img src="./assets/screenshots/IMG_3018.PNG" width="200" />

## Project Setup-Guide
Download and install **node** and **npm** through their website [NodeJS](https://nodejs.org/en/download/)
or other means

Clone this project into a directory of choice and run the command:
```bash
  npm install
```
from the project root

## Usage as Developer
To run this project locally (dev mode expo), run the command:
```bash
  npm start
```
This will display a QR code. If you want to use the app from 
your own smartphone, download the Expo-Go App from the AppStore/GooglePlay store and scan the 
QR code (on android from within the app, on IOS from camera app). Alternatively, simulators can be installed 
on your machine. See [expo docs](https://docs.expo.dev/) for more information

## Distribute
Follow [this tutorial](https://docs.expo.dev/build/setup/) to build the mytoken app for distribution

## Running Tests
To run all tests, run the following command
```bash
  npm run test
```
This creates a coverage directory with an icov-report that's viewable from a browser.

## Environment Variables

To distribute this project to any app store, you need to add the following environment
variables to your .env file.
`GOOGLE_MAPS_API_KEY`

## Authors

- Morris Baumgarten-Egemole
