import * as AsyncStorage from '@react-native-async-storage/async-storage'
import * as axiosUtility from '../../src/api/axiosRequest'
import { DOMAIN_URL_KIT } from '../../src/api/constants/domain_url'
import { DEFAULT_MASTER_TOKEN, KIT_DOMAIN_URLS } from '../../src/utilities/main_types_defaults'
import { deleteDomainAddress, resetDomain } from '../../src/api/localUnsecureStorage/localDomainAdresses'
import { resetData } from '../../src/api/localUnsecureStorage/localManipulation'
import * as hashing from '../../src/utilities/cryptoHashHelper'
import {
  firstSetupCheck,
  setFirstSetupLocalStorage
} from '../../src/api/localUnsecureStorage/localConfigurations'
import {
  localFetchMasterTokenList
} from '../../src/api/localUnsecureStorage/localMasterSubTokenManager'
import * as SecureStore from 'expo-secure-store'

// keys from localstorage
const IP_LOCATION_KEY = 'ip_location_cache'
jest.mock('crypto-es', () => {
  return {
    CryptoES: () => {}
  }
})

/*

Tests the caching of geolocation in a level1 and caching of domain urls. The cache is generated on each launch in memory, a level 2 cache which is
the AsyncStorage and the geolocation API (and .wellknown endpoint) which is used to fetch values that don't exist.

Time constraints meant there is duplicate code in this test-suite, modules did not reset correctly with jest.resetModules()
meaning they had to be 'required' each time.

 */
const uniqueEventListFull = [...Array(200)].map((value, inner) => (
  {
    event: inner.toString(),
    used: new Date(2100, 0, inner),
    ip: inner.toString(),
    info: {
      longitude: inner.toString(),
      latitude: inner.toString(),
      city: inner.toString()
    }
  }
))
const uniqueEventList = [...Array(190)].map((value, inner) => (
  {
    event: inner.toString(),
    used: new Date(2100, 0, inner),
    ip: inner.toString(),
    info: {
      longitude: inner.toString(),
      latitude: inner.toString(),
      city: inner.toString()
    }
  }
))
const uniqueEventRemovedOldest10 = [...Array(190)].map((value, inner) => {
  return {
    event: (inner + 10).toString(),
    used: new Date(2100, 0, (inner + 10)),
    ip: (inner + 10).toString(),
    info: {
      longitude: (inner + 10).toString(),
      latitude: (inner + 10).toString(),
      city: (inner + 10).toString()
    }
  }
})

const domainURLList = [KIT_DOMAIN_URLS]

describe('Unsecure Cache storage tests', () => {
  let eventListFull = uniqueEventListFull.slice()
  let eventRemovedOldest10 = uniqueEventRemovedOldest10.slice()
  const eventList = uniqueEventList.slice()
  beforeEach(() => {
    eventListFull = uniqueEventListFull.slice()
    eventRemovedOldest10 = eventRemovedOldest10.slice()
  })
  afterEach(() => {
    jest.restoreAllMocks()
  })
  test('ResetScreen Unsecure Storage', async () => {
    const spyClear = jest.spyOn(AsyncStorage, 'clear')
    const localUnsecureStorage = require('../../src/api/localUnsecureStorage/localManipulation')
    spyClear.mockReturnValue(Promise.resolve())
    await localUnsecureStorage.resetUnsecureStorage()
    expect(spyClear).toHaveBeenCalledTimes(1)
  })

  test('getIPLocation when its in level2Cache (first spot)', async () => {
    const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
    const localUnsecureStorage = require('../../src/api/localUnsecureStorage/localGeoIPCache')
    spyGetItem.mockReturnValue(Promise.resolve(JSON.stringify(eventListFull)))
    const info = await localUnsecureStorage.getIPLocation('0')
    const expectedInfo = {
      longitude: '0',
      latitude: '0',
      city: '0'
    }
    expect(spyGetItem).toHaveBeenCalledWith(IP_LOCATION_KEY)
    expect(info).toEqual(expectedInfo)
  })

  test('getIPLocation when its in level2Cache (last spot)', async () => {
    const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
    const localUnsecureStorage = require('../../src/api/localUnsecureStorage/localGeoIPCache')
    spyGetItem.mockReturnValue(Promise.resolve(JSON.stringify(eventListFull)))
    const info = await localUnsecureStorage.getIPLocation('199')
    const expectedInfo = {
      longitude: '199',
      latitude: '199',
      city: '199'
    }
    expect(spyGetItem).toHaveBeenCalledWith(IP_LOCATION_KEY)
    expect(info).toEqual(expectedInfo)
  })
  test('didnt cache in l1', async () => {
    const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
    const localUnsecureStorage = require('../../src/api/localUnsecureStorage/localGeoIPCache')
    spyGetItem.mockReturnValue(Promise.resolve(JSON.stringify(eventListFull)))
    const info = await localUnsecureStorage.getIPLocation('199')
    const expectedInfo = {
      longitude: '199',
      latitude: '199',
      city: '199'
    }
    expect(spyGetItem).toHaveBeenCalledWith(IP_LOCATION_KEY)
    expect(info).toEqual(expectedInfo)
  })
  test('getIPLocation when its in level2Cache (middle spot)', async () => {
    const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
    const localUnsecureStorage = require('../../src/api/localUnsecureStorage/localGeoIPCache')
    spyGetItem.mockReturnValue(Promise.resolve(JSON.stringify(eventListFull)))
    const info = await localUnsecureStorage.getIPLocation('123')
    const expectedInfo = {
      longitude: '123',
      latitude: '123',
      city: '123'
    }
    expect(spyGetItem).toHaveBeenCalledWith(IP_LOCATION_KEY)
    expect(info).toEqual(expectedInfo)
  })

  test('getIPLocation when its NOT in level2Cache (api - FOUND CITY)', async () => {
    const axiosGetLocation = jest.spyOn(axiosUtility, 'getIPLocationInfo')
    const localUnsecureStorage = require('../../src/api/localUnsecureStorage/localGeoIPCache')
    const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
    spyGetItem.mockReturnValue(Promise.resolve(eventList))
    const expectedInfo = {
      longitude: '300',
      latitude: '300',
      city: '300'
    }
    axiosGetLocation.mockReturnValue(Promise.resolve(expectedInfo))
    const info = await localUnsecureStorage.getIPLocation('300')
    expect(info).toEqual(expectedInfo)
  })

  test('getIPLocation when its NOT in level2Cache (api - NO ERROR BUT IP HAS NO CITY IN DB)', async () => {
    const axiosGetLocation = jest.spyOn(axiosUtility, 'getIPLocationInfo')
    const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
    spyGetItem.mockReturnValue(Promise.resolve(eventList))
    const localUnsecureStorage = require('../../src/api/localUnsecureStorage/localGeoIPCache')
    const expectedInfo = {
      longitude: undefined,
      latitude: undefined,
      city: undefined
    }
    axiosGetLocation.mockReturnValue(Promise.resolve(expectedInfo))
    const info = await localUnsecureStorage.getIPLocation('305')
    expect(info).toEqual(expectedInfo)
  })
  test('getIPLocation when its NOT in level2Cache (api - error, no response)', async () => {
    const axiosGetLocation = jest.spyOn(axiosUtility, 'getIPLocationInfo')
    const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
    spyGetItem.mockReturnValue(Promise.resolve(eventList))
    const localUnsecureStorage = require('../../src/api/localUnsecureStorage/localGeoIPCache')
    const expectedInfo = undefined
    axiosGetLocation.mockReturnValue(Promise.resolve(expectedInfo))
    const info = await localUnsecureStorage.getIPLocation('302')
    expect(info).toEqual(expectedInfo)
  })

  test('testingClearCacheElements removed oldest 10 items', async () => {
    const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
    const axiosGetLocation = jest.spyOn(axiosUtility, 'getIPLocationInfo')
    const localUnsecureStorage = require('../../src/api/localUnsecureStorage/localGeoIPCache')
    const expectedInfo = {
      longitude: '300',
      latitude: '300',
      city: '300'
    }
    axiosGetLocation.mockReturnValue(Promise.resolve(expectedInfo))
    spyGetItem.mockReturnValue(Promise.resolve(JSON.stringify(eventListFull)))
    const newList = await localUnsecureStorage.testingClearCacheElements(10, eventListFull)
    expect(newList).toEqual(eventRemovedOldest10)
  })
  test('testing remove week old item', async () => {
    const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
    const axiosGetLocation = jest.spyOn(axiosUtility, 'getIPLocationInfo')
    const localUnsecureStorage = require('../../src/api/localUnsecureStorage/localGeoIPCache')
    const expectedInfo = {
      longitude: 'somewhere',
      latitude: 'over',
      city: 'the rainbow'
    }
    axiosGetLocation.mockReturnValue(Promise.resolve(expectedInfo))
    spyGetItem.mockReturnValue(Promise.resolve(JSON.stringify([...eventListFull, {
      event: 'something',
      used: new Date(2000, 0, 0),
      ip: '12292929292',
      info: {
        longitude: 'somewhere',
        latitude: 'over',
        city: 'the rainbow'
      }
    }])))
    const info = await localUnsecureStorage.getIPLocation('12292929292')
    expect(info).toEqual(expectedInfo)
  })
  test('testingClearCacheElements removed oldest too many items', async () => {
    const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
    const axiosGetLocation = jest.spyOn(axiosUtility, 'getIPLocationInfo')
    const localUnsecureStorage = require('../../src/api/localUnsecureStorage/localGeoIPCache')
    const expectedInfo = {
      longitude: '300',
      latitude: '300',
      city: '300'
    }
    axiosGetLocation.mockReturnValue(Promise.resolve(expectedInfo))
    spyGetItem.mockReturnValue(Promise.resolve(JSON.stringify(eventListFull)))
    const newList = await localUnsecureStorage.testingClearCacheElements(210, eventListFull)
    expect(newList).toEqual([])
  })

  test('create event list', async () => {
    const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
    const spyStoreItem = jest.spyOn(AsyncStorage, 'setItem')
    const spyClear = jest.spyOn(AsyncStorage, 'clear')
    const axiosGetLocation = jest.spyOn(axiosUtility, 'getIPLocationInfo')
    const localUnsecureStorage = require('../../src/api/localUnsecureStorage/localGeoIPCache')
    spyStoreItem.mockReturnValue(Promise.resolve())
    spyGetItem.mockReturnValue(Promise.resolve(eventListFull))
    spyClear.mockReturnValue(Promise.resolve())
    axiosGetLocation.mockReturnValue(Promise.resolve())
    await localUnsecureStorage.getIPLocation('0')
    expect(spyStoreItem).toHaveBeenCalledWith(IP_LOCATION_KEY, JSON.stringify([]))
  })
})

describe('Domain url Cache storage tests', () => {
  let domainURLs = domainURLList.slice()
  beforeEach(() => {
    domainURLs = domainURLList.slice()
  })
  afterEach(() => {
    jest.restoreAllMocks()
  })
  test('get domain from external source', async () => {
    const axiosDomain = jest.spyOn(axiosUtility, 'getDomainAddressesExternal')
    const checkDomain = jest.spyOn(axiosUtility, 'checkDomain')
    const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
    const localUnsecureStorage = require('../../src/api/localUnsecureStorage/localDomainAdresses')
    const spyStoreItem = jest.spyOn(AsyncStorage, 'setItem')

    spyGetItem.mockReturnValue(Promise.resolve(JSON.stringify([KIT_DOMAIN_URLS])))
    axiosDomain.mockReturnValue(Promise.resolve(KIT_DOMAIN_URLS))
    checkDomain.mockReturnValue(Promise.resolve(true))
    spyStoreItem.mockReturnValue(Promise.resolve())

    const domain = await localUnsecureStorage.getDomainAddresses(DOMAIN_URL_KIT + 'TEST', [KIT_DOMAIN_URLS], (item) => {})
    expect(checkDomain).toHaveBeenCalledWith(DOMAIN_URL_KIT + 'TEST/')
    expect(axiosDomain).toHaveBeenCalledWith(DOMAIN_URL_KIT + 'TEST/')
    expect(domain).toEqual(KIT_DOMAIN_URLS)
  })
})

const spyGetItemAsync = jest.spyOn(AsyncStorage, 'getItem')
const spySetItemAsync = jest.spyOn(AsyncStorage, 'setItem')
const spyDeleteItemAsync = jest.spyOn(AsyncStorage, 'removeItem')
// keys from localstorage
const identitiesKey = 'identities'
const subTokensKey = 'subTokens'

const setupKey = 'setupKey'
const signature = '.mytokenapp.'
const passwordForTests = 'testpass'
const DOMAIN_USED_KEY = 'url_domain_used'
/*
test('Get empty data cells subtokenlist', async () => {
  spyGetItemAsync.mockReturnValue(Promise.resolve(JSON.stringify([])))
  const data = await localFetchSubTokenList(passwordForTests)
  expect(spyGetItemAsync).toHaveBeenCalledWith(subTokensKey)
  expect(data).toEqual([])
})

test('Get empty data cells identitieslist', async () => {
  spyGetItemAsync.mockReturnValue(Promise.resolve(JSON.stringify([])))
  const data = await localFetchIdentitiesList(passwordForTests)
  expect(spyGetItemAsync).toHaveBeenCalledWith(identitiesKey)
  expect(data).toEqual([])
})
*/

test('Get non existing data cells identitieslist', async () => {
  spyGetItemAsync.mockReturnValue(Promise.resolve(null))
  const data = await localFetchMasterTokenList(passwordForTests)
  expect(spyGetItemAsync).toHaveBeenCalledWith(identitiesKey)
  expect(data).toEqual(null)
})

test('firstSetupCheck returns true when setup not done', async () => {
  spyGetItemAsync.mockReturnValue(Promise.resolve(null))
  const data = await firstSetupCheck()
  expect(spyGetItemAsync).toHaveBeenCalledWith(signature + setupKey)
  expect(data).toEqual(true)
})
test('firstSetupCheck returns false when setup done', async () => {
  spyGetItemAsync.mockReturnValue(Promise.resolve(true))
  const data = await firstSetupCheck()
  expect(spyGetItemAsync).toHaveBeenCalledWith(signature + setupKey)
  expect(data).toEqual(false)
})

test('first setup value is deleted if false given', async () => {
  spyDeleteItemAsync.mockReturnValue(Promise.resolve())
  await setFirstSetupLocalStorage(false)
  expect(spyDeleteItemAsync).toHaveBeenCalledWith(signature + setupKey)
})

test('first setup value is set if true given', async () => {
  spySetItemAsync.mockReturnValue(Promise.resolve())
  await setFirstSetupLocalStorage(true)
  expect(spySetItemAsync).toHaveBeenCalledWith(signature + setupKey, JSON.stringify(true))
})

test('data is reset', async () => {
  spyDeleteItemAsync.mockReturnValue(Promise.resolve())
  spySetItemAsync.mockReturnValue(Promise.resolve())
  const passWordHashMock = jest.spyOn(hashing, 'getPasswordHash')
  passWordHashMock.mockReturnValue(Promise.resolve('test'))

  const spyGetItemSecure = jest.spyOn(SecureStore, 'getItemAsync')
  spyGetItemSecure.mockReturnValue(Promise.resolve('non null'))
  await resetData(passwordForTests)
  expect(spyDeleteItemAsync).toHaveBeenCalledTimes(2)
})

test('domain is deleted', async () => {
  spyDeleteItemAsync.mockReturnValue(Promise.resolve())
  await resetDomain((item) => {})
  expect(spyDeleteItemAsync).toHaveBeenCalledWith('.mytokenapp.setupKey')
})
test('domain is set invisible if exists', async () => {
  let domainListLocal = [KIT_DOMAIN_URLS]
  const updateDomainListLocal = (item) => {
    domainListLocal = item
  }
  deleteDomainAddress(KIT_DOMAIN_URLS, domainListLocal, updateDomainListLocal, [DEFAULT_MASTER_TOKEN])

  spyDeleteItemAsync.mockReturnValue(Promise.resolve())
  expect(domainListLocal).toEqual([{ ...KIT_DOMAIN_URLS, visible: false }])
})
test('domain is deleted if not exists', async () => {
  let domainListLocal = [KIT_DOMAIN_URLS]
  const updateDomainListLocal = (item) => {
    domainListLocal = item
  }
  deleteDomainAddress(KIT_DOMAIN_URLS, domainListLocal, updateDomainListLocal, [])
  spyDeleteItemAsync.mockReturnValue(Promise.resolve())
  expect(domainListLocal).toEqual([])
})
test('all domain is deleted if not in use anymore', async () => {
  let domainListLocal = [KIT_DOMAIN_URLS, { ...KIT_DOMAIN_URLS, domainUrl: 'teststinsg' }
  ]
  const updateDomainListLocal = (item) => {
    domainListLocal = item
  }
  deleteDomainAddress(KIT_DOMAIN_URLS, domainListLocal, updateDomainListLocal, [])
  spyDeleteItemAsync.mockReturnValue(Promise.resolve())
  expect(domainListLocal).toEqual([])
})
test('all domain is deleted if not in use anymore but in use is kept', async () => {
  let domainListLocal = [KIT_DOMAIN_URLS, { ...KIT_DOMAIN_URLS, domainUrl: 'teststinsg' }, { ...KIT_DOMAIN_URLS, domainUrl: 'dontdelete' }
  ]
  const updateDomainListLocal = (item) => {
    domainListLocal = item
  }
  deleteDomainAddress(KIT_DOMAIN_URLS, domainListLocal, updateDomainListLocal, [{ ...DEFAULT_MASTER_TOKEN, domainUrl: 'dontdelete' }])
  spyDeleteItemAsync.mockReturnValue(Promise.resolve())
  expect(domainListLocal).toEqual([{ ...KIT_DOMAIN_URLS, domainUrl: 'dontdelete' }])
})
