import * as SecureStore from 'expo-secure-store'
import * as AsyncStorage from '@react-native-async-storage/async-storage'
import {
  checkPassword,
  deletePassword,
  setPasswordStorage,
  storePassword
} from '../../src/api/localSecureStorage'
import * as hashing from '../../src/utilities/cryptoHashHelper'
import * as localManipulation from '../../src/api/localUnsecureStorage/localManipulation'

jest.mock('crypto-es', () => {
  return {
    CryptoES: () => ({})
  }
})

const spyGetItemAsync = jest.spyOn(SecureStore, 'getItemAsync')
const spySetItemAsync = jest.spyOn(SecureStore, 'setItemAsync')
const spyDeleteItemAsync = jest.spyOn(SecureStore, 'deleteItemAsync')

// keys from localstorage

const signature = '.mytokenapp.'
const passwordStore = 'mytokenpassword'
const passwordForTests = 'password'
beforeEach(() => {
  jest.useFakeTimers()
})
afterEach(() => {
  jest.clearAllMocks()
})

test('password check return false on wrong password', async () => {
  const saltGetter = jest.spyOn(hashing, 'createSalt')
  saltGetter.mockReturnValue('salt')
  const encryptItem = jest.spyOn(hashing, 'encryptItem')
  encryptItem.mockReturnValue('test')
  spyGetItemAsync.mockReturnValue(Promise.resolve(JSON.stringify('random string input by attacker')))
  const data = await checkPassword('wrong pass')
  expect(spyGetItemAsync).toHaveBeenCalledWith(signature + passwordStore)
  expect(data).toEqual(false)
})
test('password check return false on wrong password with empty resolve', async () => {
  const saltGetter = jest.spyOn(hashing, 'createSalt')
  saltGetter.mockReturnValue('salt')
  const encryptItem = jest.spyOn(hashing, 'encryptItem')
  encryptItem.mockReturnValue('test')
  spyGetItemAsync.mockReturnValue(Promise.resolve(null))
  const data = await checkPassword('wrong pass')
  expect(spyGetItemAsync).toHaveBeenCalledWith(signature + passwordStore)
  expect(data).toEqual(false)
})

test('password check returns true on right password', async () => {
  const saltGetter = jest.spyOn(hashing, 'createSalt')
  saltGetter.mockReturnValue('salt')
  const encryptItem = jest.spyOn(hashing, 'encryptItem')
  encryptItem.mockReturnValue('test')
  spyGetItemAsync.mockReturnValue(Promise.resolve(JSON.stringify(passwordForTests)))
  await checkPassword(passwordForTests)
  expect(spyGetItemAsync).toHaveBeenCalledWith(signature + passwordStore)
  // expect(data).toEqual(true) can't compare due to hashing (already tested by creator tho)
})

test('password is stored', async () => {
  const saltGetter = jest.spyOn(hashing, 'createSalt')
  saltGetter.mockReturnValue('salt')
  const hashGetter = jest.spyOn(hashing, 'getPasswordHash')
  hashGetter.mockReturnValue(Promise.resolve('fakeHashLongString='))
  spySetItemAsync.mockReturnValue(Promise.resolve(passwordForTests))
  await storePassword(passwordForTests)
  // expect(spySetItemAsync).toHaveBeenCalled() cant test anymore due to hashing
})

test('password is deleted', async () => {
  spyDeleteItemAsync.mockReturnValue(Promise.resolve())
  await deletePassword(passwordForTests)
  expect(spyDeleteItemAsync).toHaveBeenCalledWith(signature + passwordStore)
})

test('setPasswordStorage non empty new password', async () => {
  const hashGetter = jest.spyOn(hashing, 'getPasswordHash')
  hashGetter.mockReturnValue(Promise.resolve('fakeHashLongString='))
  const saltGetter = jest.spyOn(hashing, 'createSalt')
  saltGetter.mockReturnValue('salt')
  const encryptItem = jest.spyOn(localManipulation, 'storeDataEncrypted')
  const decryptItem = jest.spyOn(localManipulation, 'getDataEncrypted')
  encryptItem.mockReturnValue(Promise.resolve())
  decryptItem.mockReturnValue(Promise.resolve())
  const spySetItem = jest.spyOn(AsyncStorage, 'setItem')
  const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
  spyDeleteItemAsync.mockReturnValue(Promise.resolve())
  spyGetItemAsync.mockReturnValue(Promise.resolve())
  spySetItemAsync.mockReturnValue(Promise.resolve())
  spySetItem.mockReturnValue(Promise.resolve())
  spyGetItem.mockReturnValue(Promise.resolve())
  spyGetItemAsync.mockReturnValue((Promise.resolve()))

  await setPasswordStorage('anything', 'oldPass')
  expect(spyDeleteItemAsync).toHaveBeenCalledTimes(1)
  expect(spySetItemAsync).toHaveBeenCalledTimes(2)
})

test('setPasswordStorage no old password', async () => {
  const hashGetter = jest.spyOn(hashing, 'getPasswordHash')
  const saltGetter = jest.spyOn(hashing, 'createSalt')
  saltGetter.mockReturnValue('salt')
  hashGetter.mockReturnValue(Promise.resolve('fakeHashLongString='))
  const encryptItem = jest.spyOn(localManipulation, 'storeDataEncrypted')
  const decryptItem = jest.spyOn(localManipulation, 'getDataEncrypted')
  encryptItem.mockReturnValue(Promise.resolve())
  decryptItem.mockReturnValue(Promise.resolve())
  const spySetItem = jest.spyOn(AsyncStorage, 'setItem')
  const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
  spyDeleteItemAsync.mockReturnValue(Promise.resolve())
  spyGetItemAsync.mockReturnValue(Promise.resolve())
  spySetItemAsync.mockReturnValue(Promise.resolve())
  spySetItem.mockReturnValue(Promise.resolve())
  spyGetItem.mockReturnValue(Promise.resolve())
  spyGetItemAsync.mockReturnValue((Promise.resolve()))

  await setPasswordStorage('anything', '')
  expect(spySetItemAsync).toHaveBeenCalledTimes(2)
})
