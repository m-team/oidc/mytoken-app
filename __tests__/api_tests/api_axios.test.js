import mockAxios from 'jest-mock-axios'
import * as mockedFile from '../../src/api/axiosRequest'
import {
  checkDomain,
  getConfirm,
  getDomainAddressesExternal,
  getIPLocationInfo,
  getProviderList, getTemplateList,
  getAllTokens, getTokenExpiration,
  getTokenInfoForAction,
  getTransferCode,
  polling,
  revokeToken, testParseCapabilities,
  TIMEOUT
} from '../../src/api/axiosRequest'
import { DOMAIN_URL_KIT, MYTOKEN_SERVER_CONFIG } from '../../src/api/constants/domain_url'
import { DEFAULT_TEMPLATE, KIT_DOMAIN_URLS } from '../../src/utilities/main_types_defaults'
import * as AsyncStorage from '@react-native-async-storage/async-storage'
import * as localUnsecureStorage from '../../src/api/localUnsecureStorage/localDomainAdresses'
import axios from 'axios'
import { getTokenTags } from '../../src/api/generateBody'
import { NAME_APPEND } from '../../src/api/constants/request_body_defaults'
jest.mock('axios')
jest.mock('crypto-es', () => {
  return {
    CryptoES: () => {}
  }
})
const config = {
  timeout: TIMEOUT,
  headers: {
    Connection: 'keep-alive',
    'Content-Type': 'application/json',
    'User-Agent': 'mytoken-app:'
  }
}

beforeEach(() => {
  jest.useFakeTimers()
})
afterEach(() => {
  jest.useRealTimers()
  mockAxios.reset()
  jest.clearAllMocks()
})

/*
test('Polling wrong Response', () => {
  jest.useFakeTimers()
  // axios.post.mockRejectedValue()
  const axiosDomain = jest.spyOn(localUnsecureStorage, 'getDomainAddresses')
  axiosDomain.mockReturnValue(Promise.resolve(KIT_DOMAIN_URLS))
  polling('information', KIT_DOMAIN_URLS)
  for (let i = 0; i < 20; i++) {
    console.log('none')
    jest.runAllTimers()
    mockAxios.mockError()
  }
  return expect(mockAxios.post).toHaveBeenCalledTimes(20)
})

 */

describe('Mock functions using getAllTokens', () => {
  const childrenList = [{
    created: new Date(2022, 10, 0),
    name: 'firstChild',
    momID: 'fgvvvv'
  },
  {
    created: new Date(2022, 10, 0),
    name: 'secondChild',
    momID: 'sdsdfsf'
  }]
  const spy = jest.spyOn(mockedFile, 'getAllTokens')

  beforeEach(() => {
    spy.mockReturnValue(Promise.resolve(childrenList))
  })
  afterEach(() => {
    spy.mockRestore()
  })
  it('gets token data', async () => {
    const data = await mockedFile.getTokenData('randomToken', 'test', 'firstChild', KIT_DOMAIN_URLS)
    expect(data).toEqual(childrenList[0])
  })
})

describe('Mock functions using getTokenInfoForAction', () => {
  const spy = jest.spyOn(mockedFile, 'getTokenInfoForAction')

  afterAll(() => {
    spy.mockRestore()
  })
  it('gets token history', async () => {
    const unparsedHistoryData = [
      {
        event: 'created',
        comment: 'Used grant_type oidc_flow authorization_code',
        ip: '142.42.42.42',
        user_agent: 'Mozilla/5.0(X11;Linuxx86_64;rv:78.0)Gecko/20100101Firefox/78.0',
        time: 1636373529
      },
      {
        event: 'tokeninfo_introspect',
        ip: '142.42.42.42',
        user_agent: 'Mozilla/5.0(X11;Linuxx86_64;rv:78.0)Gecko/20100101Firefox/78.0',
        time: 1636373530
      }]
    const afterParsing = [
      {
        event: 'created',
        ip: '142.42.42.42',
        time: new Date(1636373529 * 1000)
      },
      {
        event: 'tokeninfo_introspect',
        ip: '142.42.42.42',
        time: new Date(1636373530 * 1000)
      }
    ]
    spy.mockReturnValue(Promise.resolve({ events: unparsedHistoryData }))
    const data = await mockedFile.getTokenHistory('randomToken', KIT_DOMAIN_URLS)
    expect(data).toEqual(afterParsing)
  })

  it('gets token children', async () => {
    const unparsedSubTokenData = [
      {
        token: {
          ip: '127.0.0.1',
          name: 'mytoken-web MT for list_mytokens',
          mom_id: 'aabbcc',
          created: 1636384021
        }
      },
      {
        token: {
          ip: '127.0.0.1',
          name: 'mytoken-web MT for AT',
          mom_id: 'ddeeff',
          created: 1636384026
        }
      }
    ]
    const afterParsing = [
      {
        name: 'mytoken-web MT for list_mytokens',
        creation: new Date(1636384021 * 1000),
        children: [],
        momID: 'aabbcc',
        parent: 'randomToken',
        expiration: undefined
      },
      {
        name: 'mytoken-web MT for AT',
        creation: new Date(1636384026 * 1000),
        children: [],
        momID: 'ddeeff',
        parent: 'randomToken',
        expiration: undefined
      }
    ]
    spy.mockReturnValue(Promise.resolve({ mytokens: [{ token: { name: NAME_APPEND + 'app', mom_id: 'other' }, children: unparsedSubTokenData }] }))
    const data = await mockedFile.getAllTokens('randomToken', 'other', KIT_DOMAIN_URLS)
    expect(data).toEqual(afterParsing)
  })
  it('gets token siblings', async () => {
    const unparsedSubTokenData = [
      {
        token: {
          ip: '127.0.0.1',
          name: 'mytoken-web MT for list_mytokens',
          mom_id: 'aabbcc',
          created: 1636384021
        }
      },
      {
        token: {
          ip: '127.0.0.1',
          name: 'mytoken-web MT for AT',
          mom_id: 'ddeeff',
          created: 1636384026
        }
      }
    ]
    const afterParsing = [
      {
        name: 'mytoken-web MT for list_mytokens',
        creation: new Date(1636384021 * 1000),
        children: [],
        momID: 'aabbcc',
        parent: 'randomToken',
        expiration: undefined
      },
      {
        name: 'mytoken-web MT for AT',
        creation: new Date(1636384026 * 1000),
        children: [],
        momID: 'ddeeff',
        parent: 'randomToken',
        expiration: undefined
      }
    ]
    spy.mockReturnValue(Promise.resolve({
      mytokens: [{
        token: {
          name: 'mytoken-web MT for AT',
          created: 1636384021,
          mom_id: 'ddeeff',
          parent: 'randomToken',
          expires_at: 1636384021
        },
        children: unparsedSubTokenData
      }]
    }))
    const data = await mockedFile.getAllTokens('randomToken', 'this', KIT_DOMAIN_URLS)
    expect(data).toEqual([{
      name: 'mytoken-web MT for AT',
      creation: new Date(1636384021 * 1000),
      children: afterParsing,
      external: true,
      momID: 'ddeeff',
      parent: 'randomToken',
      expiration: new Date(1636384021 * 1000)
    }])
  })
})

test('Fetch polling code and provide url to confirm', async () => {
  const provider = {
    name: 'EGI',
    url: 'https://aai.egi.eu/auth/realms/egi'
  }
  const body = {
    name: NAME_APPEND + 'mock',
    oidc_issuer: 'https://aai.egi.eu/auth/realms/egi',
    grant_type: 'oidc_flow',
    oidc_flow: 'authorization_code',
    client_type: 'native',
    restrictions: [{}],
    capabilities: ['AT', 'tokeninfo', 'manage_mytokens', 'create_mytoken', 'settings'],
    application_name: 'Mytoken app',
    response_type: 'short_token'
  }
  const responseObj = { data: { polling_code: 'two', consent_uri: 'two' } }
  axios.post.mockResolvedValue(responseObj)
  const axiosDomain = jest.spyOn(localUnsecureStorage, 'getDomainAddresses')
  axiosDomain.mockReturnValue(Promise.resolve(KIT_DOMAIN_URLS))
  await getConfirm(provider, KIT_DOMAIN_URLS)
  return expect(axios.post).toHaveBeenCalledWith(DOMAIN_URL_KIT + 'api/v0/token/my/', JSON.stringify(body), config)
})

test('Polling Correct Response', async () => {
  const responseObj = { data: 'server says hello!' }
  axios.post.mockResolvedValue(responseObj)
  const axiosDomain = jest.spyOn(localUnsecureStorage, 'getDomainAddresses')
  axiosDomain.mockReturnValue(Promise.resolve(KIT_DOMAIN_URLS))
  await polling('information', KIT_DOMAIN_URLS)
  return expect(axios.post).toHaveBeenCalledTimes(1)
})
test('Transfer Code Test', async () => {
  const responseObj = { data: { transfer_code: 'aNiceCode' } }
  axios.post.mockResolvedValue(responseObj)
  const axiosDomain = jest.spyOn(localUnsecureStorage, 'getDomainAddresses')
  axiosDomain.mockReturnValue(Promise.resolve(KIT_DOMAIN_URLS))
  await getTransferCode('subtokenname', 'fakemytokenstring', {
    obtain_tokens: false,
    token_info: false,
    list_tokens: false,
    create_new_token: false,
    access_user_grants: false,
    access_ssg_grants: false,
    revoke_any_token: false
  }, KIT_DOMAIN_URLS, DEFAULT_TEMPLATE, DEFAULT_TEMPLATE, DEFAULT_TEMPLATE, DEFAULT_TEMPLATE)
  const requestSubToken = {
    name: NAME_APPEND + 'subtokenname',
    grant_type: 'mytoken',
    mytoken: 'fakemytokenstring',
    capabilities: [],
    response_type: 'transfer_code'
  }

  return expect(axios.post).toHaveBeenCalledWith(DOMAIN_URL_KIT + 'api/v0/token/my/', JSON.stringify(requestSubToken), config)
})

test('Token Info for Action', async () => {
  const requestTokenInfo = {
    action: 'history',
    mytoken: 'randomToken',
    mom_id: 'tokenInfoID'
  }
  axios.post.mockResolvedValue({ date: 'something' })
  await getTokenInfoForAction('history', 'randomToken', KIT_DOMAIN_URLS, 'tokenInfoID')
  return expect(axios.post).toHaveBeenCalledWith(DOMAIN_URL_KIT + 'api/v0/tokeninfo', JSON.stringify(requestTokenInfo), config)
})

test('Get Token Children', async () => {
  // response not parsed due to jest-mock being async. Look for fix
  const responseObj = {
    data: {
      mytokens: [{
        token: {
          ip: '127.0.0.1',
          name: NAME_APPEND + 'app',
          mom_id: 'abcdef',
          created: 1636373529
        },
        children: [
          {
            token: {
              ip: '127.0.0.1',
              name: 'mytoken-web MT for list_mytokens',
              created: 1636384021,
              mom_id: 'abcd'
            }
          },
          {
            token: {
              ip: '127.0.0.1',
              name: 'mytoken-web MT for AT',
              created: 1636384026,
              mom_id: 'abcd'
            }
          }
        ]
      }]
    }
  }
  axios.post.mockResolvedValue(responseObj)
  const axiosDomain = jest.spyOn(localUnsecureStorage, 'getDomainAddresses')
  axiosDomain.mockReturnValue(Promise.resolve(KIT_DOMAIN_URLS))
  await getAllTokens('randommytokenstring', 'random', KIT_DOMAIN_URLS)
  return expect(axios.post).toHaveBeenCalledTimes(1)
})

test('Revoke Token', async () => {
  const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
  spyGetItem.mockReturnValue(Promise.resolve(JSON.stringify([KIT_DOMAIN_URLS])))
  const responseObj = { data: { transfer_code: 'aNiceCode' } }
  axios.post.mockResolvedValue(responseObj)
  await revokeToken('token', true, KIT_DOMAIN_URLS, 'revocationID').then(() => {})
  const request = {
    token: 'token',
    mom_id: 'revocationID',
    recursive: true
  }
  expect(axios.post).toHaveBeenCalledWith(DOMAIN_URL_KIT + 'api/v0/token/revoke', JSON.stringify(request), config)
})

test('Get geo ip location info fetches info through axios', async () => {
  const axiosDomain = jest.spyOn(localUnsecureStorage, 'getDomainAddresses')
  axiosDomain.mockReturnValue(Promise.resolve(KIT_DOMAIN_URLS))
  axios.post.mockResolvedValue({ data: 'nothing' })
  await getIPLocationInfo('192.1687.0.1', '1234fwsdfsr23rrdwf', KIT_DOMAIN_URLS)
  expect(axios.post).toHaveBeenCalled()
})

test('Get Token Expiration', async () => {
  // response not parsed due to jest-mock being async. Look for fix
  const responseObj = {
    data: {
      token: {
        exp: 8238482438
      }
    }
  }
  axios.post.mockResolvedValue(responseObj)
  const date = await getTokenExpiration('randommytokenstring', KIT_DOMAIN_URLS)
  expect(date).toEqual(new Date(8238482438 * 1000))
  return expect(axios.post).toHaveBeenCalledTimes(1)
})

test('Domain is checked', () => {
  checkDomain('test.com/')
  mockAxios.mockResponse({
    statusCode: 200,
    data: { body: JSON.stringify(null) }
  })
  return expect(mockAxios.get).toHaveBeenCalledWith('test.com/' + MYTOKEN_SERVER_CONFIG, config)
})

test('Get cache info', () => {
  getDomainAddressesExternal(DOMAIN_URL_KIT)
  mockAxios.mockResponse({
    statusCode: 200,
    data: { body: JSON.stringify(KIT_DOMAIN_URLS) }
  })
  return expect(mockAxios.get).toHaveBeenCalledWith(DOMAIN_URL_KIT + MYTOKEN_SERVER_CONFIG, config)
})
/*
test('Get providers', () => {
  getProviderList(KIT_DOMAIN_URLS)
  mockAxios.mockResponse({
    statusCode: 200,
    data: {
      body: JSON.stringify([
        { url: 'test', name: 'test' }
      ])
    }
  })
  return expect(mockAxios.get).toHaveBeenCalledWith(DOMAIN_URL_KIT + MYTOKEN_SERVER_CONFIG, config)
})
*/
test('Get templates rotation', async () => {
  const axiosDomain = jest.spyOn(localUnsecureStorage, 'getDomainAddresses')
  axiosDomain.mockReturnValue(Promise.resolve(KIT_DOMAIN_URLS))
  axios.get.mockResolvedValue(
    {
      statusCode: 200,
      data: [
        '_'
      ]
    }
  )
  await getTemplateList(KIT_DOMAIN_URLS, 'rotation')
  expect(axios.get).toHaveBeenNthCalledWith(1, DOMAIN_URL_KIT + 'api/v0/pt', config)
  expect(axios.get).toHaveBeenLastCalledWith(DOMAIN_URL_KIT + 'api/v0/pt/' + '_' + '/' + 'rotation', config)
})
test('Get templates capabilities', async () => {
  const axiosDomain = jest.spyOn(localUnsecureStorage, 'getDomainAddresses')
  axiosDomain.mockReturnValue(Promise.resolve(KIT_DOMAIN_URLS))
  axios.get.mockResolvedValue(
    {
      statusCode: 200,
      data: [
        '_'
      ]
    }
  )
  await getTemplateList(KIT_DOMAIN_URLS, 'capabilities')
  expect(axios.get).toHaveBeenNthCalledWith(1, DOMAIN_URL_KIT + 'api/v0/pt', config)
  expect(axios.get).toHaveBeenLastCalledWith(DOMAIN_URL_KIT + 'api/v0/pt/' + '_' + '/' + 'capabilities', config)
})
test('Get templates restrictions', async () => {
  const axiosDomain = jest.spyOn(localUnsecureStorage, 'getDomainAddresses')
  axiosDomain.mockReturnValue(Promise.resolve(KIT_DOMAIN_URLS))
  axios.get.mockResolvedValue(
    {
      statusCode: 200,
      data: [
        '_'
      ]
    }
  )
  await getTemplateList(KIT_DOMAIN_URLS, 'restrictions')
  expect(axios.get).toHaveBeenNthCalledWith(1, DOMAIN_URL_KIT + 'api/v0/pt', config)
  expect(axios.get).toHaveBeenLastCalledWith(DOMAIN_URL_KIT + 'api/v0/pt/' + '_' + '/' + 'restrictions', config)
})

test('Get profiles', async () => {
  const axiosDomain = jest.spyOn(localUnsecureStorage, 'getDomainAddresses')
  axiosDomain.mockReturnValue(Promise.resolve(KIT_DOMAIN_URLS))
  axios.get.mockResolvedValue(
    {
      statusCode: 200,
      data: [
        '_'
      ]
    }
  )
  await getTemplateList(KIT_DOMAIN_URLS, 'profile')
  expect(axios.get).toHaveBeenNthCalledWith(1, DOMAIN_URL_KIT + 'api/v0/pt', config)
  expect(axios.get).toHaveBeenLastCalledWith(DOMAIN_URL_KIT + 'api/v0/pt/' + '_' + '/' + 'profile', config)
})

test('Test parsing', async () => {
  testParseCapabilities(['AT', 'tokeninfo', 'manage_mytokens', 'create_mytoken', 'settings'])
  // just a parser, make sure doesnt crash test
})
test('Test parsing booleans', async () => {
  getTokenTags({ obtain_tokens: true, token_info: true, create_new_token: true, read_write_settings: true, manage_mytokens: true })
  // just a parser, make sure doesnt crash test
})
