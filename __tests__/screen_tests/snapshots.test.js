import React from 'react'
import renderer from 'react-test-renderer'
import CreateSubToken from '../../src/screens/CreateSubToken'
import Menu from '../../src/screens/Menu'
import AddMasterToken from '../../src/screens/AddMasterToken'
import MasterTokenSettings from '../../src/screens/MasterTokenSettings'
import LoginSettings from '../../src/screens/LoginSettings'
import NotificationSettings from '../../src/screens/NotificationSettings'
import AboutSettings from '../../src/screens/AboutSettings'
import Home from '../../src/screens/Home'
import Welcome from '../../src/screens/Welcome'
import { MyContext } from '../../src/utilities/appContext'
import { DEFAULT_MASTER_TOKEN, DEFAULT_SUBTOKEN } from '../../src/utilities/main_types_defaults'
import AppAuthenticator from '../../src/AppAuthenticator'
import AppAuthenticated from '../../src/AppAuthenticated'
import Login from '../../src/screens/Login'
import SubTokenDetails from '../../src/screens/SubTokenDetails'
import PollingScreen from '../../src/screens/PollingScreen'
import TransferToken from '../../src/screens/TransferToken'
import MapsSubTokenEvents from '../../src/screens/MapsSubTokenEvents'
import DomainScreen from '../../src/screens/DomainScreen'
import ResetScreen from '../../src/screens/ResetScreen'
import PrivacyPolicy from '../../src/screens/PrivacyPolicy'

jest.useFakeTimers()

jest.mock('expo-status-bar', () => {
  return {
    StatusBar: () => {}
  }
})
jest.mock('crypto-es', () => {
  return {
    CryptoES: () => {}
  }
})

const contextValues = {
  masterTokenList: [DEFAULT_MASTER_TOKEN],
  subTokenList: [DEFAULT_SUBTOKEN],
  setMasterTokenList: () => {},
  setSubTokenList: () => {},
  firstSetup: false,
  setFirstSetup: () => {}
}
beforeEach(() => {
  jest.useFakeTimers('legacy')
})
afterEach(() => {
  jest.useRealTimers()
})

describe('App Authenticator', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <AppAuthenticator/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('App', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
        <AppAuthenticated/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('Home Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
                <Home/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('AboutSettings Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
                <AboutSettings/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('PrivacyPolicy Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
                <PrivacyPolicy/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('Reset Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
                <ResetScreen/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('AddMasterToken Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
                <AddMasterToken/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('CreateSubToken Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
                <CreateSubToken/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('MasterTokenSettings Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
                <MasterTokenSettings/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('LoginSettings Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
                <LoginSettings/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('NotificationSettings Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
                <NotificationSettings/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('PollingScreen Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
            <PollingScreen route={{ params: { pollingLink: 'test', pollingCode: 'test', providerName: 'test' } }}/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('Settings Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
                <Menu/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('SubTokenDetails Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
            <SubTokenDetails route={{ params: { subTokenID: 'DEFAULT' } }}/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('TransferToken Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
            <TransferToken route={{ params: { subTokenID: 'test', transferCode: 'test' } }}/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('Welcome Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
                <Welcome/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('Login Screen', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
                <Login/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('MapSubTokenEvents Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
                <MapsSubTokenEvents route={{ params: { searchList: [] } }}/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('DomainScreen Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MyContext.Provider value={contextValues}>
                <DomainScreen route={{ params: { searchList: [] } }}/>
            </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
