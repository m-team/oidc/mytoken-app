import {
  addMasterToken, filterMasterTokensForProvider, filterProviders,
  // updateSubTokenList,
  removeMasterToken,
  reset,
  resolveMasterToken
} from '../../src/utilities/contextUtilities'
import mockAxios from 'jest-mock-axios'

import { KIT_DOMAIN_URLS } from '../../src/utilities/main_types_defaults'
import { DOMAIN_URL_KIT } from '../../src/api/constants/domain_url'
import * as AsyncStorage from '@react-native-async-storage/async-storage'

jest.mock('crypto-es', () => {
  return {
    CryptoES: () => {}
  }
})

const permanentSubTokenList = [{
  name: 'First SubToken',
  creation: new Date(2022, 4),
  parent: 'First Element',
  momID: 'First SubToken'

},
{
  name: 'Second SubToken',
  creation: new Date(2022, 4),
  parent: 'First Element',
  momID: 'Second SubToken'

},
{
  name: 'Third SubToken',
  creation: new Date(2022, 4),
  parent: 'First Element',
  momID: 'Third SubToken'
}]

const permanentMasterTokenList = [{
  name: 'First Element',
  creation: new Date(2022, 4),
  provider: {
    name: 'test',
    url: 'test.com'
  },
  masterToken: 'first',
  domainUrl: 'test.com'
},
{
  name: 'Second Element',
  creation: new Date(2022, 4),
  provider: {
    name: 'test',
    url: 'test.com'
  },
  masterToken: 'middle',
  domainUrl: 'test.com'

},
{
  name: 'Third Element',
  creation: new Date(2022, 4),
  provider: {
    name: 'test',
    url: 'test.com'
  },
  masterToken: 'last',
  domainUrl: 'test.com'
}]
describe('Remove Identities test', () => {
  let identitiesList
  const setIdentitiesList = (newList) => {
    identitiesList = newList
  }
  afterEach(() => {
    mockAxios.reset()
  })

  beforeEach(() => {
    identitiesList = permanentMasterTokenList.slice()
  })

  it('removed first item', () => {
    const removeFirstIdentity = {
      name: 'First Element',
      creation: new Date(2022, 4),
      provider: {
        name: 'test',
        url: 'test.com'
      },
      masterToken: 'first',
      domainUrl: 'test.com'
    }
    removeMasterToken(removeFirstIdentity, identitiesList, setIdentitiesList)
    expect(identitiesList).toEqual([permanentMasterTokenList[1], permanentMasterTokenList[2]])
  })
  it('removed middle item', () => {
    const removeMiddleIdentity = {
      name: 'Second Element',
      creation: new Date(2022, 4),
      provider: {
        name: 'test',
        url: 'test.com'
      },
      masterToken: 'middle',
      domainUrl: 'test.com'
    }
    removeMasterToken(removeMiddleIdentity, identitiesList, setIdentitiesList)
    expect(identitiesList).toEqual([permanentMasterTokenList[0], permanentMasterTokenList[2]])
  })
  it('removed last item', () => {
    const removeLastIdentity = {
      name: 'Third Element',
      creation: new Date(2022, 4),
      provider: {
        name: 'test',
        url: 'test.com'
      },
      masterToken: 'last',
      domainUrl: 'test.com'
    }
    removeMasterToken(removeLastIdentity, identitiesList, setIdentitiesList)
    expect(identitiesList).toEqual([permanentMasterTokenList[0], permanentMasterTokenList[1]])
  })
})

describe('Add Identities test', () => {
  let identitiesList
  const setIdentitiesList = (newList) => {
    identitiesList = newList
  }
  beforeEach(() => {
    identitiesList = [permanentMasterTokenList[0], permanentMasterTokenList[1]]
  })

  it('added to populated list', () => {
    const addedIdentityList = permanentMasterTokenList.slice()
    const addingIdentity = {
      name: 'Third Element',
      creation: new Date(2022, 4),
      provider: {
        name: 'test',
        url: 'test.com'
      },
      masterToken: 'last',
      domainUrl: 'test.com'
    }
    addMasterToken(addingIdentity, identitiesList, setIdentitiesList)
    expect(identitiesList).toEqual(addedIdentityList)
  })
  it('added to empty list', () => {
    const addingIdentity = {
      name: 'New Element',
      creation: new Date(2022, 4),
      provider: {
        name: 'test',
        url: 'test.com'
      },
      masterToken: 'faketokenfortestusuallylongstring',
      domainUrl: 'test.com'
    }
    addMasterToken(addingIdentity, [], setIdentitiesList)
    expect(identitiesList).toEqual([addingIdentity])
  })
})
describe('ResetScreen test', () => {
  let identitiesList
  let subTokenList
  // eslint-disable-next-line no-unused-vars
  let firstSetup = false
  const setIdentitiesList = (newList) => {
    identitiesList = newList
  }
  const setFirstSetup = (newList) => {
    firstSetup = newList
  }
  const setSubTokenList = (newList) => {
    subTokenList = newList
  }
  beforeEach(() => {
    subTokenList = permanentSubTokenList.slice()
    identitiesList = [{
      name: 'First Element',
      creation: new Date(2022, 4),
      provider: {
        name: 'test',
        url: 'test.com'
      },
      masterToken: 'faketokenfortestusuallylongstring',
      domainUrl: DOMAIN_URL_KIT
    }]
  })

  it('resets context', async () => {
    const spyGetItem = jest.spyOn(AsyncStorage, 'getItem')
    spyGetItem.mockReturnValue(Promise.resolve(JSON.stringify([KIT_DOMAIN_URLS])))
    reset(identitiesList, setSubTokenList, setIdentitiesList, setFirstSetup, [KIT_DOMAIN_URLS])
    expect(identitiesList).toEqual([])
    expect(subTokenList).toEqual([])
  })
})

describe('combines tokens with same provider and keeps oldest', () => {
  const masterList = [{
    name: 'First Element',
    expirationDate: new Date(2022, 8),
    provider: {
      name: 'test1',
      url: 'test1.com'
    },
    masterToken: 'first',
    domainUrl: 'test.com'
  },
  {
    name: 'Second Element',
    expirationDate: new Date(2022, 4),
    provider: {
      name: 'test1',
      url: 'test1.com'
    },
    masterToken: 'middle',
    domainUrl: 'test.com'

  },
  {
    name: 'Third Element',
    expirationDate: new Date(2022, 4),
    provider: {
      name: 'test',
      url: 'test.com'
    },
    masterToken: 'last',
    domainUrl: 'test.com'
  }]
  it('remove multile duplicated when multiple providers present and keeps oldest', () => {
    const masterListRemoved = [{
      name: 'First Element',
      expirationDate: new Date(2022, 8),
      provider: {
        name: 'test1',
        url: 'test1.com'
      },
      masterToken: 'first',
      domainUrl: 'test.com'
    },
    {
      name: 'Third Element',
      expirationDate: new Date(2022, 4),
      provider: {
        name: 'test',
        url: 'test.com'
      },
      masterToken: 'last',
      domainUrl: 'test.com'
    }]
    const array = filterMasterTokensForProvider(masterList)
    expect(array).toEqual(masterListRemoved)
  })
  it('removed subtokens that arent with the provider in question', () => {
    const permanentSubTokenList = [{
      name: 'First SubToken',
      creation: new Date(2022, 4),
      parent: 'First Element',
      momID: 'First SubToken'

    },
    {
      name: 'Second SubToken',
      creation: new Date(2022, 4),
      parent: 'First Element',
      momID: 'Second SubToken'

    },
    {
      name: 'Third SubToken',
      creation: new Date(2022, 4),
      parent: 'last',
      momID: 'Third SubToken'
    }]

    const array = filterProviders({
      name: 'test',
      url: 'test.com'
    }, permanentSubTokenList, masterList)
    expect(array).toEqual([{
      name: 'Third SubToken',
      creation: new Date(2022, 4),
      parent: 'last',
      momID: 'Third SubToken'
    }])
  })
})

describe('Resolving Identity by token', () => {
  it('doesnt exist', () => {
    expect(resolveMasterToken('first', permanentMasterTokenList)).toEqual(permanentMasterTokenList[0])
  })
})
