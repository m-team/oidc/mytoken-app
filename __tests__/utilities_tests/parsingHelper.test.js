import { getText, percentBetween, searchEventHistoryList, searchSubTokenList } from '../../src/utilities/parsingHelper'

describe('SubToken list search', () => {
  const subTokenList = [{
    name: 'identity',
    creation: new Date(2022, 4),
    parent: 'First Provider'
  },
  {
    name: 'anotherOne',
    creation: new Date(2022, 4),
    parent: 'First Element'
  },
  {
    name: 'IDENTITY1',
    creation: new Date(2022, 4),
    parent: 'First Element'
  }]
  it('search lowercase match', () => {
    expect(searchSubTokenList('identity', subTokenList)).toEqual([subTokenList[0], subTokenList[2]])
  })
  it('search uppercase match', () => {
    expect(searchSubTokenList('IDENTITY', subTokenList)).toEqual([subTokenList[0], subTokenList[2]])
  })
  it('search camelcase match', () => {
    expect(searchSubTokenList('IdEnTity', subTokenList)).toEqual([subTokenList[0], subTokenList[2]])
  })
  it('search single match', () => {
    expect(searchSubTokenList('anotherOne', subTokenList)).toEqual([subTokenList[1]])
  })
  it('search no match', () => {
    expect(searchSubTokenList('asafdsfsdfsdf', subTokenList)).toEqual([])
  })
  it('search empty match', () => {
    expect(searchSubTokenList('', subTokenList)).toEqual(subTokenList)
  })
  it('search beginning letter', () => {
    expect(searchSubTokenList('I', subTokenList)).toEqual([subTokenList[0], subTokenList[2]])
  })
  it('search contains word', () => {
    expect(searchSubTokenList('one', subTokenList)).toEqual([subTokenList[1]])
  })
  it('search no word because too short', () => {
    expect(searchSubTokenList('on', subTokenList)).toEqual([])
  })
  it('search provider', () => {
    expect(searchSubTokenList('First Provider', subTokenList)).toEqual([subTokenList[0]])
  })
  it('trimmed phrase', () => {
    expect(searchSubTokenList('one ', subTokenList)).toEqual([subTokenList[1]])
  })
})

describe('SubToken list search', () => {
  const eventList = [{
    event: 'creation',
    time: new Date()
  },
  {
    event: 'token_subtokens',
    time: new Date()
  },
  {
    event: 'token_history',
    time: new Date()
  }]

  it('trimmed phrase', () => {
    expect(searchEventHistoryList('creation ', eventList)).toEqual([eventList[0]])
  })
  it('phrase', () => {
    expect(searchEventHistoryList('creation', eventList)).toEqual([eventList[0]])
  })
})

describe('Get text  tester', () => {
  it('get minutes', () => {
    expect(getText(new Date(new Date().getTime() + 4.5 * 60000))).toEqual('4 min')
  })
  it('get hours', () => {
    expect(getText(new Date(new Date().getTime() + 4.5 * 60000 * 60))).toEqual('4 hours')
  })
  it('get days', () => {
    expect(getText(new Date(new Date().getTime() + 4.5 * 60000 * 60 * 24))).toEqual('4 days')
  })
  it('get years', () => {
    expect(getText(new Date(new Date().getTime() + 4.5 * 60000 * 60 * 24 * 365))).toEqual('4 years')
  })
})
describe('Get between  tester', () => {
  it('get years', () => {
    expect(percentBetween(new Date(new Date().getTime() - 4.5 * 60000 * 60 * 24 * 365), new Date(new Date().getTime() + 4.5 * 60000 * 60 * 24 * 365))).toEqual(50)
  })
})
