import React from 'react'
import CreateNewButton from '../../src/components/buttons/CreateNewButton'
import MainButton from '../../src/components/buttons/MainButton'
import SubButton from '../../src/components/buttons/SubButton'
import CheckBoxComponent from '../../src/components/fields/check_box/CheckBoxComponent'
import renderer from 'react-test-renderer'
import ProviderDropDown from '../../src/components/fields/drop_downs/ProviderDropDown'
import SearchBar from '../../src/components/fields/search_bar/SearchBar'
import TextFieldWithHeader from '../../src/components/spacing_and_headers/TextFieldWithHeader'
import DividerWithText from '../../src/components/spacing_and_headers/DividerWithText'
import CardContent from '../../src/components/content_cards/CardContent'
import CardMasterTokenSettings from '../../src/components/content_cards/CardMasterTokenSettings'
import CardSettings from '../../src/components/content_cards/CardSettings'
import EntrySubTitle from '../../src/components/spacing_and_headers/EntrySubTitle'
import StandardHeader from '../../src/components/spacing_and_headers/StandardHeader'
import Divider from '../../src/components/spacing_and_headers/Divider'
import SubDivider from '../../src/components/spacing_and_headers/SubDivider'
import Heatmap from '../../src/components/Heatmap'
import SecureSquare from '../../src/components/fields/secure_text/SecureSquare'
import SecureText from '../../src/components/fields/secure_text/SecureText'
import ErrorScreen from '../../src/components/fields/ErrorScreen'
import SearchComponentSubToken from '../../src/components/SearchComponentSubToken'
import Carousel from '../../src/components/Carousel'
import SearchHeader from '../../src/components/spacing_and_headers/SearchHeader'
import HomeHeader from '../../src/components/spacing_and_headers/HomeHeader'
import { DEFAULT_MASTER_TOKEN, DEFAULT_TEMPLATE } from '../../src/utilities/main_types_defaults'
import { MyContext } from '../../src/utilities/appContext'
import CheckBoxFilter from '../../src/components/fields/check_box/CheckboxFilter'
import DomainDropDown from '../../src/components/fields/drop_downs/DomainDropDown'
import MasterTokenDropDown from '../../src/components/fields/drop_downs/MasterTokenDropDown'
import DropDownMenu from '../../src/components/fields/drop_downs/DropDownMenu'
import TemplatesDropDown from '../../src/components/fields/drop_downs/TemplatesDropDown'
import TransferProgressIndicator from '../../src/components/fields/secure_text/TransferProgressIndicator'
import LoadingIndicator from '../../src/components/fields/loading_indicator/LoadingIndicator'

const contextValues = {
  masterTokenList: [DEFAULT_MASTER_TOKEN],
  subTokenList: [],
  setMasterTokenList: () => {},
  setSubTokenList: () => {},
  firstSetup: false,
  setFirstSetup: () => {}
}
jest.mock('crypto-es', () => {
  return {
    CryptoES: () => {}
  }
})
jest.mock('react-native-circular-progress-indicator')

beforeEach(() => {
  jest.useFakeTimers('legacy')
})
afterEach(() => {
  jest.useRealTimers()
})
describe('CreateNewButton Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <CreateNewButton/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('MainButton Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <MainButton/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('SubButton Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <SubButton/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('CheckBoxComponent Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <CheckBoxComponent/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('Heatmap Test', () => {
  it('renders correctly', () => {
    const historyEvent = {
      event: 'tokeninfo_introspect',
      ip: '142.42.42.42',
      time: new Date()
    }
    const today = new Date()
    const yesterday = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1)
    const tomorrow = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 2)
    const historyData = [{ ...historyEvent, event: 'created', time: yesterday }, { ...historyEvent, time: tomorrow }]
    for (let i = 0; i < 21; i++) {
      historyData.push(historyEvent)
    }
    const tree = renderer.create(
            <Heatmap allEvents={historyData}/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('ProviderDropDown Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <ProviderDropDown provider = {{
              name: 'EGI',
              url: 'https://aai.egi.eu/auth/realms/egi'
            }}
            />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('SearchBar Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <SearchBar onChangeSearch={() => {}} filterThroughList={() => {
              return []
            }} setSearchList={() => {}}/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('SecureSquare Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <SecureSquare/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('SecureText Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <SecureText phrase="test"/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('TextFieldWithHeader Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <TextFieldWithHeader/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('Divider Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <Divider/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('DividerWithText Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <DividerWithText/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('Sub_divider Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <SubDivider/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('CardContent Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <CardContent/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('CardMasterTokenSettings Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <CardMasterTokenSettings/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('CardSettings Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <CardSettings/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('EntrySubTitle Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <EntrySubTitle/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('StandardHeader Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
            <StandardHeader/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('ErrorScreen Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
        <ErrorScreen/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('SearchComponentSubToken Test', () => {
  it('renders correctly', () => {
    const historyEvent = {
      event: 'tokeninfo_introspect',
      ip: '142.42.42.42',
      time: new Date()
    }
    const historyData = []
    for (let i = 0; i < 21; i++) {
      historyData.push(historyEvent)
    }
    const tree = renderer.create(
        <SearchComponentSubToken filterList={historyData} transferSearch={'test'}/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('Carousel Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
        <Carousel DATA={[]}/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('HomeHeader Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
        <HomeHeader DATA={[]}/>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('SearchHeader Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
        <MyContext.Provider value={contextValues}>
          <SearchHeader DATA={[]}/>
        </MyContext.Provider>

    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('MasterTokenDropDown Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
        <MyContext.Provider value={contextValues}>
          <MasterTokenDropDown chosenProvider={{ ...DEFAULT_MASTER_TOKEN, name: 'anythingElse' }} />
        </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('CheckBoxFilter Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
        <MyContext.Provider value={contextValues}>
          <CheckBoxFilter chosenIdentity={DEFAULT_MASTER_TOKEN} />
        </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('MasterTokenDropDown Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
        <MyContext.Provider value={contextValues}>
          <MasterTokenDropDown chosenProvider={DEFAULT_MASTER_TOKEN} />
        </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('renders correctly with different identity', () => {
    const tree = renderer.create(
        <MyContext.Provider value={contextValues}>
          <MasterTokenDropDown chosenProvider={{ ...DEFAULT_MASTER_TOKEN, name: 'testing' }}/>
        </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('Domain drop down Test', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
        <MyContext.Provider value={contextValues}>
          <DomainDropDown/>
        </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('Drop down menu tenders correctly', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
        <MyContext.Provider value={contextValues}>
          <DropDownMenu renderContent={() => {}}/>
        </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('Transfer progress indicator renders correctly', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
        <MyContext.Provider value={contextValues}>
          <TransferProgressIndicator created={new Date()} expires={new Date().getTime() + 5 * 60000} phrase={'bsdfj3j'}/>
        </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
describe('Loading indicator renders correctly', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
        <MyContext.Provider value={contextValues}>
          <LoadingIndicator created={new Date()} expires={new Date().getTime() + 5 * 60000} phrase={'bsdfj3j'} colorPrimary={'#dbfbff'} colorSecondary={'#ffff33'} dateCreated={new Date()}/>
        </MyContext.Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
